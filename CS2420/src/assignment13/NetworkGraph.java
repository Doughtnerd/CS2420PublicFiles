package assignment13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * <p>
 * This class represents a graph of flights and airports along with specific
 * data about those flights. It is recommended to create an airport class and a
 * flight class to represent nodes and edges respectively. There are other ways
 * to accomplish this and you are free to explore those.
 * </p>
 * 
 * <p>
 * Testing will be done with different criteria for the "best" path so be sure
 * to keep all information from the given file. Also, before implementing this
 * class (or any other) draw a diagram of how each class will work in relation
 * to the others. Creating such a diagram will help avoid headache and confusion
 * later.
 * </p>
 * 
 * <p>
 * Be aware also that you are free to add any member variables or methods needed
 * to completed the task at hand
 * </p>
 * 
 * @author CS2420 Teaching Staff - Spring 2016
 */
public class NetworkGraph {

	/**
	 * The master data that contains all flight information.
	 */
	private ArrayList<Airport<String>> data;

	/**
	 * The graph that operates on the data.
	 */
	private DijkstraGraph<String> graph;

	/**
	 * The path to the file used to load in the data.
	 */
	// private String path;

	/**
	 * <p>
	 * Constructs a NetworkGraph object and populates it with the information
	 * contained in the given file. See the sample files or a randomly generated
	 * one for the proper file format.
	 * </p>
	 * 
	 * <p>
	 * You will notice that in the given files there are duplicate flights with
	 * some differing information. That information should be averaged and
	 * stored properly. For example some times flights are canceled and that is
	 * represented with a 1 if it is and a 0 if it is not. When several of the
	 * same flights are reported totals should be added up and then reported as
	 * an average or a probability (value between 0-1 inclusive).
	 * </p>
	 * 
	 * @param flightInfoPath
	 *            - The path to the file containing flight data. This should be
	 *            a *.csv(comma separated value) file
	 * 
	 * @throws FileNotFoundException
	 *             The only exception that can be thrown in this assignment and
	 *             only if a file is not found.
	 */
	public NetworkGraph(String flightInfoPath) throws FileNotFoundException {
		this.data = readDataFromFile(flightInfoPath);
		System.out.println("File Read");
		if (data.isEmpty()) {
			System.err.println("No valid data was read fromt the file.");
		}
	}

	/**
	 * Reads the input file and adds every flight into and arraylist to be used
	 * for the data.
	 * 
	 * @param path
	 *            - The location of the file to be read.
	 * @return An ArrayList of Airport<String> that contains all flight
	 *         information.
	 * @throws FileNotFoundException
	 */
	private ArrayList<Airport<String>> readDataFromFile(String path) throws FileNotFoundException {

		// Used to skip header
		int lineCount = 0;

		// List of flights that will be constructed.
		ArrayList<Airport<String>> list = new ArrayList<Airport<String>>();

		// The file to scan;
		File file = new File(path);

		// Scanner used to read the file.
		Scanner scanner = new Scanner(file);

		// Scan through the file.
		while (scanner.hasNextLine()) {
			// Get the line.
			String line = scanner.nextLine();

			// If this is the first line, skip, else enter if.
			if (lineCount != 0) {
				// Split the line using commas as delimiters.
				String[] attributes = line.split(",");

				// Set temporary airports in case the information doesn't
				// currently exist.
				Airport<String> origin = new Airport<String>(attributes[0]);
				Airport<String> destination = new Airport<String>(attributes[1]);
				int index = list.indexOf(destination);
				if (index != -1) {
					destination = list.get(index);
				} else {
					list.add(destination);
				}
				// Construct the flight data from the rest of the attributes
				// list and the destination.
				Flight<String> route = constructRoute(destination, attributes);

				/*
				 * If the list already has the airport, don't add it again.
				 * Instead, pull out that airport and add the flight to its
				 * neighbors list - But only if the neighbors list doesn't
				 * already contain the flight.
				 */
				if (!list.contains(origin)) {
					origin.neighbors.add(route);
					list.add(origin);
				} else {
					origin = list.get(list.indexOf(origin));
					origin.neighbors.add(route);
				}
			} else {
				lineCount++;
			}
		}
		scanner.close();
		return list;
	}

	/**
	 * Constructs a flight using the attributes list that must follow strict
	 * formatting rules.
	 * 
	 * @param destination
	 *            - The destination airport.
	 * @param attributes
	 *            - The list of attributes that describe the flight.
	 * @return A new flight to add to the data.
	 */
	private Flight<String> constructRoute(Airport<String> destination, String[] attributes) {
		if (destination == null || attributes.length != 8) {
			System.err.println("There was a problem with the data handed to the route constructor");
			return null;
		}
		Flight<String> flight = new Flight<String>();
		flight.start = attributes[0];
		flight.otherEnd = destination;
		flight.carrier = attributes[2];
		try {
			flight.delay = Double.parseDouble(attributes[3]);
			flight.cancelled = Double.parseDouble(attributes[4]);
			flight.time = Double.parseDouble(attributes[5]);
			flight.distance = Double.parseDouble(attributes[6]);
			flight.cost = Double.parseDouble(attributes[7]);
		} catch (NumberFormatException e) {
			System.err.println("There was an error trying to convert flight data to a double.");
			return null;
		}
		return flight;
	}

	/**
	 * This method returns a BestPath object containing information about the
	 * best way to fly to the destination from the origin. "Best" is defined by
	 * the FlightCriteria parameter <code>enum</code>. This method should throw
	 * no exceptions and simply return a BestPath object with information
	 * dictating the result. For example, if a destination or origin is not
	 * contained in this instance of NetworkGraph simply return a BestPath with
	 * no path (not a <code>null</code> path). If origin or destination are
	 * <code>null</code> return a BestPath object with null as origin or
	 * destination (which ever is <code>null</code>.
	 * 
	 * @param origin
	 *            - The starting location to find a path from. This should be a
	 *            3 character long string denoting an airport.
	 * 
	 * @param destination
	 *            - The destination location from the starting airport. Again,
	 *            this should be a 3 character long string denoting an airport.
	 * 
	 * @param criteria
	 *            - This enum dictates the definition of "best". Based on this
	 *            value a path should be generated and return.
	 * 
	 * @return - An object containing path information including origin,
	 *         destination, and everything in between.
	 */
	public BestPath getBestPath(String origin, String destination, FlightCriteria criteria) {
		return getBestPath(origin, destination, criteria, null);
	}

	/**
	 * <p>
	 * This overloaded method should do the same as the one above only when
	 * looking for paths skip the ones that don't match the given airliner.
	 * </p>
	 * 
	 * @param origin
	 *            - The starting location to find a path from. This should be a
	 *            3 character long string denoting an airport.
	 * 
	 * @param destination
	 *            - The destination location from the starting airport. Again,
	 *            this should be a 3 character long string denoting an airport.
	 * 
	 * @param criteria
	 *            - This enum dictates the definition of "best". Based on this
	 *            value a path should be generated and return.
	 * 
	 * @param airliner
	 *            - a string dictating the airliner the user wants to use
	 *            exclusively. Meaning no flights from other airliners will be
	 *            considered.
	 * 
	 * @return - An object containing path information including origin,
	 *         destination, and everything in between.
	 */
	public BestPath getBestPath(String origin, String destination, FlightCriteria criteria, String airliner) {
		if (origin == null || destination == null || origin.equals("") || destination.equals("")) {
			return new BestPath(new ArrayList<>(), 0.0);
		}

		/*
		 * This restricts airliner to either be a default null value or the
		 * passed value
		 */
		airliner = airliner == null || airliner.equals("") ? null : airliner;

		/**
		 * Remove this if inputting the same airport for origin and destination
		 * is not allowed.
		 */
		
		if (origin.equals(destination)) {
			return new BestPath(new ArrayList<String>() {
				private static final long serialVersionUID = 1L;
				{
					add(origin);
				}
			}, 0.0);
		}
		
		/*
		 * Initialize variables.
		 */
		ArrayList<String> path = null;
		Airport<String> start = null, goal = null;
		
		/*
		 * Loops through data to find the origin and destination airports.
		 */
		for(Airport<String> a : data){
			if(a.data.equals(origin)){
				start = a;
			} else if (a.data.equals(destination)){
				goal = a;
			}
		}
		
		/*
		 * Makes sure a start and a goal were found, else return null and print an error.
		 */
		if (start==null || goal==null) {
			System.err.println("A valid start and a valid goal were not found.");
			return new BestPath(new ArrayList<>(), 0);
		} else {
			graph = new DijkstraGraph<>(start, goal);
			graph.findPath(airliner, criteria);
			path = graph.getPath();
		}
		return new BestPath(path, goal.costSoFar);
	}

	/**
	 * Class representing the graph that will use Dijkstra's algorithm
	 * 
	 * @author Christopher Carlson
	 *
	 */
	class DijkstraGraph<E extends Comparable<? super E>> {

		/**
		 * Queue used for Dijkstra's algorithm
		 */
		Queue<Airport<E>> q;

		/**
		 * The Node representing the starting point in the maze.
		 */
		Airport<E> start;

		/**
		 * The Node representing the goal point in the maze.
		 */
		Airport<E> goal;

		/**
		 * Constructs a Dijkstra graph used to find flight information.
		 * 
		 * @param start
		 *            - The origin airport.
		 * @param goal
		 *            - The destination airport.
		 */
		private DijkstraGraph(Airport<E> start, Airport<E> goal) {
			q = new LinkedList<>();
			this.start = start;
			this.goal = goal;
		}

		/**
		 * Returns the list of nodes that was used to get to the goal.
		 * 
		 * @return The path to the goal
		 */
		public ArrayList<E> getPath() {
			ArrayList<E> list = new ArrayList<>();
			Airport<E> current = goal;
			while (current.cameFrom != null) {
				list.add(0, current.data);
				current = current.cameFrom;
			}
			list.add(0, start.data);
			return list.size() == 1 ? new ArrayList<E>() : list;
		}

		/**
		 * Uses dijkstra's algorithm to find the best path.
		 * 
		 * @param carrier
		 *            - The carrier to add to the search criteria.
		 * @param criteria
		 *            - The flight criteria to determine how to weigh flights.
		 */
		private void findPath(String carrier, FlightCriteria criteria) {
			resetNodes();
			this.start.costSoFar = 0;
			q.offer(this.start);
			while (!q.isEmpty()) {
				Airport<E> curr = q.poll();
				if (curr == this.goal) {
					return;
				}
				curr.visited = true;

				/*
				 * This filters the nodes neighbors that match the carrier depending on if the carrier
				 * parameter is null or not.
				 */
				TreeSet<Flight<E>> temp = carrier != null && carrier.equals("") == false ? curr.neighbors.stream().filter(n -> n.carrier.equals(carrier)).collect(Collectors.toCollection(TreeSet::new)): curr.neighbors;
				
				for (Flight<E> f : temp) {
					setWeight(criteria, f);
					if (f.otherEnd.visited == false) {
						if (f.otherEnd.costSoFar > curr.costSoFar + f.weight) {
							q.offer(f.otherEnd);
							f.otherEnd.cameFrom = curr;
							f.otherEnd.costSoFar = curr.costSoFar + f.weight;
						}
					}
				}
			}
		}

		/**
		 * This sets the weight of the flight depending on the criteria used.
		 * 
		 * @param criteria
		 *            - FlightCriteria to use.
		 * @param f
		 *            - The flight path to alter.
		 */
		private void setWeight(FlightCriteria criteria, Flight<E> f) {
			switch (criteria) {
			case CANCELED:
				f.weight = f.cancelled;
				break;
			case COST:
				f.weight = f.cost;
				break;
			case DELAY:
				f.weight = f.delay;
				break;
			case DISTANCE:
				f.weight = f.distance;
				break;
			case TIME:
				f.weight = f.time;
				break;
			default:
				f.weight = f.cost;
				break;
			}
		}

		/**
		 * This method uses a for each loop to reset the cost, the visited,
		 * and the cameFrom variables of each airport node.
		 */
		private void resetNodes() {
			for(Airport<String> a : data){
				a.costSoFar = Double.POSITIVE_INFINITY;
				a.visited = false;
				a.cameFrom = null;
			}
		}
	}

	/**
	 * Class representing a Node (vertex)
	 * 
	 * @author Christopher Carlson
	 *
	 */
	class Airport<E extends Comparable<? super E>> implements Comparable<Airport<E>> {

		/**
		 * The data stored in the node.
		 */
		E data;

		/**
		 * The airport that preceeded this one.
		 */
		Airport<E> cameFrom;

		/**
		 * Whether or not the airport has been visited.
		 */
		boolean visited;

		/**
		 * The flights that connect to this airport.
		 */
		TreeSet<Flight<E>> neighbors;

		/**
		 * The cost so far (used in dijkstra's).
		 */
		double costSoFar;

		/**
		 * Constructs an airport node with type e data and the given list of
		 * connecting flights.
		 * 
		 * @param data
		 *            - Data this node contains.
		 * @param neighbors
		 *            - List of connecting flights.
		 */
		private Airport(E data) {
			this.data = data;
			this.visited = false;
			neighbors = new TreeSet<Flight<E>>();
		}

		@Override
		public int compareTo(Airport<E> o) {
			return this.data.compareTo(o.data);
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o) {
			if (o == this) {
				return true;
			}
			if (o == null) {
				return false;
			}
			if (o instanceof Airport) {
				return ((Airport<E>) o).data.equals(this.data);
			} else {
				return false;
			}
		}
	}

	/**
	 * Class representing an flight/edge (connection between two nodes)
	 * 
	 * @author Christopher Carlson
	 *
	 */
	class Flight<E extends Comparable<? super E>> implements Comparable<Flight<E>> {

		/**
		 * The name of the starting location, only used for toString and
		 * comparisons.
		 */
		String start;

		/**
		 * The opposite end of the flight path.
		 */
		Airport<E> otherEnd;

		/**
		 * The cost of this flight.
		 */
		double weight;

		/**
		 * The carrier for this flight path.
		 */
		String carrier;

		/**
		 * The delay for this flight.
		 */
		double delay;

		/**
		 * The chance of cancellation for this flight.
		 */
		double cancelled;

		/**
		 * The time this flight will take.
		 */
		double time;

		/**
		 * The distance of this flight.
		 */
		double distance;

		/**
		 * The monetary cost of this flight.
		 */
		double cost;

		/**
		 * Constructs an empty flight object.
		 */
		private Flight() {
			this(0);
		}

		/**
		 * Constructs an edge with a given weight.
		 * 
		 * @param weight
		 *            - Cost of the flight.
		 */
		private Flight(double weight) {
			this(null, weight);
		}

		/**
		 * Constructs a flight path with the given ending airport node and
		 * weight.
		 * 
		 * @param otherEnd
		 *            - The other end of the edge.
		 * @param weight
		 *            - The cost of this route.
		 */
		private Flight(Airport<E> otherEnd, double weight) {
			this.otherEnd = otherEnd;
			this.weight = weight;
		}

		@Override
		public String toString() {
			return start + "," + otherEnd.data + "," + carrier + "," + delay + "," + cancelled + "," + time + ","
					+ distance + "," + cost;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null) {
				return false;
			}
			if (o instanceof Flight) {
				return this.compareTo(Flight.class.cast(o)) == 0;
			} else {
				return false;
			}
		}

		@Override
		public int compareTo(Flight<E> o) {
			return Integer.compare(this.toString().hashCode(), o.toString().hashCode());
		}
	}
}
