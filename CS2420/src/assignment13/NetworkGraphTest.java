package assignment13;

import org.junit.Test;

public class NetworkGraphTest {

	@Test
	public void test() {
		NetworkGraph airportGraph = null;
		try {
			airportGraph = new NetworkGraph("testfile.csv");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		BestPath path1 = airportGraph.getBestPath("MZL", "UAZ", FlightCriteria.COST, "OO");
		System.out.println(path1.toString());
		
		BestPath path2 = airportGraph.getBestPath("MZL", "UAZ", FlightCriteria.DELAY);
		System.out.println(path2.toString());

		BestPath path3 = airportGraph.getBestPath("MZL", "UAZ", FlightCriteria.DISTANCE);
		System.out.println(path3.toString());
		
		BestPath path4 = airportGraph.getBestPath("MZL", "UAZ", FlightCriteria.TIME);
		System.out.println(path4.toString());
		
		BestPath path5 = airportGraph.getBestPath("MZL", "UAZ", FlightCriteria.CANCELED);
		System.out.println(path5.toString());
		
	}

}
