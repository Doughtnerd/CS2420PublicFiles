package assignment11;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

/**
 * Testing class for assignment 11
 * @author Christopher Carlson
 * @author Spencer Smith
 */
public class PriorityQueueTest {

	ArrayList<String> list;
	PriorityQueue<String> q;
	Random rand = new Random(System.currentTimeMillis());

	PriorityQueue<Integer> intQueue;
	PriorityQueue<String> stringQueue;

	@Test
	public void addTest() { // Tests add and addAll

		Integer[] input1 = { 7, 12, 5, 9, 3, 2, 6, 4, 1, 8 };
		for (int i = 0; i < input1.length; i++) {
			intQueue.add(input1[i]);
		}

		ArrayList<Integer> input2 = new ArrayList<>();
		input2.add(3);
		input2.add(3);
		for (Integer i : input2) {
			intQueue.add(i);
		}

		Integer[] expected = { 1, 2, 3, 4, 3, 3, 6, 12, 5, 9, 8, 7 };
		Object[] result = intQueue.toArray();

		for (int i = 0; i < input1.length + input2.size(); i++) {
			if (result[i] != expected[i]) {
				fail();
			}
		}
	}

	@Before
	public void setup() {
		list = new ArrayList<String>();
		q = new PriorityQueue<>();
		intQueue = new PriorityQueue<>();
		stringQueue = new PriorityQueue<>();
	}

	@Test
	public void testSize() {
		for (int i = 0; i < 1000; i++) {
			q.add(this.randomString(8));
			assertTrue(q.size() == i + 1);
		}
	}

	@Test
	public void testClear() {
		for (int i = 0; i < 1000; i++) {
			q.add(this.randomString(8));
			assertTrue(q.size() == i + 1);
		}
		assertTrue(q.size() == 1000);
		q.clear();
		assertTrue(q.size() == 0);
	}

	@Test(expected = NoSuchElementException.class)
	public void testEmptyRemove() {
		q.deleteMin();
	}

	@Test(expected = NoSuchElementException.class)
	public void testEmptyFind() {
		q.findMin();
	}

	@Test
	public void testFindMin() {
		for (int i = 0; i < 1000; i++) {
			q.add(this.randomString(8));
			assertTrue(q.size() == i + 1);
		}
		q.add("a");
		assertTrue(q.findMin() == "a");
	}

	@Test
	public void testDeleteMinSize10() {
		for (int i = 0; i < 10; i++) {
			q.add(this.randomString(1));
			assertTrue(q.size() == i + 1);
		}
		q.add("a");
		q.generateDotFile("dotFile.dot");
		assertEquals("a", q.findMin());
		q.deleteMin();
		q.generateDotFile("dotFile2.dot");
	}

	@Test
	public void testDeleteMinSmall() {
		try {
			q.deleteMin();
			fail("Did not catch excetion");
		} catch (NoSuchElementException e) {
		}
		q.add("Hello");
		q.add("Hello2");
		assertEquals("Hello", q.deleteMin());
		assertEquals("Hello2", q.deleteMin());
		try {
			q.deleteMin();
			fail("Did not catch excetion");
		} catch (NoSuchElementException e) {
		}
	}

	@Test
	public void testAdd() {
		for (int i = 0; i < 1000; i++) {
			q.add(this.randomString(8));
			assertTrue(q.size() == i + 1);
		}
	}

	@Test
	public void testGenerateDotFile() {
		for (int i = 0; i < 10; i++) {
			q.add(randomString(1));
			assertTrue(q.size() == i + 1);
		}
		q.deleteMin();

	}

	@Test
	public void testToArray() {
		for (int i = 0; i < 1000; i++) {
			String s = this.randomString(8);
			q.add(s);
			list.add(s);
			assertTrue(q.size() == i + 1);
		}
		for (Object s : q.toArray()) {
			assertTrue(list.contains(s));
		}

	}

	/**
	 * Generates a random string of the specified length.
	 * 
	 * @param length
	 *            - Lenght of the string to generate
	 * @return The generated string.
	 */
	public String randomString(int length) {
		String retval = "";
		for (int i = 0; i < length; i++) {
			retval += (char) ('a' + (rand.nextInt(26)));
		}
		return retval;
	}

}
