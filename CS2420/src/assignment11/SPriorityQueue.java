package assignment11;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * Represents a priority queue of generically-typed items. 
 * The queue is implemented as a min heap. 
 * The min heap is implemented implicitly as an array.
 * 
 * @author 
 */
@SuppressWarnings("unchecked")
public class SPriorityQueue<AnyType> {

	private int currentSize;
	private AnyType[] array;
	private Comparator<? super AnyType> cmp;

	/**
	 * Constructs an empty priority queue. Orders elements according
	 * to their natural ordering (i.e., AnyType is expected to be Comparable)
	 * AnyType is not forced to be Comparable.
	 */
	public SPriorityQueue() {
		currentSize = 0;
		cmp = null;
		array = (AnyType[]) new Object[10]; // safe to ignore warning 
	}

	/**
	 * Construct an empty priority queue with a specified comparator.
	 * Orders elements according to the input Comparator (i.e., AnyType need not
	 * be Comparable).
	 */
	public SPriorityQueue(Comparator<? super AnyType> c) {
		currentSize = 0;
		cmp = c;
		array = (AnyType[]) new Object[10]; // safe to ignore warning
	}

	/**
	 * @return the number of items in this priority queue.
	 */
	public int size() {
		return currentSize;
	}

	/**
	 * Makes this priority queue empty.
	 */
	public void clear() {
		currentSize = 0;
	}

	/**
	 * @return the minimum item in this priority queue.
	 * @throws NoSuchElementException if this priority queue is empty.
	 * 
	 * (Runs in constant time.)
	 */
	public AnyType findMin() throws NoSuchElementException {
		if(array.length < 1){
			throw new NoSuchElementException();
		}
		return array[0];
	}


	/**
	 * Removes and returns the minimum item in this priority queue.
	 * 
	 * @throws NoSuchElementException if this priority queue is empty.
	 * 
	 * (Runs in logarithmic time.)
	 */
	public AnyType deleteMin() throws NoSuchElementException {
		
		if(currentSize == 0){
			throw new NoSuchElementException();
		}
		
		AnyType minItem = array[0];
		
		
		
		// FILL IN -- do not return null
		// if the heap is empty, throw a NoSuchElementException
		// store the minimum item so that it may be returned at the end
		// replace the item at minIndex with the last item in the tree
		// update size
		// percolate the item at minIndex down the tree until heap order is restored
		// It is STRONGLY recommended that you write a percolateDown helper method!
		// return the minimum item that was stored

		return null;
	}


	/**
	 * Adds an item to this priority queue.
	 * 
	 * (Runs in logarithmic time.) Can sometimes terminate early.
	 * 
	 * @param x -- the item to be inserted
	 */
	public void add(AnyType x) {

		// Expand array if full
		if (currentSize == array.length) {
			expandArray();
		}

		array[currentSize] = x;
		currentSize++;

		if (currentSize == 1) { // x is root if size is 1. No percolating needed.
			return;
		}

		int itemIndex = currentSize - 1;
		int parentIndex = getParentIndex(itemIndex);

		percolateUp(itemIndex, parentIndex);
	}
	
	/**
	 * Adds all items in a collection to this priority queue.
	 */
	public void addAll(Collection<AnyType> collection) {
		for(AnyType item : collection){
			add(item);
		}
	}
	
	/**
	 * Adds all items in an array to this priority queue.
	 */
	public void addAll(AnyType[] inputArray) {
		for(AnyType item : inputArray){
			add(item);
		}
	}
	
	/**
	 * Percolate the new item up the levels of the tree until heap order is restored.
	 */
	private void percolateUp(int itemIndex, int parentIndex){
		
		int comparison = compare(array[itemIndex], array[parentIndex]);
		
		// Item is greater than it's parent. Leave it.
		if(comparison > 0){
			return;
		}
		
		// Item is root. It's in order. (For recursive call)
		if(itemIndex == 0){
			return;
		}
		
		// Item is smaller than it's parent. Put in place and percolate.
		if(comparison < 0){
			AnyType temp = array[parentIndex];
			array[parentIndex] = array[itemIndex];
			array[itemIndex] = temp;
			
			percolateUp(parentIndex, getParentIndex(parentIndex));
		}
	}
	
	/**
	 * Returns the parent index from a given index.
	 */
	private int getParentIndex(int itemIndex){
		return (itemIndex - 1) / 2;
	}
	
	/**
	 * Doubles the capacity of the array.
	 */
	private void expandArray(){
		AnyType[] newArray = (AnyType[]) new Object[array.length * 2];
		for(int i = 0; i < array.length; i++){
			newArray[i] = array[i];
		}
		array = newArray;
	}
	
	/**
	 * Internal method for comparing lhs and rhs using Comparator if provided by the
	 * user at construction time, or Comparable, if no Comparator was provided.
	 * 
	 * **Turn back private after tests!!**
	 */
	public int compare(AnyType lhs, AnyType rhs) {
		if (cmp == null) {
			return ((Comparable<? super AnyType>) lhs).compareTo(rhs); // safe to ignore warning
		}
		// We won't test your code on non-Comparable types if we didn't supply a Comparator

		return cmp.compare(lhs, rhs);
	}
	
	/**
	 * Generates a DOT file for visualizing the binary heap.
	 */
	public void generateDotFile(String filename) {
		try(PrintWriter out = new PrintWriter(filename)) {
			out.println("digraph Heap {\n\tnode [shape=record]\n");

			for(int i = 0; i < currentSize; i++) {
				out.println("\tnode" + i + " [label = \"<f0> |<f1> " + array[i] + "|<f2> \"]");
				if(((i*2) + 1) < currentSize)
					out.println("\tnode" + i + ":f0 -> node" + ((i*2) + 1) + ":f1");
				if(((i*2) + 2) < currentSize)
					out.println("\tnode" + i + ":f2 -> node" + ((i*2) + 2) + ":f1");
			}
			out.println("}");
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	//LEAVE IN for grading purposes
	public Object[] toArray() {    
		Object[] ret = new Object[currentSize];
		for(int i = 0; i < currentSize; i++) {
			ret[i] = array[i];
		}
		return ret;
	}
}