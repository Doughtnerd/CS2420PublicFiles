package assignment11;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 * Represents a priority queue of generically-typed items. The queue is
 * implemented as a min heap. The min heap is implemented implicitly as an
 * array.
 * 
 * @author Christopher Carlson
 * @author Spencer Smith
 */
@SuppressWarnings("unchecked")
public class PriorityQueue<AnyType> {

	/**
	 * Current size of the heap
	 */
	private int currentSize;

	/**
	 * Data array backing the heap.
	 */
	private AnyType[] array;

	/**
	 * Comparator to use for comparing items.
	 */
	private Comparator<? super AnyType> cmp;

	/**
	 * Constructs an empty priority queue. Orders elements according to their
	 * natural ordering (i.e., AnyType is expected to be Comparable) AnyType is
	 * not forced to be Comparable.
	 */
	public PriorityQueue() {
		currentSize = 0;
		cmp = null;
		array = (AnyType[]) new Object[10]; // safe to ignore warning
	}

	/**
	 * Construct an empty priority queue with a specified comparator. Orders
	 * elements according to the input Comparator (i.e., AnyType need not be
	 * Comparable).
	 */
	public PriorityQueue(Comparator<? super AnyType> c) {
		currentSize = 0;
		cmp = c;
		array = (AnyType[]) new Object[10]; // safe to ignore warning
	}

	/**
	 * @return the number of items in this priority queue.
	 */
	public int size() {
		return currentSize;
	}

	/**
	 * Makes this priority queue empty.
	 */
	public void clear() {
		currentSize = 0;
	}

	/**
	 * @return the minimum item in this priority queue.
	 * @throws NoSuchElementException
	 *             if this priority queue is empty.
	 * 
	 *             (Runs in constant time.)
	 */
	public AnyType findMin() throws NoSuchElementException {
		if (currentSize == 0) {
			throw new NoSuchElementException();
		} else {
			return array[0];
		}
	}

	/**
	 * Removes and returns the minimum item in this priority queue.
	 * 
	 * @throws NoSuchElementException
	 *             if this priority queue is empty.
	 * 
	 *             (Runs in logarithmic time.)
	 */
	public AnyType deleteMin() throws NoSuchElementException {
		// if the heap is empty, throw a NoSuchElementException
		if (currentSize == 0) {
			throw new NoSuchElementException();
		}

		// store the minimum item so that it may be returned at the end
		AnyType temp = array[0];
		// replace the item at minIndex with the last item in the tree
		array[0] = array[currentSize-1];
		// update size
		currentSize--;
		// percolate the item at minIndex down the tree until heap order is
		// restored
		// It is STRONGLY recommended that you write a percolateDown helper
		// method!
		percolateDown(0);
		// return the minimum item that was stored
		return temp;
	}

	/**
	 * Shifts the item at index down until it is less than or equal to both of
	 * its children.
	 * 
	 * @param index
	 *            - The index of the item to shift down the tree.
	 */
	private void percolateDown(int index) {
		while(true){
			int left = getLeftIndex(index);
			int right = getRightIndex(index);
			if(left>=currentSize || right>=currentSize){
				return;
			} else {
				if(compare(array[index], array[left])>0 || compare(array[index], array[right])>0){
					int childComp = compare(array[left], array[right]);
					if(childComp<0){
						swap(index, left);
						index = left;
					} else {
						swap(index, right);
						index = right;
					}
				} else {
					return;
				}
			}
		}
	}

	/**
	 * Adds an item to this priority queue.
	 * 
	 * (Runs in logarithmic time.) Can sometimes terminate early.
	 * 
	 * @param x
	 *            -- the item to be inserted
	 */
	public void add(AnyType x) {
		// if the array is full, double its capacity
		if(x == null){
			return;
		}
		
		if (currentSize + 1 >= array.length) {
			resize(2);
		}
		// add the new item to the next available node in the tree, so that
		// complete tree structure is maintained
		array[currentSize] = x;

		// update size
		this.currentSize++;

		// percolate the new item up the levels of the tree until heap order is
		// restored
		// It is STRONGLY recommended that you write a percolateUp helper
		// method!
		percolateUp(currentSize - 1);

	}

	/**
	 * Shifts the item at index up the tree while the parent is greater than the
	 * item
	 * 
	 * @param index
	 *            - Item to shift up the tree.
	 */
	private void percolateUp(int index) {
		int parent = getParentIndex(index);
		while (index > 0 && compare(array[parent], array[index]) > 0) {
			swap(index, parent);
			index = parent;
			parent = getParentIndex(index);
		}
	}

	/**
	 * Swaps the items it indices index and parent.
	 * 
	 * @param index
	 *            - The higher index item
	 * @param parent
	 *            - The lower index item to swap.
	 */
	private void swap(int index, int parent) {
		AnyType temp = array[parent];
		array[parent] = array[index];
		array[index] = temp;
	}

	/**
	 * Returns the left child index of i.
	 * 
	 * @param i
	 *            - Index to get left child of.
	 * @return The index of the left child
	 */
	private int getLeftIndex(int i) {
		return 2 * i + 1;
	}

	/**
	 * Returns the right child index of i.
	 * 
	 * @param i
	 *            - Index to get right child of.
	 * @return The index of the right child
	 */
	private int getRightIndex(int i) {
		return 2 * i + 2;
	}

	/**
	 * Returns the parent index of i.
	 * 
	 * @param i
	 *            - Index to get parent of.
	 * @return The index of the parent.
	 */
	private int getParentIndex(int i) {
		return (i - 1) / 2;
	}

	/**
	 * Resizes the data array to be the current size*factor if the
	 * currentSize!=0. Else it resizes the array to be of size factor.
	 * 
	 * @param factor
	 *            - Number to multiply size by.
	 */
	private void resize(int factor) {
		int newSize = currentSize == 0 ? factor : currentSize * factor;
		AnyType[] temp = (AnyType[]) new Object[newSize];
		for (int i = 0; i < currentSize; i++) {
			temp[i] = array[i];
		}
		array = temp;
	}

	/**
	 * Generates a DOT file for visualizing the binary heap.
	 */
	public void generateDotFile(String filename) {
		try (PrintWriter out = new PrintWriter(filename)) {
			out.println("digraph Heap {\n\tnode [shape=record]\n");

			for (int i = 0; i < currentSize; i++) {
				out.println("\tnode" + i + " [label = \"<f0> |<f1> " + array[i] + "|<f2> \"]");
				if (((i * 2) + 1) < currentSize)
					out.println("\tnode" + i + ":f0 -> node" + ((i * 2) + 1) + ":f1");
				if (((i * 2) + 2) < currentSize)
					out.println("\tnode" + i + ":f2 -> node" + ((i * 2) + 2) + ":f1");
			}
			out.println("}");
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Internal method for comparing lhs and rhs using Comparator if provided by
	 * the user at construction time, or Comparable, if no Comparator was
	 * provided.
	 */
	private int compare(AnyType lhs, AnyType rhs) {
		if (cmp == null) {
			return ((Comparable<? super AnyType>) lhs).compareTo(rhs);
		}
		// We won't test your code on non-Comparable types if we didn't supply a
		// Comparator

		return cmp.compare(lhs, rhs);
	}

	// LEAVE IN for grading purposes
	public Object[] toArray() {
		Object[] ret = new Object[currentSize];
		for (int i = 0; i < currentSize; i++) {
			ret[i] = array[i];
		}
		return ret;
	}
}