package assignment05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

/**
 * Provides various utility methods for sorting ArrayLists.
 *
 * @author Shaheen Page
 * @author Christopher Carlson
 */
public class SortUtilShaheen {

	/**
	 * When the length of the portion of the list being sorted reaches this
	 * threshold in <tt>recursiveMergesort</tt>, insertion sort is used instead.
	 */
	private static final int insertionSortThreshold = 100;

	/**
	 * Random generator
	 */
	private static final Random rand = new Random();

	/**
	 * Generates and returns an ArrayList of integers of range 1 to size in
	 * ascending order.
	 *
	 * @param size
	 *            desired length of returned ArrayList
	 * @return ArrayList of integers of range 1 to size in ascending order
	 */
	public static ArrayList<Integer> generateBestCase(int size) {

		if (size < 1) {
			return new ArrayList<>();
		}

		ArrayList<Integer> numbers = new ArrayList<>(size);

		for (int n = 1; n <= size; n += 1) {
			numbers.add(n);
		}

		return numbers;

	}

	/**
	 * Generates and returns an ArrayList of integers of range 1 to size in
	 * permuted order (i,e., randomly ordered).
	 *
	 * @param size
	 *            desired length of returned ArrayList
	 * @return ArrayList of integers of range 1 to size in permuted order
	 */
	public static ArrayList<Integer> generateAverageCase(int size) {

		if (size < 1) {
			return new ArrayList<>();
		}

		ArrayList<Integer> numbers = generateBestCase(size);

		Collections.shuffle(numbers);

		return numbers;

	}

	/**
	 * Generates and returns an ArrayList of integers 1 to size in descending
	 * order.
	 *
	 * @param size
	 *            desired length of returned ArrayList
	 * @return ArrayList of integers of range 1 to size in descending order
	 */
	public static ArrayList<Integer> generateWorstCase(int size) {

		if (size < 1) {
			return new ArrayList<>();
		}

		ArrayList<Integer> numbers = new ArrayList<>(size);

		for (int n = size; n >= 1; n -= 1) {
			numbers.add(n);
		}

		return numbers;

	}

	/**
	 * Merge sort driver method that does error checking as well as initializes
	 * the temporary ArrayList for merge space before calling the recursive
	 * merge sort method.
	 *
	 * @param list
	 *            ArrayList on which merge sort will be performed
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 */
	public static <T> void mergesort(ArrayList<T> list, Comparator<? super T> comparator) {

		if (list == null || list.size() <= 1 || comparator == null) {
			return;
		}

		ArrayList<T> temp = new ArrayList<>(list.size());

		for (int i = 0; i < list.size(); i++) {
			temp.add(null);
		}

		recursiveMergesort(list, temp, comparator, 0, list.size() - 1);

	}

	/**
	 * Performs a recursive merge sort on <tt>list</tt> using
	 * <tt>comparator</tt> for ordering within the bounds defined by
	 * <tt>left</tt> and <tt>right</tt>. If the range between the bounds reaches
	 * <tt>insertionSortThreshold</tt>, an insertion sort is instead performed
	 * on <tt>list</tt>.
	 *
	 * @param list
	 *            ArrayList on which merge sort will be performed
	 * @param temp
	 *            temporary ArrayList for merge space
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 * @param left
	 *            bound at which to start the merge sort
	 * @param right
	 *            bound at which to stop the merge sort
	 */
	private static <T> void recursiveMergesort(ArrayList<T> list, ArrayList<T> temp, Comparator<? super T> comparator,
			int left, int right) {

		if (left >= right) {
			return;
		}

		if (right - left + 1 <= insertionSortThreshold) {
			insertionSort(list, comparator, left, right);
			return;
		}

		int mid = (left + right) / 2;

		recursiveMergesort(list, temp, comparator, left, mid);
		recursiveMergesort(list, temp, comparator, mid + 1, right);
		merge(list, temp, comparator, left, mid + 1, right);

	}

	/**
	 * Performs a merge of two sections of <tt>list</tt> defined by the bounds:
	 * <tt>leftIndex</tt>, <tt>rightIndex</tt>, and <tt>rightBound</tt>.
	 *
	 * @param list
	 *            ArrayList containing the two specified sections to be merged
	 * @param temp
	 *            temporary ArrayList used to merge the two sections
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 * @param leftIndex
	 *            index of the first item in the first section of <tt>list</tt>
	 *            to be merged
	 * @param rightIndex
	 *            index of the first item in the second section of <tt>list</tt>
	 *            to be merged
	 * @param rightBound
	 *            bound at which to stop the merge (last item in the second
	 *            section to be merged)
	 */
	@SuppressWarnings("unchecked")
	private static <T> void merge(ArrayList<T> list, ArrayList<T> temp, Comparator<? super T> comparator, int leftIndex,
			int rightIndex, int rightBound) {

		int leftBound = rightIndex - 1;
		int tempIndex = leftIndex;
		int length = rightBound - leftIndex + 1;

		while (leftIndex <= leftBound && rightIndex <= rightBound) {

			T leftEl = list.get(leftIndex);
			T rightEl = list.get(rightIndex);
			if (comparator != null) {
				if (comparator.compare(leftEl, rightEl) <= 0) {
					temp.set(tempIndex, leftEl);
					leftIndex += 1;
				} else {
					temp.set(tempIndex, rightEl);
					rightIndex += 1;
				}
			} else {
				if (((Comparable<T>) leftEl).compareTo(rightEl) <= 0) {
					temp.set(tempIndex, leftEl);
					leftIndex += 1;
				} else {
					temp.set(tempIndex, rightEl);
					rightIndex += 1;
				}
			}
			tempIndex += 1;

		}

		while (leftIndex <= leftBound) {
			temp.set(tempIndex, list.get(leftIndex));
			leftIndex += 1;
			tempIndex += 1;
		}

		while (rightIndex <= rightBound) {
			temp.set(tempIndex, list.get(rightIndex));
			rightIndex += 1;
			tempIndex += 1;
		}

		for (int i = 0; i < length; i += 1) {
			list.set(rightBound, temp.get(rightBound));
			rightBound -= 1;
		}

	}

	/**
	 * Quick sort driver method that does error checking before calling the
	 * recursive quick sort method.
	 *
	 * @param list
	 *            ArrayList on which merge sort will be performed
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 */
	public static <T> void quicksort(ArrayList<T> list, Comparator<? super T> comparator) {

		if (list == null || list.size() <= 1 || comparator == null) {
			return;
		}

		recursiveQuicksort(list, comparator, 0, list.size() - 1);

	}

	/**
	 * Performs an insertion sort on <tt>list</tt> using <tt>comparator</tt> for
	 * ordering within the bounds defined by <tt>left</tt> and <tt>right</tt>.
	 *
	 * @param list
	 *            ArrayList on which insertion sort will be performed
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 * @param left
	 *            bound at which to start the insertion sort
	 * @param right
	 *            bound at which to stop the insertion sort
	 */
	@SuppressWarnings("unchecked")
	private static <T> void insertionSort(ArrayList<T> list, Comparator<? super T> comparator, int left, int right) {

		for (int i = left + 1; i <= right; i += 1) {

			T temp = list.get(i);
			int j = i;

			if (comparator != null) {
				while (j > left && comparator.compare(list.get(j - 1), temp) > 0) {
					list.set(j, list.get(j - 1));
					j -= 1;
				}
			} else {
				while (j > left && ((Comparable<T>)list.get(j - 1)).compareTo(temp) > 0) {
					list.set(j, list.get(j - 1));
					j -= 1;
				}
			}
			list.set(j, temp);

		}

	}

	/**
	 * Performs a recursive quick sort on <tt>list</tt> using
	 * <tt>comparator</tt> for ordering within the bounds defined by
	 * <tt>left</tt> and <tt>right</tt>. If the range between the bounds reaches
	 * <tt>insertionSortThreshold</tt>, an insertion sort is instead performed
	 * on <tt>list</tt>.
	 *
	 * @param list
	 *            ArrayList on which quick sort will be performed
	 * @param comparator
	 *            used to sort the items in <tt>list</tt>
	 * @param left
	 *            bound at which to start the quick sort
	 * @param right
	 *            bound at which to end the quick sort
	 */
	private static <T> void recursiveQuicksort(ArrayList<T> list, Comparator<? super T> comparator, int left,
			int right) {

		if (left >= right) {
			return;
		}

		if (right - left + 1 <= insertionSortThreshold) {
			insertionSort(list, comparator, left, right);
			return;
		}

		// int pivotIndex = medianOfThreePivot(list, comparator, left, right);
		// int pivotIndex = (new Random().nextInt((right-left)+1)) + left;
		int pivotIndex = (left + right) >> 1;
		T pivot = list.get(pivotIndex);

		int leftBound = left;
		int rightBound = right;

		while (leftBound <= rightBound) {

			while (comparator.compare(list.get(leftBound), pivot) < 0) {
				leftBound += 1;
			}

			while (comparator.compare(list.get(rightBound), pivot) > 0) {
				rightBound -= 1;
			}

			if (leftBound <= rightBound) {
				swap(list, leftBound, rightBound);
				leftBound += 1;
				rightBound -= 1;
			}

		}

		if (left < rightBound) {
			recursiveQuicksort(list, comparator, left, rightBound);
		}

		if (right > leftBound) {
			recursiveQuicksort(list, comparator, leftBound, right);
		}

	}

	/**
	 * Returns an integer somewhere between the two specified bounds:
	 * <tt>lowBound</tt> and <tt>highBound</tt>. The integer is chosen by
	 * finding the median of three random elements from <tt>list</tt>, and
	 * returning the index of that item in <tt>list</tt>.
	 *
	 * @param list
	 *            ArrayList used to chose the three random elements
	 * @param comparator
	 *            used to sort the three random elements
	 * @param lowBound
	 *            integer representing the low bound of the range
	 * @param highBound
	 *            integer representing the high bound of the range
	 * @return integer somewhere between <tt>lowBound</tt> and
	 *         <tt>highBound</tt>
	 */
	@SuppressWarnings("unused")
	private static <T> int medianOfThreePivot(ArrayList<T> list, Comparator<? super T> comparator, int lowBound,
			int highBound) {

		T item1 = list.get(lowBound + rand.nextInt((highBound - lowBound) + 1));
		T item2 = list.get(lowBound + rand.nextInt((highBound - lowBound) + 1));
		T item3 = list.get(lowBound + rand.nextInt((highBound - lowBound) + 1));
		int compared12 = comparator.compare(item1, item2);
		int compared13 = comparator.compare(item1, item3);
		int compared23 = comparator.compare(item2, item3);

		// cases: 2 <= 1 <= 3 or 3 <= 1 <= 2
		if ((compared12 >= 0 && compared13 <= 0) || (compared13 >= 0 && compared12 <= 0)) {
			return list.indexOf(item1);

			// cases: 1 <= 2 <= 3 or 3 <= 2 <= 1
		} else if ((compared12 <= 0 && compared23 <= 0) || (compared23 >= 0 && compared12 >= 0)) {
			return list.indexOf(item2);

			// cases: 1 <= 3 <= 2 or 2 <= 3 <= 1
		} else {
			return list.indexOf(item3);
		}

	}

	/**
	 * Swaps two elements in <tt>list</tt>, given their indices:
	 * <tt>leftIndex</tt> and <tt>rightIndex</tt>.
	 *
	 * @param list
	 *            ArrayList in which the swap will be performed
	 * @param leftIndex
	 *            the index in <tt>list</tt> of one of the items to be swapped
	 * @param rightIndex
	 *            the index in <tt>list</tt> of one of the items to be swapped
	 */
	private static <T> void swap(ArrayList<T> list, int leftIndex, int rightIndex) {

		T temp = list.get(leftIndex);

		list.set(leftIndex, list.get(rightIndex));
		list.set(rightIndex, temp);

	}

	static class StringCompare implements Comparator<String> {

		@Override
		public int compare(String lhs, String rhs) {
			return lhs.compareTo(rhs);
		}

	}

	static class IntegerCompare implements Comparator<Integer> {

		@Override
		public int compare(Integer lhs, Integer rhs) {
			return lhs - rhs;
		}

	}

}