package assignment05;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

/**
 * 
 * @author Christopher Carlson
 *
 */
public class SortUtil {

	private static final int THRESHOLD = 30;
	
	/*
	 * While your implementation of mergesort and quicksort may seem correct,
	 * minor implementation issues can cause quadratic performance. Follow these
	 * guidelines to avoid an incorrect implementation.
	 * 
	 * Avoid any costly O(N) ArrayList operations, such as: indexOf remove
	 * contains addAll For mergesort, pre-allocate a temporary list for merge
	 * space once in the driver method.
	 * 
	 * Avoid allocating and creating enough size for it during every recursive
	 * call. Mergesort should never have quadratic performance, unless the
	 * insertion sort threshold is very large. When the insertion sort threshold
	 * is reached during mergesort, make sure you only insertion-sort the
	 * relevent portion of the list. Your insertion sort method will have to be
	 * modified to only work on a certain range (i.e., take a left and right
	 * index, and only sort between those indices), instead of sorting the
	 * entire array. Quicksort should never have quadratic performance with a
	 * decent pivot selection, such as picking a random index.
	 */

	/*
	 * For the mergesort algorithm, see the class notes and/or the textbook.
	 * There is pseudo code in the slides. Your mergesort implementation must
	 * switch over to insertion sort when the size of the sublist to be sorted
	 * meets a certain threshold (i.e., becomes small enough). Make this
	 * threshold value a private static variable that you can easily change. You
	 * will perform experiments to determine which threshold value works best
	 * (see the Analysis Document).
	 * 
	 * Don't forget to include the insertion sort in the program files you
	 * submit.
	 */

	public static void main(String[] args) {
		ArrayList<Integer> list = generateWorstCase(200);
		mergesort(list, null);
		for(int i = 0; i < 200; i++){
			if(i%10==0 && i!=0){
				System.out.println();
			}
			System.out.print(list.get(i) + " ");
			
		}
	}

	/**
	 * This driver method performs a mergesort on the generic ArrayList given as input for
	 * N > ****** and uses insertion sort for N<*******.
	 * If comparator is null, sorts based on natural order, otherwise, uses the
	 * comparator.
	 * 
	 * @param list
	 *            ArrayList to sort.
	 * @param comparator
	 *            Comparator to use.
	 */
	public static <T> void mergesort(ArrayList<T> input, Comparator<? super T> comparator) {
		if (input == null || input.size() == 1) {
			return;
		}
		@SuppressWarnings("unchecked")
		T[] temp = (T[]) new Object[input.size()];
		mergesort(input, temp, 0, input.size() - 1, comparator);
		

	}

	/**
	 * This is the recursive mergesort method. It splits the array down as small
	 * as possible, then mergest them back together as it travels back through
	 * the stack.
	 * 
	 * @param input
	 *            The input ArrayList to sort.
	 * @param temp
	 *            The temporary T array that will be used to help sort.
	 * @param left
	 *            The left bound.
	 * @param right
	 *            The right bound
	 * @param comparator
	 *            The comparator to use for comparisons. If null, uses natural
	 *            order.
	 */
	private static <T> void mergesort(ArrayList<T> input, T[] temp, int left, int right,
			Comparator<? super T> comparator) {
		if (left < right) {
			int mid = left + (right - left) / 2;
			mergesort(input, temp, left, mid, comparator);
			mergesort(input, temp, mid + 1, right, comparator);
			if(mid>=THRESHOLD){
				merge(input, temp, left, mid, right, comparator);
			} else {
				insertionSort(input, left, right, comparator);
			}
			
		}
	}

	/**
	 * This method merges the sub-arrays back together in a sorted order and inserts the correct
	 * values back into the input array.
	 * @param input
	 *            The input ArrayList to sort.
	 * @param temp
	 *            The temporary T array that will be used to help sort.
	 * @param left
	 *            The left bound.
	 * @param right
	 *            The right bound
	 * @param comparator
	 *            The comparator to use for comparisons. If null, uses natural
	 *            order.
	 */
	@SuppressWarnings("unchecked")
	private static <T> void merge(ArrayList<T> input, T[] temp, int left, int mid, int right,
			Comparator<? super T> comparator) {
		
		/*
		 * Put the sub-array into temp, this is likely the cause of the N complexity.
		 */
		/*
		for(int i = left; i <=right;i++){
			temp[i]= input.get(i);
		}
		*/
		
		/*
		 * Define walking indices and insertion point.
		 */
		int i1 = left, i2 = mid+1, index = left;
		
		/*
		 * Sort the smallest of temp into the input list using the index insertion point.
		 */
		while (i1 <= mid && i2 <= right) {
			if (comparator != null) {
				if (comparator.compare(input.get(i1), input.get(i2)) <= 0) {
					temp[index] = input.get(i1);
					i1++;
				} else {
					temp[index] = input.get(i2);
					i2++;
				}
			} else {
				if (((Comparable<T>) input.get(i1)).compareTo(input.get(i2)) <= 0) {
					temp[index] = input.get(i1);
					i1++;
				} else {
					temp[index] = input.get(i2);
					i2++;
				}
			}
			index++;
		}
		
		/*
		 * Copy what's left from the larger array back into input.
		 */
		while (i1 <= mid) {
			temp[index]=input.get(i1);
			i1++;
			index++;
		}
		while (i2 <= right) {
			temp[index] = input.get(i2);
			i2++;
			index++;
		}
		
		/*
		 * Copy temp back into index.
		 */
		int range = right-left+1;
		for(int i = 0; i < range; i++, right--)
            input.set(right, temp[right]);
	}

	/**
	 * Insertion sort algorithm to use when N is small enough.
	 * @param list 
	 * @param comparator
	 */
	@SuppressWarnings("unchecked")
	private static <T> void insertionSort(ArrayList<T> input, int left, int right, Comparator<? super T> comparator) {
		if (input == null || input.size() == 0 || input.size() == 1) {
			return;
		}
		for (int i = left; i <= right; i++) {
			T key = input.get(i);
			int j = i;
			if (comparator != null) {
				while (j > 0 && comparator.compare(key, input.get(j-1)) < 0) {
					T tempElement = input.get(j-1);
					input.set(j-1, key);
					input.set(j, tempElement);
					j--;
				}
			} else {
				while (j > 0 && ((Comparable<T>) key).compareTo(input.get(j-1)) < 0) {
					T tempElement = input.get(j-1);
					input.set(j-1, key);
					input.set(j, tempElement);
					j--;
				}
			}
		}
	}
	
	
	/*
	 * For the quicksort algorithm, see the class notes and/or the textbook.
	 * There is pseudo code in the slides. You must implement three different
	 * strategies for determining the pivot. Your quicksort implementation
	 * should be able to easily switch among these strategies. (Consider using a
	 * few private helper methods for your different pivot selection
	 * strategies.) You will perform experiments to determine which pivot
	 * strategy works best (see the Analysis Document). Your quicksort may also
	 * switch to insertion sort on some small threshold if you wish.
	 */

	/**
	 * This method performs a quicksort on the generic ArrayList given as input.
	 * If comparator is null, uses natural order to sort the elements,
	 * otherwise, uses the comparator.
	 * 
	 * @param list
	 *            The ArrayList to sort.
	 * @param comparator
	 *            The comparator to use.
	 */
	public static <T> void quicksort(ArrayList<T> input, Comparator<? super T> comparator) {
		if(input == null || input.size()==1){
			return;
		}
	}

	/**
	 * This method generates and returns an ArrayList of integers 1 to size in
	 * ascending order.
	 * 
	 * @param size
	 * @return
	 */
	public static ArrayList<Integer> generateBestCase(int size) {
		ArrayList<Integer>result = new ArrayList<>();
		for(int i = 0; i < size; i++){
			result.add(i);
		}
		return result;
	}

	/**
	 * This method generates and returns an ArrayList of integers 1 to size in
	 * permuted order (i,e., randomly ordered).
	 * 
	 * @param size
	 * @return
	 */
	public static ArrayList<Integer> generateAverageCase(int size) {
		ArrayList<Integer> result = new ArrayList<>();
		Random rand = new Random(System.currentTimeMillis());
		for(int i = 0; i < size; i++){
			result.add(rand.nextInt());
		}
		return result;
	}

	/**
	 * This method generates and returns an ArrayList of integers 1 to size in
	 * descending order.
	 * 
	 * @param size
	 * @return
	 */
	public static ArrayList<Integer> generateWorstCase(int size) {
		ArrayList<Integer>result = new ArrayList<>();
		for(int i = size; i >0; i--){
			result.add(i);
		}
		return result;
	}
}
