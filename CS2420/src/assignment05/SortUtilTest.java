package assignment05;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class SortUtilTest {

	@Test
	public void test() {
		ArrayList<Integer> arr = SortUtil.generateWorstCase(1024);
		
		SortUtil.mergesort(arr, null);
		
		System.out.println(arr);
		for(int i = 0; i < arr.size(); i++){
			assertEquals(new Integer(i+1), arr.get(i));
			
		}
	}

}
