package obstacles;

import java.io.File;
import java.net.URL;

import exceptions.CommandoException;
import secretcodes.CommandConstants;

public class FileObstacle extends Obstacle {
	
	String absFile, relFile, cpFile;
	public FileObstacle(String absoluteFile, String relativeFile, String classpathFile) {
		absFile = absoluteFile;
		relFile = relativeFile;
		cpFile = classpathFile;
	}

	@Override
	protected void work() {
		checkAbsoluteFile();
		checkRelativeFile();
		checkCPFile();
	}

	private void checkCPFile() {
		if(absoluteCheck(cpFile)) {
			throw new CommandoException("The classpath file shouldn't be an absolute path!");
		}
		URL url = getClass().getClassLoader().getResource(cpFile);
		if(url == null) {
			throw new CommandoException("I couldn't the resource '" + cpFile + "' on the class path!");
		}
	}

	private void checkRelativeFile() {
		if(absoluteCheck(relFile)) {
			throw new CommandoException("The relative path file name looks like an absolute path!");
		}
		File checkFile = new File(relFile);
		if(!checkFile.exists()) {
			throw new CommandoException("The path looks relative...but couldn't find the file '" + checkFile + "'");
		}
	}

	private void checkAbsoluteFile() {
		if(!absoluteCheck(absFile)) {
			throw new CommandoException("The absolute path isn't absolute!");
		}
		File checkFile = new File(absFile);
		if(!checkFile.exists()){ 
			throw new CommandoException("Seems like a valid path...but I can't find the file '" + absFile + '"');
		}
	}

	private boolean absoluteCheck(String filename) {
		if(isWindows()) {
			return filename.substring(1,3).equals(":\\");
		} else {
			return filename.startsWith("/");
		}
	}

	private boolean isWindows() {
		return System.getProperty("path.separator").equals(";");
	}

	@Override
	protected String getSNPassword() {
		return CommandConstants.PW4;
	}

}
