package bootcamp;

import java.io.Console;

import dottag.DotTag;
import exceptions.CommandoException;
import secretcodes.CommandConstants;

public class Bootcamp {
	Console console;
	
	public static void main(String[] args) {
		Bootcamp camp = new Bootcamp();
		camp.run();
	}
	
	public Bootcamp() {
		console = System.console();
		if(console == null) {
			System.out.println("Remember, you have to use this in a terminal!");
			System.exit(1);
		}
	}
	
	public void run() {
		System.out.println("Quick test to see if you've been paying attention.");
		mkdir();
		changeDirectory();
		javac();
		runjava();
		javap();
		jar();
		createDotTag();
	}
	
	private void mkdir() {
		input("Incorrect.", "mkdir", "How do you create a directory using the command line?");
	}

	private void input(String failure, String answer, String prompt) {
		while(!console.readLine(prompt + "\nAnswer: ").equals(answer)) {
			System.out.println(failure);
		}
		System.out.println("----");
	}

	private void createDotTag() {
		System.out.println("Well done. Fill out the following information to recieve your dot-tag.");
		String name = null, uid = null;
		do {
			name = console.readLine("Name: ");
			uid = console.readLine("Uid: ");
		} while(console.readLine("Is this information correct (Y/n):"
				+ "\nName = " + name +"\n"
				+ "uid = " + uid + "\n").equals("n"));
		DotTag.createDottag(".tag", name, uid).addSerialNumber(CommandConstants.PW1);
		System.setProperty("dottag", ".tag");
	}

	private void jar() {
		input("Sorry! That is not the jar command.", "jar", "What command do you use to package up .class files?");
	}

	private void javap() {
		input("That's not the command for the java disassembler.", "javap", "What command do you use to inspect class files?");
	}

	private void runjava() {
		input("Incorrect", "java", "What command is used to launch the Java Virtual Machine?");
	}

	private void javac() {
		input("Incorrect. Remember, this is the compilation step.", "javac", "What is the command used convert Java source code into Java byte code?");
	}

	private void changeDirectory() {
		input("Incorrect.", "cd", "what is the command used to change directories on the command line?");
	}
	
	public boolean alreadyDoneBootcamp() {
		String dottagProperty = System.getProperty("dottag");
		if(dottagProperty != null) {
			try {
				new DotTag(dottagProperty);
				return true;
			} catch(CommandoException e) {
				return false;
			}
		}
		return false;
	}
}
