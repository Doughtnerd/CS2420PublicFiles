package obstacles;

import java.io.File;

import exceptions.CommandoException;
import secretcodes.CommandConstants;

public class JarObstacle extends Obstacle{
	@Override
	protected void work() {
		String classPath = System.getProperty("java.class.path");
		String[] paths = classPath.split(File.pathSeparator);
		if(!containsLab10(paths)) {
			throw new CommandoException("You didn't make a Lab 10 jar file, or put it on your path!");
		}
	}

	@Override
	protected String getSNPassword() {
		return CommandConstants.PW3;
	}
	
	private static boolean containsLab10(String[] paths) {
		boolean found = false;
		for(String path : paths) {
			if(path.contains("lab10.jar")) {
				found = true;
			}
		}
		return found;
	}
}
