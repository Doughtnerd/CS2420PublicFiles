package obstacles;

import equipment.keyboard.Keyboard;
import equipment.mouse.Mouse;
import exceptions.CommandoException;
import secretcodes.CommandConstants;

@SuppressWarnings("unchecked")
public class EquipmentObstacle extends Obstacle{
	private String[] mice = new String[] {"DeathAdder", "G502", "M65"};
	private String[] keyboards = new String[] {"CMStormQuickfire", "LogitechK480", "RazerBlackWidow"};

	private void checkKeyboard() {
		String PACKAGE = "equipment.keyboard.";
		boolean found = false;
		for(String keyboardClass : keyboards) {
			try {
				Class<Keyboard> clazz = (Class<Keyboard>) Class.forName(PACKAGE + keyboardClass);
				Keyboard keyboard = clazz.newInstance();
				printKeyboardInfo(keyboard);
				if(!found) {
					found = true;
				} else {
					throw new CommandoException("You can't take more than one keyboard! What are you crazy?!");
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// didn't find or construct this class! this class!
			}
		}
	}
	
	private void printKeyboardInfo(Keyboard keyboard) {
		System.out.println("Excellent choice! You're taking the " + keyboard.getType() + " keyboard...and you've named it " + keyboard.getName() + ".");
		if(keyboard.isMechanical()) {
			System.out.println("I see that you prefer mechanical keyboards!");
		} else {
			System.out.println("Keeping it simple with the non-mechanical keyboard, nice.");
		}
	}
	
	
	private void checkMouse() {
		String PACKAGE = "equipment.mouse.";
		boolean found = false;
		for(String mouseClass : mice) {
			try {
				Class<Mouse> clazz = (Class<Mouse>) Class.forName(PACKAGE + mouseClass);
				Mouse mouse = clazz.newInstance();
				writeMouseDetails(mouse);
				if(!found) {
					found = true;
				} else {
					throw new CommandoException("You can't take more than one mouse! What are you, crazy?!");
				}
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// didn't find or construct this class!
			}
		}
	}

	private void writeMouseDetails(Mouse mouse) {
		System.out.println("Looks like you favor the " + mouse.getType() + ", weighing in at " + mouse.getWeight() + "g.\nAnd you're calling it " + mouse.getName() + " how cute!");
	}

	@Override
	protected void work() {
		checkKeyboard();
		checkMouse();
	}

	@Override
	protected String getSNPassword() {
		return CommandConstants.PW2;
	}
}
