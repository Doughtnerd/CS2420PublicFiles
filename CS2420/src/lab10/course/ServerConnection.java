package connection;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;

import com.google.gson.Gson;

public class ServerConnection {
	
	private static final String BASE_URL = "http://cs2420-lab.eng.utah.edu";


	private static class BaseRequest {};
	public static class LabRequest extends BaseRequest {
		public String submission;
	}
	
	public static class Response {
		
	}
	
	public static void submitSerialNumber(LabRequest request) {
		Response response = httpPostCall("/post/lab10", request, LabRequest.class, Response.class);
		if(response != null) {
			System.out.println("Go check your score!");
		} else {
			System.out.println("You might have to do this manually");
		}
		
	}
	
	protected static <E, T extends BaseRequest> E httpPostCall(String url, T requestObject, Class<T> requestClass, Class<E> responseClass) {
        HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(createCookies()).build();
        HttpPost request = new HttpPost(BASE_URL + url);
        request.addHeader("Content-type","application/json");
        String responseAsJSONString = null;
        Gson gson = new Gson();
        String params = gson.toJson(requestObject, requestClass);
        try {
        	request.setEntity(new StringEntity(params));
        	HttpResponse response = client.execute(request);
        	if(!validResponse(response)) {
        		System.out.println("You request failed with status code: " + response.getStatusLine().getStatusCode());
        		return null;
        	}
        } catch (IOException e) {
        	System.out.println("Something went wrong while trying to talk to server.");
        	System.err.println(e.getMessage());
        	return null;
        }
        return gson.fromJson(responseAsJSONString, responseClass);
    }
	
	private static BasicCookieStore createCookies() {
		BasicCookieStore cookieStore = new BasicCookieStore();
		BasicClientCookie uidCookie = new BasicClientCookie("uid", System.getProperty("uid"));
		
		uidCookie.setPath("/");
		uidCookie.setDomain("cs2420-lab.eng.utah.edu");
		cookieStore.addCookie(uidCookie);
		return cookieStore;
	}


	private static boolean validResponse(HttpResponse response) throws ParseException, IOException {
		return response.getStatusLine().getStatusCode() == 200;
	}
}
