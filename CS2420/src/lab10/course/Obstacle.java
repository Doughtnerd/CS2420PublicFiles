package obstacles;

import dottag.DotTag;

public abstract class Obstacle {
	
	public void complete() {
		work();
		writeDogTag();
	}
	
	private void writeDogTag() {
		DotTag dottag = new DotTag(System.getProperty("dottag"));
		dottag.addSerialNumber(getSNPassword());
	}
	
	protected abstract void work();
	protected abstract String getSNPassword();
}
