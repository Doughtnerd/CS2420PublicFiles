package main;

import java.util.ArrayList;
import java.util.List;

import bootcamp.Bootcamp;
import connection.ServerConnection;
import connection.ServerConnection.LabRequest;
import dottag.DotTag;
import exceptions.CommandoException;
import obstacles.ArgumentObstacle;
import obstacles.EquipmentObstacle;
import obstacles.FileObstacle;
import obstacles.JarObstacle;
import obstacles.Obstacle;
import secretcodes.CommandConstants;

public class ObstacleCourse {
	public static void main(String[] args) {
		setup();
		Bootcamp bootcamp = new Bootcamp();
		
		if(!bootcamp.alreadyDoneBootcamp()) { // skip this with valid dottag file specified as a System argument.
			bootcamp.run();
		}
		
		List<Obstacle> course = buildObstacles(args);
		boolean failed = false;
		for(Obstacle obstacle : course) {
			try{
				obstacle.complete();
			} catch (CommandoException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
				failed = true;
				break;
			}
			System.out.println(obstacle.getClass().getSimpleName() + " cleared!");
		}
		if(!failed) {
			submitForPoints();
		}
	}

	private static void submitForPoints() {
		LabRequest req = new LabRequest();
		DotTag dottag = new DotTag(System.getProperty("dottag"));
		req.submission = dottag.getSerialNumber();
		ServerConnection.submitSerialNumber(req);
	}

	private static void setup() {
		CommandConstants.init();
	}

	private static List<Obstacle> buildObstacles(String[] args) {
		List<Obstacle> obstacles = new ArrayList<>();
		obstacles.add(new ArgumentObstacle(args));
		obstacles.add(new FileObstacle(args[0], args[1], args[2]));
		obstacles.add(new JarObstacle());
		obstacles.add(new EquipmentObstacle());
		return obstacles;
	}
}
