package obstacles;

import exceptions.CommandoException;
import secretcodes.CommandConstants;

public class ArgumentObstacle extends Obstacle{

	private String[] args;
	public ArgumentObstacle(String... args) {
		this.args = args;
	}
	
	@Override
	protected void work() {
		if(this.args.length != 3) {
			throw new CommandoException("Incorrect number of arguments. Recieved " + args.length + " but expected 3.");
		}
		String property = null;
		if((property = System.getProperty("uid")) == null) {
			throw new CommandoException("Remember to pass in your uid as a system argument.");
		}
		
		if((property = System.getProperty("dottag")) == null) {
			throw new CommandoException("Remember to pass in your dottag file as a system argument.");
		}
		if(!property.equals(".tag")) {
			System.err.println("It would've been cooler to call your DotTag file \".tag\"");
		}
	}

	@Override
	protected String getSNPassword() {
		return CommandConstants.PW5;
	}
}
