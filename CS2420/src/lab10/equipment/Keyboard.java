package equipment.keyboard;

public abstract class Keyboard {
	protected boolean isMechanical;
	
	public abstract String getType();
	public abstract String getName();
	public abstract String getDescription();
	public boolean isMechanical() {
		return isMechanical;
	}
}
