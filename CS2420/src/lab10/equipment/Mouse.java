package equipment.mouse;

public abstract class Mouse {
	protected int weight;
	public abstract String getName();
	public abstract String getType();
	public abstract String getDecription();
	
	public int getWeight() {
		return weight;
	}
}
