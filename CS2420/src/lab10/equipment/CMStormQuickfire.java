package equipment.keyboard;

public class CMStormQuickfire extends Keyboard{


	public CMStormQuickfire() {
		isMechanical = false;
	}

	@Override
	public String getType() {
		return "Tallion";
	}

	@Override
	public String getName() {
		// give your keyboard a cool name!
		return null;
	}

	@Override
	public String getDescription() {
		return "A serious keyboard for serious developers";
	}

}
