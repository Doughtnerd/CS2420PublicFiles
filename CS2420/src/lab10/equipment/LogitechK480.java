package equipment.keyboard;

public class LogitechK480 extends Keyboard {

	public LogitechK480() {
		isMechanical = false;
	}
	
	@Override
	public String getType() {
		return "Logitech K480 Bluetooth multi-device";
	}

	@Override
	public String getName() {
		//give your keyboard a cool name!
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "For the developer on the go, this keyboard will work with any device you need it to.";
	}

}
