package equipment.mouse;

public class DeathAdder extends Mouse {

	public DeathAdder() {
		weight = 105;
	}
	@Override
	public String getName() {
		// give your mouse a cool name!
		return "Kraal, Warrior King";
	}

	@Override
	public String getType() {

		return "Razor Death Adder";
	}

	@Override
	public String getDecription() {
		return "This is the most standard, by-the-books gaming mouse out there";
	}
}
