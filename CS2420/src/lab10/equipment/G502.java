package equipment.mouse;

public class G502 extends Mouse{

	public G502() {
		weight = 169;
	}
	@Override
	public String getName() {
		return null; // give your mouse a name!
	}

	@Override
	public String getType() {
		
		return "Logitech G502 Proteus Spectrum";
	}

	@Override
	public String getDecription() {
		return "A perfect all-purpose mouse with ELEVEN button. Weight is adjustable!";
	}
	
}
