package equipment.mouse;

public class M65 extends Mouse {

	public M65() {
		weight = 164;
	}
	
	@Override
	public String getName() {
		// give your mouse a cool name!
		return null;
	}

	@Override
	public String getType() {
		return "Corsiar Vengeance M65";
	}

	@Override
	public String getDecription() {
		return "If you're a lefty, Mac user, or FPS player...this is the mouse for you.";
	}

}
