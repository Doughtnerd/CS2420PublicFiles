package equipment.keyboard;

public class RazerBlackWidow extends Keyboard {

	public RazerBlackWidow() {
		isMechanical = true;
	}
	@Override
	public String getType() {
		return "Razer Black Widow";
	}
	
	@Override
	public String getName() {
		// Give your keyboard a cool name.
		return null;
	}
	@Override
	public String getDescription() {
		return "The Razer BlackWidow has been consistently redefining gaming excellence, securing its position as the best-selling gaming keyboard in the world and the clear choice of pro gamers everywhere.";
	}

}
