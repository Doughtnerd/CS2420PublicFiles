package graphUtils;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class View extends JFrame {

	private JPanel panel;
	
	public View(){
		this.setTitle("View");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 800);
		
		panel = new JPanel();
		
		this.add(panel);
	}
	
}
