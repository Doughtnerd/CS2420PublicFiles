package graphUtils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

public abstract class GraphElement {

	private Shape border;
	private double x;
	private double y;
	
	public void draw (Graphics2D g)
    {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (border == null)
        {
            border = getOutline();
        }
        g.draw(border);
    }
	
	public abstract Shape getOutline();
	
}
