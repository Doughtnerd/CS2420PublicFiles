package graphUtils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class GraphNode implements MouseListener{

	private Shape outline;
	private double x;
	private double y;
	private double radius;
	private Color color;
	
	public GraphNode(double x, double y, double radius, Color color){
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
	}
	
	public void setOutline(Shape newOutline){
		this.outline = newOutline;
	}
	
	public void setX(double x){
		this.x = x;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public void setRadius(double radius){
		this.radius = radius;
	}
	
	public void setColor(Color color){
		this.color = color;
	}
	
	public Shape getOutline() {
		return this.outline;
	}
	
	public double getXPos(){
		return x;
	}

	public double getYPos(){
		return y;
	}
	
	public double getRadius(){
		return radius;
	}
	
	public Color getColor(){
		return color;
	}
	
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		System.out.println("Entered Node");
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void draw (Graphics2D g)
    {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (outline == null)
        {
            outline = getOutline();
        }
        g.draw(outline);
    }
}
