package assignment07;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

public class BalancedSymbolCheckerTest {

	BalancedSymbolChecker check = new BalancedSymbolChecker();
	@Test
	public void testClass1() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 6 and column 1. Expected ), but read } instead.", check.checkFile("src/assignment07/A7_examples/Class1.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testClass2() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 7 and column 1. Expected  , but read } instead.", check.checkFile("src/assignment07/A7_examples/Class2.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass3() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class3.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass4() {
		try {
			assertEquals("ERROR: File ended before closing comment.", check.checkFile("src/assignment07/A7_examples/Class4.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass5() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 3 and column 18. Expected ], but read } instead.", check.checkFile("src/assignment07/A7_examples/Class5.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass6() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class6.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass7() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 3 and column 33. Expected ], but read ) instead.", check.checkFile("src/assignment07/A7_examples/Class7.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass8() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 5 and column 30. Expected }, but read ) instead.", check.checkFile("src/assignment07/A7_examples/Class8.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass9() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 3 and column 33. Expected ), but read ] instead.", check.checkFile("src/assignment07/A7_examples/Class9.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass10() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 5 and column 10. Expected }, but read ] instead.", check.checkFile("src/assignment07/A7_examples/Class10.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass11() {
		try {
			assertEquals("ERROR: Unmatched symbol at the end of file. Expected }.", check.checkFile("src/assignment07/A7_examples/Class11.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass12() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class12.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass13() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class13.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testClass14() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class14.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass15() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 3 and column 44. Expected ), but read ] instead.", check.checkFile("src/assignment07/A7_examples/Class15.java"));
			//assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class15.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass16() {
		try {
			assertEquals("ERROR: Unmatched symbol at line 3 and column 50. Expected ), but read ] instead.", check.checkFile("src/assignment07/A7_examples/Class16.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testClass17() {
		try {
			assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class17.java"));
			//assertEquals("No errors found. All symbols match.", check.checkFile("src/assignment07/A7_examples/Class15.java"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
