package assignment07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Class containing the checkFile method for checking if the (, [, and { symbols
 * in an input file are correctly matched.
 * 
 * @author Shaheen Page
 * @author Christopher Carlson
 */
public class BalancedSymbolChecker {

	/**
	 * The current line of the file the scanner is on.
	 */
	private int currentLine;

	/**
	 * The current index of the line.
	 */
	private int currentColumn;

	/**
	 * Whether or not to ignore bracket symbols.
	 */
	private boolean ignoreSymbols;

	/**
	 * Whether or not the scanner is operating in a single line comment.
	 */
	private boolean singleCom;

	/**
	 * Whether or not the scanner is operating in a multi-line comment.
	 */
	private boolean multiCom;

	/**
	 * Whether or not the scanner is operating in a string or character literal.
	 */
	private boolean literal;

	/**
	 * The stack backing the operation.
	 */
	LinkedListStack<Character> stack;

	/**
	 * Returns a message indicating whether the input file has unmatched
	 * symbols. (Use the methods below for constructing messages.) Throws
	 * FileNotFoundException if the file does not exist.
	 */
	public String checkFile(String filename) throws FileNotFoundException {
		File file = new File(filename);
		Scanner scanner = new Scanner(file);
		scanner.useDelimiter("\n");
		stack = new LinkedListStack<>();
		currentLine = 1;
		while (scanner.hasNextLine()) {
			currentColumn = 1;
			String line = scanner.nextLine();
			String result = scanLine(line);
			if (result != "") {
				scanner.close();
				return result;
			}
			currentLine++;
			if (singleCom) {
				this.singleCom = false;
				this.ignoreSymbols = false;
			}
		}
		scanner.close();
		if (multiCom == true) {
			return this.unfinishedComment();
		}
		if (stack.isEmpty()) {
			return this.allSymbolsMatch();
		} else {
			return this.unmatchedSymbolAtEOF(getComplement(stack.pop()));
		}
	}

	/**
	 * Scans an input line for symbol matching errors.
	 * 
	 * @param line
	 *            A line from the file being scanned.
	 * @return An empty String if no errors were found, otherwise returns an
	 *         error message containing information as to where in the document
	 *         the error occurred.
	 */
	private String scanLine(String line) {
		for (int i = 0; i < line.length(); i++) {
			char character = line.charAt(i);

			if (i + 1 < line.length()) {
				if (character == '/' && line.charAt(i + 1) == '/') {
					this.ignoreSymbols = true;
					this.singleCom = true;
				}

				if (character == '/' && line.charAt(i + 1) == '*') {
					this.ignoreSymbols = true;
					this.multiCom = true;
				}

				if (character == '*' && line.charAt(i + 1) == '/') {
					this.ignoreSymbols = false;
					this.multiCom = false;
				}
			}

			if (multiCom == false && singleCom == false) {
				if (character == '"' || character == '\'') {
					if ((i - 1 >= 0 && line.charAt(i - 1) != '\\') || ((i-1 >=0 && line.charAt(i-1)=='\\') && (i-2 >=0 && line.charAt(i-2)=='\\'))) {
							literal = !literal;
							this.ignoreSymbols = literal;
					}
				}
			}

			if (ignoreSymbols == false) {
				if (character == '{' || character == '[' || character == '(') {
					stack.push(character);
				}
				if (character == '}' || character == ']' || character == ')') {
					char temp;
					try {
						temp = stack.pop();
					} catch (NoSuchElementException e) {
						return this.unmatchedSymbol(currentLine, currentColumn, character, ' ');
					}
					if (getComplement(character) != temp) {
						return this.unmatchedSymbol(currentLine, currentColumn, character, getComplement(temp));
					}
				}
			}
			currentColumn++;
		}
		return "";
	}

	/**
	 * Returns the matching complement of opening or closing symbols.
	 * 
	 * @param input
	 *            The symbol to return the complement of.
	 * @return
	 */
	private char getComplement(char input) {
		if (input == '{')
			return '}';
		if (input == '[')
			return ']';
		if (input == '(')
			return ')';
		if (input == '}')
			return '{';
		if (input == ']')
			return '[';
		if (input == ')')
			return '(';
		return ' ';
	}

	/**
	 * Returns an error message for unmatched symbol at the input line and
	 * column numbers. Indicates the symbol match that was expected and the
	 * symbol that was read.
	 */
	private String unmatchedSymbol(int lineNumber, int colNumber, char symbolRead, char symbolExpected) {
		return "ERROR: Unmatched symbol at line " + lineNumber + " and column " + colNumber + ". Expected "
				+ symbolExpected + ", but read " + symbolRead + " instead.";
	}

	/**
	 * Returns an error message for unmatched symbol at the end of file.
	 * Indicates the symbol match that was expected.
	 */
	private String unmatchedSymbolAtEOF(char symbolExpected) {
		return "ERROR: Unmatched symbol at the end of file. Expected " + symbolExpected + ".";
	}

	/**
	 * Returns an error message for a file that ends with an open /* comment.
	 */
	private String unfinishedComment() {
		return "ERROR: File ended before closing comment.";
	}

	/**
	 * Returns a message for a file in which all symbols match.
	 */
	private String allSymbolsMatch() {
		return "No errors found. All symbols match.";
	}
}