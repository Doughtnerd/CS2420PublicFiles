package assignment07;

public class SymbolCheckerRedux {

	/**
	 * The current line of the file the scanner is on.
	 */
	private int currentLine;

	/**
	 * The current index of the line.
	 */
	private int currentColumn;

	/**
	 * Whether or not to ignore bracket symbols.
	 */
	private boolean ignoreSymbols;

	/**
	 * Whether or not the scanner is operating in a single line comment.
	 */
	private boolean singleCom;

	/**
	 * Whether or not the scanner is operating in a multi-line comment.
	 */
	private boolean multiCom;

	/**
	 * Whether or not the scanner is operating in a string or character literal.
	 */
	private boolean literal;
	
	LinkedListStack<Character> stack;

	public SymbolCheckerRedux() {
		stack = new LinkedListStack<Character>();
		
	}

}
