package assignment12;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

/**
 * Class for testing the Huffman Tree algorithm.
 * 
 * @author Christopher Carlson
 * @author Spencer Smith
 *
 */
public class HuffmanTreeTest {

	HuffmanTree tree;
	File file, comp, decomp;

	@Before
	public void setUp() {
		try {
			tree = new HuffmanTree();
			file = File.createTempFile("HuffTreeTestFile", ".txt");
			FileWriter writer = new FileWriter(file);
			writer.write("I heart data.");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCompressAndDecompressFile() {
		try {
			
			// Assert the original file exists
			assertTrue(file.exists());

			/*
			 * Create the temporary compressed file, assert it exists. Compress
			 * the data into the file, and because the data is so short, Assert
			 * that the compressed data is actually larger than the original
			 * file.
			 */
			comp = File.createTempFile("Compressed", ".txt");
			assertTrue(comp.exists());
			tree.compressFile(file, comp);
			assertTrue(comp.length() > file.length());

			/*
			 * Create the temp decompressed file, assert it exists, decompress
			 * the compressed file into it, assert its size is the same as the
			 * original, then assert that it contains the same data that the
			 * original had.
			 */
			decomp = File.createTempFile("Decompressed", ".txt");
			assertTrue(decomp.exists());
			tree.decompressFile(comp, decomp);
			assertTrue(decomp.length() == file.length());
			Scanner sc = new Scanner(decomp);
			assertTrue(sc.nextLine().equals("I heart data."));
			sc.close();

			/*
			 * Mark temporary files for deletion upon closing of the VM.
			 */
			comp.deleteOnExit();
			file.deleteOnExit();
			decomp.deleteOnExit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
