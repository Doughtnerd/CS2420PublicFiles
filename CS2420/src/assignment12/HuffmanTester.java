package assignment12;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * This class is used to run timing experiments for Assignment08. This is the
 * timing code that was presented in the timing lab, only altered slightly for
 * my preferences/needs.
 * 
 * @author Christopher Carlson
 * @author Spencer Smith
 */
public class HuffmanTester {

	/**
	 * How many times to do the experiment, used to get a good average.
	 */
	private static final int ITER_COUNT = 1;

	/**
	 * This is a random generator that is sometimes used in experiments.
	 */
	private static Random rand;

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		/*
		 * Initialize Random.
		 */
		rand = new Random();
		/*
		 * Set the seed to system time for better random numbers.
		 */
		rand.setSeed(System.currentTimeMillis());

		/*
		 * Class startup time.
		 */
		long startTime = System.nanoTime();

		/*
		 * Cuts out class startup time.
		 */
		while (System.nanoTime() - startTime < 1000000000) {
		}

		/*
		 * The filename and path to write this test to.
		 */
		String file = JOptionPane.showInputDialog("Enter a filename");
		/*
		 * The trial name being ran, used in dimpleGraph.
		 */
		String trialName = JOptionPane.showInputDialog("What's the name of this trial (name the line will have)?");
		/*
		 * If either of the above are null, return and end execution.
		 */
		if (file == null || trialName == null) {
			System.err.println(
					"The file and trial names cannot be null. \nFile was: " + file + " Trial Name was: " + trialName);
			return;
		}
		/*
		 * Otherwise, open the filewriter, writes the csv file.
		 */
		try (FileWriter fw = new FileWriter(new File(file + ".csv"))) {
			/*
			 * Controls the size of the list to generate.
			 */
			for (int exp = 1; exp <= 16; exp++) {
				/*
				 * Bit shift, 0001 becomes 0010 and so forth, depending on how
				 * many bits it is being shifted.
				 */
				int size = 1 << exp;

				String[] array = {"aaaaaaaa","bbbbbbbb","cccccccc","dddddddd","eeeeeeee","ffffffff"};
				File original = File.createTempFile("Original", ".txt");
				File compressed = File.createTempFile("Compressed", ".txt");
				FileWriter writer = new FileWriter(original);
				HuffmanTree tree = new HuffmanTree();

				for(int i = 0; i < size; i++){
					writer.write(array[rand.nextInt(array.length)]);
					//writer.write(randomString(8));
				}
				
				writer.close();
				
				tree.compressFile(original, compressed);

				/*
				 * Prints the data being produced to the console.
				 */
				// System.out.println(trialName + "," + table.length() + "," +
				// count / ITER_COUNT);
				System.out.println(trialName + "," + original.length() + "," + Math.floor(((double)compressed.length()/(double)original.length())*100));
				/*
				 * Writes the data to the file in .csv format.
				 */
				// fw.write(trialName + "," + table.length() + "," + count /
				// ITER_COUNT+"\n");
				fw.write(trialName + "," + original.length() + "," + compressed.length() + "\n");
				
				original.delete();
				compressed.delete();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String randomString(int length) {
		String retval = "";
		for (int i = 0; i < length; i++) {
			retval += (char) ('a' + (rand.nextInt(26)));
		}
		return retval;
	}
}
