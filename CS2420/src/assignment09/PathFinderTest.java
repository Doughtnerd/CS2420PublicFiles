package assignment09;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.BeforeClass;
import org.junit.Test;

public class PathFinderTest {

	private static final String MAZE_FOLDER = "src/assignment09/mazes/";
	private static ArrayList<File> mazeFiles;
	private static ArrayList<File> solFiles;

	@BeforeClass
	public static void setUpFiles() {
		mazeFiles = new ArrayList<>();
		solFiles = new ArrayList<>();
		File mazeFile = new File(MAZE_FOLDER);
		for (File file : mazeFile.listFiles()) {
			if (file.isFile() && !file.getName().contains("My")) {
				if (file.getName().contains("Sol")) {
					solFiles.add(file);
				} else {
					mazeFiles.add(file);
				}
			}
		}
		assertEquals(mazeFiles.size(), solFiles.size());

	}

	@Test
	public void testMazes() {
		int i = 0;
		for (File file : mazeFiles) {
			String outputName = file.getName().replace(".txt", "") + "Solution.txt";
			PathFinder.solveMaze(file.getAbsolutePath(), outputName);
			assertTrue(filesAreEqual(new File(outputName), solFiles.get(i)));
			i++;
		}
	}
	
	@Test
	public void testMyTinyNoStart(){
		PathFinder.solveMaze(MAZE_FOLDER + "MyTinyNoStart.txt", "MyTinyNoStartSolution.txt");
	}
	
	@Test
	public void testMyTinyNoGoal(){
		PathFinder.solveMaze(MAZE_FOLDER + "MyTinyNoGoal.txt", "MyTinyNoGoalSolution.txt");
	}

	/**
	 * Written to scan text files line by line to ensure they are equal.
	 * 
	 * @param f1
	 *            - File 1
	 * @param f2
	 *            - File 2
	 * @return True if files are equal, false otherwise.
	 */
	private boolean filesAreEqual(File f1, File f2) {
		Scanner scanner1 = null;
		Scanner scanner2 = null;
		boolean flag = true;
		try {
			scanner1 = new Scanner(f1);
			scanner2 = new Scanner(f2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while (scanner1.hasNext() || scanner2.hasNext()) {
			String line1 = scanner1.nextLine();
			String line2 = scanner2.nextLine();
			if (!line1.equals(line2)) {
				flag = false;
				break;
			}
		}
		scanner1.close();
		scanner2.close();
		return flag;

	}

}
