package assignment09;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Maze path finding class that solves mazes with the shortest possible route.
 * @author Christopher Carlson
 * @author Shaheen Page
 *
 */
public class PathFinder {

	/**
	 * Constant for a wall in the maze.
	 */
	private static final char WALL = 'X';

	/**
	 * Constant for the starting point in the maze.
	 */
	private static final char START = 'S';

	/**
	 * Constant for a path character.
	 */
	private static final char PATH ='.';
	/**
	 * Constant for the end point in the maze.
	 */
	private static final char GOAL = 'G';

	/**
	 * Array of Nodes that represent the maze.
	 */
	private static Graph.Node[][] maze;

	/**
	 * Takes an input file following the explicit maze file pattern and outputs
	 * the solution to the maze.
	 * 
	 * @param inputFile
	 *            - The path and filename of the explicitly formatted maze file
	 *            to be solved.
	 * @param outputFile
	 *            - The path and filename of the solution to the input maze.
	 * @throws IOException
	 */
	public static void solveMaze(String inputFile, String outputFile) {
		/*
		 * Reads the inputFile into a Scanner
		 */
		try (Scanner input = new Scanner(new File(inputFile))) {

			/*
			 * Reads dimensions from the first line.
			 */
			String[] dimensions = input.nextLine().split(" ");
			int height = Integer.parseInt(dimensions[0]);
			int width = Integer.parseInt(dimensions[1]);

			/*
			 * Constructs a Node array based off of the height and width. With
			 * this, <i> every </i> character in the maze becomes a node, which
			 * increases the memory overhead but is easier to find neighbors in the
			 * maze with.
			 */
			maze = new Graph.Node[height][width];

			/**
			 * Read in the maze.
			 */
			for (int i = 0; input.hasNext(); i++) {
				String line = input.nextLine();
				for (int j = 0; j < width; j++) {
					maze[i][j] = new Graph.Node(line.charAt(j));
				}
			}
			input.close();

			/*
			 * Create the graph and solve the maze.
			 */
			Graph graph = new Graph();
			graph.findShortestPath();

			/*
			 * Write the file to the specified outputFile
			 */
			FileWriter writer = new FileWriter(outputFile);
			writer.write(height + " " + width + "\r\n");
			for (int i = 0; i < maze.length; i++) {
				for (int j = 0; j < maze[0].length; j++) {
					writer.write(maze[i][j].data);
				}
				writer.write("\r\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Class representing the graph data structure that solves the maze.
	 * 
	 * @author Christopher Carlson
	 * @author Shaheen Page
	 *
	 */
	static class Graph {
		/**
		 * List of valid nodes in the maze.
		 */
		// private ArrayList<Node> nodes;

		/**
		 * Queue used for the breadth first search.
		 */
		Queue<Node> q;

		/**
		 * The Node representing the starting point in the maze.
		 */
		Node start;

		/**
		 * The Node representing the goal point in the maze.
		 */
		Node goal;

		/**
		 * Constructs the graph and sets up all of the data so the maze can be
		 * solved.
		 */
		private Graph() {
			q = new LinkedList<>();
			for (int i = 0; i < maze.length; i++) {
				for (int j = 0; j < maze[0].length; j++) {
					Node node = maze[i][j];
					if (node.data != WALL) {
						if (node.data == START) {
							start = node;
						}
						if (node.data == GOAL) {
							goal = node;
						}
						node.neighbors = findNeighbors(i, j);
					}
				}
			}
		}

		/**
		 * Method that finds the shortest path from the start node to the end
		 * node.
		 */
		private void findShortestPath() {
			if (start == null || goal == null) {
				System.err.println("Path finding failed due to null nodes");
				return;
			}
			breadthFirstSearch();
			reconstructPath(goal);
		}

		/**
		 * Executes a breadth first search on the graph.
		 */
		private void breadthFirstSearch() {
			start.visited = true;
			q.add(start);

			while (!q.isEmpty()) {
				Node current = q.remove();
				if (current.equals(goal)) {
					return;
				}
				for (Node next : current.neighbors) {
					if (!next.visited) {
						next.visited = true;
						next.cameFrom = current;
						q.add(next);
					}
				}
			}
		}

		/**
		 * Recursively reconstructs the path used to find the goal.
		 * 
		 * @param node
		 *            - The Node to recurse on.
		 */
		private void reconstructPath(Node node) {
			if (node == null) {
				return;
			} else {
				/*
				 * Switch node's data so the path can be traced when the
				 * solution is printed.
				 */
				node.data = !node.equals(start) && !node.equals(goal) ? PATH : node.data;
				reconstructPath(node.cameFrom);
			}
		}

		/**
		 * Finds the neighbors of a node at a given point in the maze array.
		 * Because this method does not iterate over an AdjacencyMatrix or the
		 * entire maze matrix, complexity is constant time.
		 * 
		 * @param i
		 *            - The row of the Node to find neighbors for.
		 * @param j
		 *            - The column of the Node to find neighbors for.
		 * @return An ArrayList of Nodes that represent the desired node's
		 *         neighbors.
		 */
		private ArrayList<Node> findNeighbors(int i, int j) {
			ArrayList<Node> list = new ArrayList<>();
			for (int k = 0; k < 4; k++) {
				Node node = null;
				switch (k) {
				case 0:
					/*
					 * Gets the left neighbor.
					 */
					node = getNeighbor(i, j - 1);
					break;
				case 1:
					/*
					 * Gets the right neighbor.
					 */
					node = getNeighbor(i, j + 1);
					break;
				case 2:
					/*
					 * Gets the top neighbor
					 */
					node = getNeighbor(i - 1, j);
					break;
				case 3:
					/*
					 * Gets the bottom neighbor.
					 */
					node = getNeighbor(i + 1, j);
					break;
				}
				if (node != null) {
					list.add(node);
				}
			}
			return list;
		}

		/**
		 * Returns the Node located at the given index, assuming the index is
		 * valid and the data inside the node at the index is not 'X'.
		 * 
		 * @param i
		 *            - The row of the desired Node.
		 * @param j
		 *            - The column of the desired Node.
		 * @return The Node at the given index if Node.data!='X'. Returns null
		 *         otherwise.
		 */
		private Node getNeighbor(int i, int j) {
			if ((i < 0 || i > maze.length - 1) || (j < 0 || j > maze[0].length - 1) || maze[i][j].data == WALL) {
				return null;
			} else {
				return maze[i][j];
			}
		}

		/**
		 * Class representing each node in the graph.
		 * 
		 * @author Christopher Carlson
		 * @author Shaheen Page
		 */
		static class Node {
			/**
			 * List of neighbors to this node.
			 */
			ArrayList<Node> neighbors;
			/**
			 * The data of type Character that this node contains.
			 */
			Character data;

			/**
			 * Whether or not this node has been visited.
			 */
			boolean visited;

			/**
			 * The node that was visited previous to coming to this node.
			 */
			Node cameFrom;

			/**
			 * Constructs a Node with null data.
			 */
			private Node() {
				this(null);
			}

			/**
			 * Constructs a Node passed starting data.
			 * 
			 * @param data
			 *            - The data to initialize this Node with.
			 */
			private Node(Character data) {
				this.data = data;
				visited = false;
			}
		}
	}
}
