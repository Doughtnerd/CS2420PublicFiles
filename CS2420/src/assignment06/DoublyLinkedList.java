package assignment06;

import java.util.Iterator;
import java.util.NoSuchElementException;

import assignment06.List;

public class DoublyLinkedList<E> implements List<E>, Iterable<E> {

	private int size;
	private Node dummy;

	public DoublyLinkedList() {
		this.size = -1;
		dummy = new Node(null);
		dummy.setNext(dummy);
		dummy.setPrevious(dummy);
	}

	/**
	 * Inserts the specified element at the beginning of the list. O(1) for a
	 * doubly-linked list.
	 */
	@Override
	public void addFirst(E element) {
		Node newNode = new Node(element, dummy, dummy.getNext());
		dummy.getNext().setPrevious(newNode);
		dummy.setNext(newNode);
	}

	/**
	 * Inserts the specified element at the end of the list. O(1) for a
	 * doubly-linked list.
	 */
	@Override
	public void addLast(E o) {
		Node last = dummy.getPrevious();
		Node newNode = new Node(o, last, dummy);
		last.setNext(newNode);
		dummy.setPrevious(newNode);
	}

	/**
	 * Inserts the specified element at the specified position in the list.
	 * Throws IndexOutOfBoundsException if index is out of range (index < 0 ||
	 * index > size()) O(N) for a doubly-linked list.
	 */
	@Override
	public void add(int index, E element) throws IndexOutOfBoundsException {
		if (index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
		if (index == 0) {
			addFirst(element);
		} else if (index == size) {
			addLast(element);
		} else {
			Node indexNode = getNode(index);
			Node newNode = new Node(element);

			newNode.setNext(indexNode);
			newNode.setPrevious(indexNode.getPrevious());

			indexNode.getPrevious().setNext(newNode);
			indexNode.setPrevious(newNode);
		}

	}

	/**
	 * Returns the node at the specified index.
	 * 
	 * @param i
	 *            Index of the desired node.
	 * @return The node at the specified index.
	 */
	private Node getNode(int i) {
		Node p = null;
		if (i < size / 2) {
			p = dummy.getNext();
			for (int j = 0; j < i; j++)
				p = p.getNext();
		} else {
			p = dummy.getPrevious();
			for (int j = size; j > i; j--)
				p = p.getPrevious();
		}
		return p;
	}

	/**
	 * Returns the first element in the list. Throws NoSuchElementException if
	 * the list is empty. O(1) for a doubly-linked list.
	 */
	@Override
	public E getFirst() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}
		return dummy.getNext().getData();
	}

	/**
	 * Returns the last element in the list. Throws NoSuchElementException if
	 * the list is empty. O(1) for a doubly-linked list.
	 */
	@Override
	public E getLast() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}
		return dummy.getPrevious().getData();
	}

	/**
	 * Returns the element at the specified position in the list. Throws
	 * IndexOutOfBoundsException if index is out of range (index < 0 || index >=
	 * size()) O(N) for a doubly-linked list.
	 */
	@Override
	public E get(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
		return getNode(index).getData();
	}

	/**
	 * Removes and returns the first element from the list. Throws
	 * NoSuchElementException if the list is empty. O(1) for a doubly-linked
	 * list.
	 */
	@Override
	public E removeFirst() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}

		Node removed = dummy.getNext();
		dummy.setNext(removed.getNext());
		this.size--;
		return removed.getData();
	}

	/**
	 * Removes and returns the last element from the list. Throws
	 * NoSuchElementException if the list is empty. O(1) for a doubly-linked
	 * list.
	 */
	@Override
	public E removeLast() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException();
		}

		Node removed = dummy.getPrevious();
		dummy.setNext(removed.getPrevious());
		this.size--;
		return removed.getData();
	}

	/**
	 * Removes and returns the element at the specified position in the list.
	 * Throws IndexOutOfBoundsException if index is out of range (index < 0 ||
	 * index >= size()) O(N) for a doubly-linked list.
	 */
	@Override
	public E remove(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
		if (index == 0) {
			return removeFirst();
		} else if (index == size) {
			return removeLast();
		} else {
			Node removed = getNode(index);
			removed.getPrevious().setNext(removed.getNext());
			removed.getNext().setPrevious(removed.getPrevious());
			this.size--;
			return removed.getData();
		}
	}

	/**
	 * Returns the index of the first occurrence of the specified element in the
	 * list, or -1 if this list does not contain the element. O(N) for a
	 * doubly-linked list.
	 */
	@Override
	public int indexOf(E element) {
		Node n = dummy.getNext();
		for (int i = 0; i < size; i++) {
			if (n.getData() == element) {
				return i;
			}
			n = n.getNext();
		}
		return -1;
	}

	/**
	 * Returns the index of the last occurrence of the specified element in this
	 * list, or -1 if this list does not contain the element. O(N) for a
	 * doubly-linked list.
	 */
	@Override
	public int lastIndexOf(E element) {
		Node n = dummy.getPrevious();
		for (int i = 0; i < size; i++) {
			if (n.getData() == element) {
				return i;
			}
			n = n.getPrevious();
		}
		return -1;
	}

	/**
	 * Returns the number of elements in this list. O(1) for a doubly-linked
	 * list.
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Returns true if this collection contains no elements. O(1) for a
	 * doubly-linked list.
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/**
	 * Removes all of the elements from this list. O(1) for a doubly-linked
	 * list.
	 */
	@Override
	public void clear() {
		dummy.setNext(dummy);
		dummy.setPrevious(dummy);
		this.size = 0;
	}

	/**
	 * Returns an array containing all of the elements in this list in proper
	 * sequence (from first to last element). O(N) for a doubly-linked list.
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		Node n = dummy.getNext();
		for (int i = 0; i < size; i++) {
			array[i] = n.getData();
			n = n.getNext();
		}
		return array;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {

			private Node current = dummy;
			private int index = -1;
			private boolean nextCalled = false;
			private boolean hasRemoved = false;

			@Override
			public boolean hasNext() {
				return current.getNext() != null && index < size - 1;
			}

			@Override
			public E next() throws NoSuchElementException {

				if (!hasNext()) {
					throw new NoSuchElementException();
				}

				current = current.getNext();
				index += 1;
				nextCalled = true;
				hasRemoved = false;

				return current.getData();

			}

			@Override
			public void remove() {

				if (hasRemoved || !nextCalled) {
					throw new IllegalStateException();
				}

				DoublyLinkedList.this.remove(index);
				current = current.getPrevious();
				index -= 1;
				hasRemoved = true;

			}
		};
	}

	class Node {

		private E data;
		private Node previous;
		private Node next;

		protected Node(E data) {
			this(data, null);
		}

		protected Node(E data, Node next) {
			this(data, null, next);
		}

		protected Node(E data, Node previous, Node next) {
			size++;
			this.data = data;
			this.previous = previous;
			this.next = next;
		}

		protected void setNext(Node next) {
			this.next = next;
		}

		protected void setPrevious(Node previous) {
			this.previous = previous;
		}

		protected E getData() {
			return data;
		}

		protected void setData(E data) {
			this.data = data;
		}

		protected Node getNext() {
			return this.next;
		}

		protected Node getPrevious() {
			return this.previous;
		}

	}

}
