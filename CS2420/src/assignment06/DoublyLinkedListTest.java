package assignment06;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;

public class DoublyLinkedListTest {

	DoublyLinkedList<Integer> list;
	
	
	@Before
	public void setupIntList(){
		list = new DoublyLinkedList<Integer>();
	}
	
	@Test
	public void testDoublyLinkedList() {
		assertEquals(0, list.size());
	}

	@Test
	public void testAddFirst() {
		list.addFirst(8);
		assertEquals(new Integer(8), list.getFirst());
		assertEquals(1, list.size());
		list.addFirst(9);
		assertEquals(new Integer(9), list.getFirst());
		assertEquals(2, list.size());
		assertEquals(new Integer(8), list.get(1));
	}

	@Test
	public void testAddLast() {
		list.addLast(2);
		assertEquals(new Integer(2), list.getFirst());
		assertEquals(new Integer(2), list.getLast());
		assertEquals(1, list.size());
		list.addLast(3);
		assertEquals(new Integer(2), list.getFirst());
		assertEquals(new Integer(3), list.getLast());
		assertEquals(2, list.size());
		list.addLast(4);
		assertEquals(new Integer(2), list.getFirst());
		assertEquals(new Integer(3), list.get(1));
		assertEquals(new Integer(4), list.getLast());
		assertEquals(3, list.size());
		list.clear();
		assertEquals(0, list.size());
	}

	@Test
	public void testAdd() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetFirst() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetLast() {
		fail("Not yet implemented");
	}

	@Test
	public void testGet() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveFirst() {
		fail("Not yet implemented");
		
	}

	@Test
	public void testRemoveLast() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemove() {
		fail("Not yet implemented");
	}

	@Test
	public void testIndexOf() {
		list.addFirst(1);
		list.addFirst(2);
		list.addFirst(3);
		assertArrayEquals(new int[0], new int[0]);
		assertEquals(2, list.indexOf(1));
		assertEquals(-1, list.indexOf(4));
	}

	@Test
	public void testLastIndexOf() {
		fail("Not yet implemented");
		//shaheenspage@gmail.com
	}

	@Test
	public void testSize() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsEmpty() {
		fail("Not yet implemented");
	}

	@Test
	public void testClear() {
		fail("Not yet implemented");
	}

	@Test
	public void testToArray() {
		fail("Not yet implemented");
	}

	@Test
	public void testIterator() {
		assertEquals(0, list.size());
		list.addFirst(1);
		assertEquals(1, list.size());
		list.addFirst(2);
		assertEquals(2, list.size());
		list.addFirst(3);
		assertEquals(3, list.size());
		Iterator<Integer> iter = list.iterator();
		int i = 3;
		while(iter.hasNext()){
			int k = iter.next();
			assertEquals(new Integer(i), new Integer(k));
			i--;
		}
		assertFalse(iter.hasNext());
		try{
			iter.next();
			fail("Did not catch exception");
		} catch(NoSuchElementException e){}
	}
	
	@Test
	public void testConcurrencySupport(){
		for(int i = 0; i < 1024; i++){
			list.add(i, i);
		}
		
		Iterator<Integer> iter = list.iterator();
		iter.next();
		iter.remove();
		list.remove(0);
		assertEquals(new Integer(2), list.get(0));
		iter.next();
		assertTrue(list.get(1)==3);
		iter.remove();
		assertFalse(list.get(1)==3);
		assertEquals(0, list.indexOf(3));
		assertEquals(-1, list.indexOf(2));
	}
	
	@Test
	public void testCModPassesHead(){
		for(int i = 0; i < 1024; i++){
			list.add(i, i);
		}
		
		Iterator<Integer> iter = list.iterator();
		list.removeFirst();
		assertEquals(new Integer(1), iter.next());
		list.removeFirst();
		list.removeFirst();
		list.removeFirst();
		assertEquals(new Integer(2), iter.next());
	}
	
	
	@SuppressWarnings("unused")
	private void printArray(){
		int i = 0;
		for(Object o : list.toArray()){
			System.out.print(o + " ");
			i++;
			if(i%10 == 0){
				System.out.println();
			}
		}
		System.out.println();
	}
	@Test
	public void testIteratorRemove(){
		for(int i = 0; i < 10; i++){
			list.add(i, i);
		}
		Iterator<Integer> iter = list.iterator();
		try{
			iter.remove();
			fail("Did not catch exception");
		} catch (IllegalStateException e){}
		iter.next();
		iter.remove();
		assertEquals(new Integer(1), list.get(0));
		assertEquals(9, list.size());
		assertTrue(iter.hasNext());
		
		while(iter.hasNext()){
			iter.next();
			iter.remove();
		}
		
		assertEquals(0,list.size());
		
		
	}
	

}
