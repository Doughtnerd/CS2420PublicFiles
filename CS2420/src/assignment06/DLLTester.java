package assignment06;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import javax.swing.JOptionPane;

public class DLLTester {

	private static final int ITER_COUNT = 10;
	private static Random rand;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		
		long startTime = System.nanoTime();

		while (System.nanoTime() - startTime < 1000000000);

		try(FileWriter fw = new FileWriter(new File(JOptionPane.showInputDialog("Enter a filename")))) { //open up a file writer so we can write to file.
			String trialName = JOptionPane.showInputDialog("What's the name of this trial (name the line will have)?");
			for(int exp = 10; exp <= 25; exp++) { // This is used as the exponent to calculate the size of the set.
				int size = (int) Math.pow(2, exp); // or ..  
				//size = 1 << exp; // the two statements are equivalent, look into bit-shifting if you're interested what is going on here.
				// Do the experiment multiple times, and average out the results
				long totalTime = 0;
				for (int iter = 0; iter < ITER_COUNT; iter++) {
					
					// SET UP!
					DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
					//int index = rand.nextInt(size-1);
					// TIME IT!
					for(int i = size; i > 0; i--){
						list.addLast(i);
					}
					long start = System.nanoTime();
					list.addLast(size+1);
					long stop = System.nanoTime();
					totalTime += stop - start;
				}
				double averageTime = totalTime / (double)ITER_COUNT;
				System.out.println(trialName + ","+ size + "," + averageTime); // print to console
				fw.write(trialName + ","+ size + "," + averageTime + "\n"); // write to file.
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Create a random string [a-z] of specified length
	public static String randomString(int length)
	{
		String retval = "";
		for(int i = 0; i < length; i++)
		{
			// ASCII values a-z,A-Z are contiguous (52 characters)
			retval += (char)('a' + (rand.nextInt(26)));
		}
		return retval;
	}
}
