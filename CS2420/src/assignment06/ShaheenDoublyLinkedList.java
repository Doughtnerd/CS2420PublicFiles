package assignment06;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Shaheen Page
 *
 * @param <E>
 *            type of elements contained in the doubly linked list
 */
public class ShaheenDoublyLinkedList<E> implements List<E>, Iterable<E> {

    private int size;
    private Node head;
    private Node tail;

    @Override
    public void addFirst (E element) {

        Node temp = head;

        head = new Node();
        head.data = element;
        head.next = temp;

        if (temp == null) {
            tail = head;
        } else {
            temp.previous = head;
        }

        size += 1;

    }

    @Override
    public void addLast (E element) {

        Node temp = tail;

        tail = new Node();
        tail.data = element;
        tail.previous = temp;

        if (temp == null) {
            head = tail;
        } else {
            temp.next = tail;
        }

        size += 1;

    }

    @Override
    public void add (int index, E element) throws IndexOutOfBoundsException {

        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {

            addFirst(element);

        } else if (index == size) {

            addLast(element);

        } else {

            Node temp = getNode(index);
            Node newNode = new Node();

            newNode.data = element;
            newNode.next = temp;
            newNode.previous = temp.previous;

            temp.previous.next = newNode;
            temp.previous = newNode;

            size += 1;

        }

    }

    @Override
    public E getFirst () throws NoSuchElementException {

        if (head == null) {
            throw new NoSuchElementException();
        }

        return head.data;

    }

    @Override
    public E getLast () throws NoSuchElementException {

        if (tail == null) {
            throw new NoSuchElementException();
        }

        return tail.data;

    }

    @Override
    public E get (int index) throws IndexOutOfBoundsException {

        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {

            return getFirst();

        } else if (index == size - 1) {

            return getLast();

        } else {

            return getNode(index).data;

        }

    }

    /**
     * Returns the node at the specified index.
     *
     * @param index
     *            item in the linked list
     * @return
     *         node at the specified index
     */
    private Node getNode (int index) {

        boolean startAtHead = index <= size / 2;
        Node temp = startAtHead ? head : tail;
        int i = startAtHead ? 0 : size - 1;

        while (i != index) {
            temp = startAtHead ? temp.next : temp.previous;
            i = i + (startAtHead ? 1 : -1);
        }

        return temp;

    }

    @Override
    public E removeFirst () throws NoSuchElementException {

        if (head == null) {
            throw new NoSuchElementException();
        }

        Node temp = head;

        if (temp.next == null) {
            tail = null;
        } else {
            temp.next.previous = temp.previous;
        }

        head = temp.next;

        size -= 1;

        return temp.data;

    }

    @Override
    public E removeLast () throws NoSuchElementException {

        if (tail == null) {
            throw new NoSuchElementException();
        }

        Node temp = tail;

        if (temp.previous == null) {
            head = null;
        } else {
            temp.previous.next = temp.next;
        }

        tail = temp.previous;

        size -= 1;

        return temp.data;

    }

    @Override
    public E remove (int index) throws IndexOutOfBoundsException {

        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        E removedData;

        if (index == 0) {
            removedData = removeFirst();
        } else if (index == size - 1) {
            removedData = removeLast();
        } else {
            removedData = removeNode(getNode(index));
        }

        return removedData;

    }

    /**
     * Removes the specified node from the DoublyLinkedList then returns the removed node's data.
     *
     * @param nodeToRemove
     *            node to remove from the DoublyLinkedList
     * @return
     *         <tt>nodeToRemove</tt>'s data
     */
    private E removeNode (Node nodeToRemove) {

        nodeToRemove.previous.next = nodeToRemove.next;
        nodeToRemove.next.previous = nodeToRemove.previous;

        size -= 1;

        return nodeToRemove.data;

    }

    @Override
    public int indexOf (E element) {

        Node temp = head;
        int i = 0;

        while (temp != null) {

            if ((temp.data == null && element == null) || (temp.data != null && temp.data.equals(element))) {
                return i;
            }

            temp = temp.next;
            i += 1;

        }

        return -1;

    }

    @Override
    public int lastIndexOf (E element) {

        Node temp = tail;
        int i = size - 1;

        while (temp != null) {

            if ((temp.data == null && element == null) || (temp.data != null && temp.data.equals(element))) {
                return i;
            }

            temp = temp.previous;
            i -= 1;
        }

        return -1;

    }

    @Override
    public int size () {
        return size;
    }

    @Override
    public boolean isEmpty () {
        return size == 0;
    }

    @Override
    public void clear () {
        size = 0;
        head = null;
        tail = null;
    }

    @Override
    public Object[] toArray () {

        Object[] array = new Object[size];
        Node temp = head;
        int i = 0;

        while (temp != null) {
            array[i] = temp.data;
            temp = temp.next;
            i += 1;
        }

        return array;

    }

    @Override
    public Iterator<E> iterator () {

        return new Iterator<E>() {

            private Node current = null;
            private int index = -1;
            private boolean nextCalled = false;
            private boolean hasRemoved = false;

            @Override
            public boolean hasNext () {
                return index == -1 ? head != null : current.next != null;
            }

            @Override
            public E next () throws NoSuchElementException {

                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                current = index == -1 ? head : current.next;
                index += 1;
                nextCalled = true;
                hasRemoved = false;

                return current.data;

            }

            @Override
            public void remove () throws IllegalStateException {

                if (hasRemoved || !nextCalled) {
                    throw new IllegalStateException();
                }

                if (index == 0) {
                    ShaheenDoublyLinkedList.this.removeFirst();
                } else if (index == size - 1) {
                	ShaheenDoublyLinkedList.this.removeLast();
                } else {
                	ShaheenDoublyLinkedList.this.removeNode(current);
                }

                current = current.previous;
                index -= 1;
                hasRemoved = true;

            }

        };

    }

    private class Node {

        private E data;
        private Node previous;
        private Node next;

    }
}
