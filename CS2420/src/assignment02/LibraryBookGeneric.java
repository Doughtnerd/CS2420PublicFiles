package assignment02;

import java.util.GregorianCalendar;

/**
 * A class representing a Library Book with a generic holder.
 * 
 * @author Christopher Carlson, Brianne Young.
 *
 * @param <Type>
 *            - The type of holder the book contains.
 */
public class LibraryBookGeneric<Type> extends Book {

	private boolean checkedIn;
	private GregorianCalendar dueDate;
	private Type holder;

	public LibraryBookGeneric(long isbn, String author, String title) {
		super(isbn, author, title);
		checkedIn = true;
		dueDate = null;
		holder = null;
	}

	public Type getHolder() {
		return holder; // == null ? (Type) "":holder;
	}

	public GregorianCalendar getDueDate() {
		return dueDate;
	}

	public boolean isCheckedIn() {
		return this.checkedIn;
	}

	/*
	 * public void setHolder(String holder){ this.holder = holder; }
	 * 
	 * public void setDueDate(GregorianCalendar dueDate){ this.dueDate =
	 * dueDate; }
	 */
	public void checkIn() {
		checkedIn = true;
		dueDate = null;
		holder = null;
	}

	public void checkOut(Type holder, GregorianCalendar dueDate) {
		checkedIn = false;
		this.holder = holder;
		this.dueDate = dueDate;
	}
}
