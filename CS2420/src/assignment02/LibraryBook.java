package assignment02;

import java.util.GregorianCalendar;

/**
 * A class representing a Library Book with a string holder.
 * 
 * @author Christopher Carlson, Brianne Young
 *
 */
public class LibraryBook extends Book {

	private boolean checkedIn;
	private GregorianCalendar dueDate;
	private String holder;

	public LibraryBook(long isbn, String author, String title) {
		super(isbn, author, title);
		checkedIn = true;
		dueDate = null;
		holder = null;
	}

	public String getHolder() {
		return holder == null ? "" : holder;
	}

	public GregorianCalendar getDueDate() {
		return dueDate;
	}

	public boolean isCheckedIn() {
		return this.checkedIn;
	}

	/*
	 * public void setHolder(String holder){ this.holder = holder; }
	 * 
	 * public void setDueDate(GregorianCalendar dueDate){ this.dueDate =
	 * dueDate; }
	 */
	public void checkIn() {
		checkedIn = true;
		dueDate = null;
		holder = null;
	}

	public void checkOut(String holder, GregorianCalendar dueDate) {
		checkedIn = false;
		this.holder = holder;
		this.dueDate = dueDate;
	}
}
