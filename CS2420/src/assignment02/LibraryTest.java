package assignment02;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;

import org.junit.Test;

/**
 * A class to test the non-generic library.
 * 
 * @author Christopher Carlson, Brianne Young
 *
 */
public class LibraryTest {

	Library lib = new Library();

	@Test
	public void testAdd() {
		lib.add(123456789, "John Doe", "There's Idiots Everywhere");
		lib.add(123789456, "Jane Doe", "You're Included");
		lib.add(789456123, "Jack Deaux", "But Hey, So Am I");
		assertEquals(null, lib.lookup(123456789));
		assertEquals(null, lib.lookup(123789456));
		assertEquals(null, lib.lookup(789456123));

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals("Hubris", lib.lookup(123456789));
		assertEquals(1, lib.lookup("Hubris").size());

		assertEquals(true, lib.checkout(789456123, "Hubris", 1, 1, 2016));
		assertEquals(2, lib.lookup("Hubris").size());

		assertEquals(true, lib.checkin(123456789));
		assertEquals(1, lib.lookup("Hubris").size());

		assertEquals("But Hey, So Am I", lib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testAddAllArrayListOfLibraryBook() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(null, lib.lookup(123456789));
		assertEquals(null, lib.lookup(123789456));
		assertEquals(null, lib.lookup(789456123));

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals("Hubris", lib.lookup(123456789));
		assertEquals(1, lib.lookup("Hubris").size());

		assertEquals(true, lib.checkout(789456123, "Hubris", 1, 1, 2016));
		assertEquals(2, lib.lookup("Hubris").size());

		assertEquals(true, lib.checkin(123456789));
		assertEquals(1, lib.lookup("Hubris").size());

		assertEquals("But Hey, So Am I", lib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testLookupLong() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals(null, lib.lookup(789456123));
		assertEquals("Hubris", lib.lookup(123456789));
	}

	@Test
	public void testLookupString() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals(true, lib.checkout(789456123, "Hubris", 30, 30, -1));
		assertEquals(0, lib.lookup("Llama").size());
		assertEquals(2, lib.lookup("Hubris").size());
		assertEquals("There's Idiots Everywhere", lib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testCheckout() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals(false, lib.checkout(159789456, "Hubris", 1, 1, 2016));
		assertEquals(false, lib.checkout(123456951, "Llama", 2, 4, 2016));
		assertEquals(false, lib.checkout(236159478, "Hubris", 20, 20, 2016));
		assertEquals(false, lib.checkout(123456789, "Hubris", 1, 2, 2016));
		assertEquals(true, lib.checkout(789456123, "Hubris", 30, 30, -1));
	}

	@Test
	public void testCheckinLong() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals(true, lib.checkout(123789456, "Hubris", 2, 4, 2016));
		assertEquals(true, lib.checkout(789456123, "Hubris", 30, 30, -1));

		assertEquals(false, lib.checkout(123456789, "Llama", 2, 4, 2016));
		assertEquals(true, lib.checkin(123456789));
		assertEquals(true, lib.checkout(123456789, "Llama", 2, 4, 2016));

	}

	@Test
	public void testCheckinString() {
		ArrayList<LibraryBook> list = new ArrayList<>();
		list.add(new LibraryBook(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBook(123789456, "Jane Doe", "You're Included"));
		list.add(new LibraryBook(789456123, "Jack Deaux", "But Hey, So Am I"));
		lib.addAll(list);

		assertEquals(true, lib.checkout(123456789, "Hubris", 1, 1, 2016));
		assertEquals(true, lib.checkout(123789456, "Hubris", 2, 4, 2016));
		assertEquals(true, lib.checkout(789456123, "Hubris", 30, 30, -1));

		assertEquals(false, lib.checkout(123456789, "Llama", 2, 4, 2016));

		assertEquals(true, lib.checkin("Hubris"));

		assertEquals(true, lib.checkout(123456789, "Llama", 1, 2, 2016));
	}

	@Test
	public void firstTest() {
		// test an empty library
		Library lib = new Library();

		if (lib.lookup(978037429279L) != null)
			fail("TEST FAILED -- empty library: lookup(isbn)");
		ArrayList<LibraryBook> booksCheckedOut = lib.lookup("Jane Doe");
		if (booksCheckedOut == null || booksCheckedOut.size() != 0)
			fail("TEST FAILED -- empty library: lookup(holder)");
		if (lib.checkout(978037429279L, "Jane Doe", 1, 1, 2008))
			fail("TEST FAILED -- empty library: checkout");
		if (lib.checkin(978037429279L))
			fail("TEST FAILED -- empty library: checkin(isbn)");
		if (lib.checkin("Jane Doe"))
			fail("TEST FAILED -- empty library: checkin(holder)");

		// test a small library
		lib.add(9780374292799L, "Thomas L. Friedman", "The World is Flat");
		lib.add(9780330351690L, "Jon Krakauer", "Into the Wild");
		lib.add(9780446580342L, "David Baldacci", "Simple Genius");

		if (lib.lookup(9780330351690L) != null)
			fail("TEST FAILED -- small library: lookup(isbn)");
		if (!lib.checkout(9780330351690L, "Jane Doe", 1, 1, 2008))
			fail("TEST FAILED -- small library: checkout");
		booksCheckedOut = lib.lookup("Jane Doe");
		if (booksCheckedOut == null || booksCheckedOut.size() != 1
				|| !booksCheckedOut.get(0).equals(new Book(9780330351690L, "Jon Krakauer", "Into the Wild"))
				|| !booksCheckedOut.get(0).getHolder().equals("Jane Doe")
				|| !booksCheckedOut.get(0).getDueDate().equals(new GregorianCalendar(2008, 1, 1)))
			fail("TEST FAILED -- small library: lookup(holder)");
		if (!lib.checkin(9780330351690L))
			fail("TEST FAILED -- small library: checkin(isbn)");
		if (lib.checkin("Jane Doe"))
			fail("TEST FAILED -- small library: checkin(holder)");

		// test a medium library
		lib.addAll("Mushroom_Publishing.txt");

		// FILL IN

		System.out.println("Testing done.");
	}

	/**
	 * Returns a library of "dummy" books (random ISBN and placeholders for
	 * author and title).
	 * 
	 * Useful for collecting running times for operations on libraries of
	 * varying size.
	 * 
	 * @param size
	 *            -- size of the library to be generated
	 */
	public static ArrayList<LibraryBook> generateLibrary(int size) {
		ArrayList<LibraryBook> result = new ArrayList<LibraryBook>();

		for (int i = 0; i < size; i++) {
			// generate random ISBN
			Random randomNumGen = new Random();
			String isbn = "";
			for (int j = 0; j < 13; j++)
				isbn += randomNumGen.nextInt(10);

			result.add(new LibraryBook(Long.parseLong(isbn), "An author", "A title"));
		}

		return result;
	}

	/**
	 * Returns a randomly-generated ISBN (a long with 13 digits).
	 * 
	 * Useful for collecting running times for operations on libraries of
	 * varying size.
	 * 
	 */
	public static long generateIsbn() {
		Random randomNumGen = new Random();

		String isbn = "";
		for (int j = 0; j < 13; j++)
			isbn += randomNumGen.nextInt(10);

		return Long.parseLong(isbn);
	}
}
