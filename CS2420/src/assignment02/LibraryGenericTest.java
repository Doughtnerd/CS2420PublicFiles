
package assignment02;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit Test Class for Generic Library.
 * 
 * @author Christopher Carlson, Brianne Young
 *
 */
public class LibraryGenericTest {

	private static final long[] ISBNS = { 123456789L, 123789456L, 789456123L, 147258369L };
	private static final String[] AUTHORS = { "John Doe", "Jane Doe", "Jack Deaux" };
	private LibraryGeneric<String> lib2;
	private LibraryGeneric<String> lib3;
	private LibraryGeneric<PhoneNumber> phoneLib;
	private LibraryGeneric<String> stringLib;
	private LibraryBookGeneric<PhoneNumber> phoneBook1;
	private LibraryBookGeneric<PhoneNumber> phoneBook2;
	private LibraryBookGeneric<PhoneNumber> phoneBook3;
	private LibraryBookGeneric<PhoneNumber> phoneBook4;
	private LibraryBookGeneric<String> stringBook1;
	private LibraryBookGeneric<String> stringBook2;
	private LibraryBookGeneric<String> stringBook3;
	private LibraryBookGeneric<String> stringBook4;
	private PhoneNumber p1 = new PhoneNumber("8016661234");
	private PhoneNumber p2 = new PhoneNumber("1112223333");

	@Before
	public void setUpPhoneLibWithAddAll() {
		phoneLib = new LibraryGeneric<>();
		phoneBook1 = new LibraryBookGeneric<PhoneNumber>(ISBNS[0], AUTHORS[0], "There's Idiots Everywhere");
		phoneBook2 = new LibraryBookGeneric<PhoneNumber>(ISBNS[1], AUTHORS[1], "You're Included");
		phoneBook3 = new LibraryBookGeneric<PhoneNumber>(ISBNS[2], AUTHORS[2], "But Hey, So Am I");
		phoneBook4 = new LibraryBookGeneric<PhoneNumber>(ISBNS[3], AUTHORS[2], "All I Know is Nothing");
		ArrayList<LibraryBookGeneric<PhoneNumber>> list = new ArrayList<>();
		list.add(phoneBook1);
		list.add(phoneBook2);
		list.add(phoneBook3);
		list.add(phoneBook4);
		phoneLib.addAll(list);
	}

	@Before
	public void setUpStringLibWithAddAll() {
		stringLib = new LibraryGeneric<>();
		stringBook1 = new LibraryBookGeneric<String>(ISBNS[0], AUTHORS[0], "There's Idiots Everywhere");
		stringBook2 = new LibraryBookGeneric<String>(ISBNS[1], AUTHORS[1], "You're Included");
		stringBook3 = new LibraryBookGeneric<String>(ISBNS[2], AUTHORS[2], "But Hey, So Am I");
		stringBook4 = new LibraryBookGeneric<String>(ISBNS[3], AUTHORS[2], "All I Know is Nothing");
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(stringBook1);
		list.add(stringBook2);
		list.add(stringBook3);
		list.add(stringBook4);
		stringLib.addAll(list);
	}

	@Before
	public void setUpAdditionalLibs() {
		lib2 = new LibraryGeneric<>();
		lib3 = new LibraryGeneric<>();
	}

	@Test
	public void testPhoneLibAdd() {
		LibraryGeneric<PhoneNumber> myLib = new LibraryGeneric<>();
		myLib.add(ISBNS[0], AUTHORS[0], "There's Idiots Everywhere");
		myLib.add(ISBNS[1], AUTHORS[1], "You're Included");
		myLib.add(ISBNS[2], AUTHORS[2], "But Hey, So Am I");
		assertEquals(null, phoneLib.lookup(ISBNS[0]));
		assertEquals(null, phoneLib.lookup(ISBNS[1]));
		assertEquals(null, phoneLib.lookup(ISBNS[2]));
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(p1, phoneLib.lookup(ISBNS[0]));
		assertEquals(1, phoneLib.lookup(p1).size());
		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 1, 1, 2016));
		assertEquals(2, phoneLib.lookup(p1).size());
		assertEquals(true, phoneLib.checkin(ISBNS[0]));
		assertEquals(1, phoneLib.lookup(p1).size());
		assertEquals("But Hey, So Am I", phoneLib.lookup(p1).get(0).getTitle());
	}

	@Test
	public void testStringLibAdd() {
		LibraryGeneric<String> myLib = new LibraryGeneric<>();
		assertEquals(0, myLib.getInventoryList().size());
		myLib.add(ISBNS[0], AUTHORS[0], "There's Idiots Everywhere");
		myLib.add(ISBNS[1], AUTHORS[1], "You're Included");
		myLib.add(ISBNS[2], AUTHORS[2], "But Hey, So Am I");

		assertEquals(null, stringLib.lookup(ISBNS[0]));
		assertEquals(null, stringLib.lookup(ISBNS[1]));
		assertEquals(null, stringLib.lookup(ISBNS[2]));
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals("Hubris", stringLib.lookup(ISBNS[0]));
		assertEquals(1, stringLib.lookup("Hubris").size());
		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 1, 1, 2016));
		assertEquals(2, stringLib.lookup("Hubris").size());
		assertEquals(true, stringLib.checkin(ISBNS[0]));
		assertEquals(1, stringLib.lookup("Hubris").size());
		assertEquals("But Hey, So Am I", stringLib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testPhoneLibAddAllArrayListOfLibraryBookGenericOfType() {
		assertEquals(null, phoneLib.lookup(ISBNS[0]));
		assertEquals(null, phoneLib.lookup(ISBNS[1]));
		assertEquals(null, phoneLib.lookup(ISBNS[2]));

		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(p1, phoneLib.lookup(ISBNS[0]));
		assertEquals(1, phoneLib.lookup(p1).size());

		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 1, 1, 2016));
		assertEquals(2, phoneLib.lookup(p1).size());

		assertEquals(true, phoneLib.checkin(ISBNS[0]));
		assertEquals(1, phoneLib.lookup(p1).size());

		assertEquals("But Hey, So Am I", phoneLib.lookup(p1).get(0).getTitle());
	}

	@Test
	public void testStringLibAddAllArrayListOfLibraryBookGenericOfType() {
		assertEquals(null, stringLib.lookup(ISBNS[0]));
		assertEquals(null, stringLib.lookup(ISBNS[1]));
		assertEquals(null, stringLib.lookup(ISBNS[2]));

		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals("Hubris", stringLib.lookup(ISBNS[0]));
		assertEquals(1, stringLib.lookup("Hubris").size());

		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 1, 1, 2016));
		assertEquals(2, stringLib.lookup("Hubris").size());

		assertEquals(true, stringLib.checkin(ISBNS[0]));
		assertEquals(1, stringLib.lookup("Hubris").size());

		assertEquals("But Hey, So Am I", stringLib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testPhoneLibLookupLong() {
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(null, phoneLib.lookup(ISBNS[2]));
		assertEquals(p1, phoneLib.lookup(ISBNS[0]));
	}

	@Test
	public void testStringLibLookupLong() {
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals(null, stringLib.lookup(ISBNS[2]));
		assertEquals("Hubris", stringLib.lookup(ISBNS[0]));
	}

	@Test
	public void testPhoneLibLookupType() {
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 30, 30, -1));
		assertEquals(0, phoneLib.lookup(p2).size());
		assertEquals(2, phoneLib.lookup(p1).size());
		assertEquals("There's Idiots Everywhere", phoneLib.lookup(p1).get(0).getTitle());
	}

	@Test
	public void testStringLibLookupType() {
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 30, 30, -1));
		assertEquals(0, stringLib.lookup("Llama").size());
		assertEquals(2, stringLib.lookup("Hubris").size());
		assertEquals("There's Idiots Everywhere", stringLib.lookup("Hubris").get(0).getTitle());
	}

	@Test
	public void testPhoneLibCheckout() {
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(false, phoneLib.checkout(159789456, p1, 1, 1, 2016));
		assertEquals(false, phoneLib.checkout(123456951, p2, 2, 4, 2016));
		assertEquals(false, phoneLib.checkout(236159478, p1, 20, 20, 2016));
		assertEquals(false, phoneLib.checkout(ISBNS[0], p1, 1, 2, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 30, 30, -1));
	}

	@Test
	public void testStringLibCheckout() {
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals(false, stringLib.checkout(159789456, "Hubris", 1, 1, 2016));
		assertEquals(false, stringLib.checkout(123456951, "Llama", 2, 4, 2016));
		assertEquals(false, stringLib.checkout(236159478, "Hubris", 20, 20, 2016));
		assertEquals(false, stringLib.checkout(ISBNS[0], "Hubris", 1, 2, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 30, 30, -1));
	}

	@Test
	public void testPhoneLibCheckinLong() {
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[1], p1, 2, 4, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 30, 30, -1));
		assertEquals(false, phoneLib.checkout(ISBNS[0], p2, 2, 4, 2016));
		assertEquals(true, phoneLib.checkin(ISBNS[0]));
		assertEquals(true, phoneLib.checkout(ISBNS[0], p2, 1, 2, 2016));
		assertEquals(true, phoneLib.checkin(ISBNS[0]));
		assertEquals(false, phoneLib.checkin(ISBNS[0]));
	}

	@Test
	public void testStringLibCheckinLong() {
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[1], "Hubris", 2, 4, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 30, 30, -1));
		assertEquals(false, stringLib.checkout(ISBNS[0], "Llama", 2, 4, 2016));
		assertEquals(true, stringLib.checkin(ISBNS[0]));
		assertEquals(true, stringLib.checkout(ISBNS[0], "Llama", 1, 2, 2016));
		assertEquals(true, stringLib.checkin(ISBNS[0]));
		assertEquals(false, stringLib.checkin(ISBNS[0]));
	}

	@Test
	public void testPhoneLibCheckinType() {
		assertEquals(true, phoneLib.checkout(ISBNS[0], p1, 1, 1, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[1], p1, 2, 4, 2016));
		assertEquals(true, phoneLib.checkout(ISBNS[2], p1, 30, 30, -1));
		assertEquals(false, phoneLib.checkout(ISBNS[0], p2, 2, 4, 2016));
		assertEquals(true, phoneLib.checkin(p1));
		assertEquals(true, phoneLib.checkout(ISBNS[0], p2, 1, 2, 2016));
		assertEquals(true, phoneLib.checkin(p2));
		assertEquals(false, phoneLib.checkin(p1));
	}

	@Test
	public void testStringLibCheckinType() {
		assertEquals(true, stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[1], "Hubris", 2, 4, 2016));
		assertEquals(true, stringLib.checkout(ISBNS[2], "Hubris", 30, 30, -1));
		assertEquals(false, stringLib.checkout(ISBNS[0], "Llama", 2, 4, 2016));
		assertEquals(true, stringLib.checkin("Hubris"));
		assertEquals(true, stringLib.checkout(ISBNS[0], "Llama", 1, 2, 2016));
		assertEquals(true, stringLib.checkin("Llama"));
		assertEquals(false, stringLib.checkin("Hubris"));
	}

	@Test
	public void testPhoneLibSortAuthor() {
		ArrayList<LibraryBookGeneric<PhoneNumber>> sorted = phoneLib.getOrderedByAuthor();
		assertEquals(AUTHORS[2], sorted.get(0).getAuthor());
		assertEquals("All I Know is Nothing", sorted.get(0).getTitle());
		assertEquals(AUTHORS[2], sorted.get(1).getAuthor());
		assertEquals("But Hey, So Am I", sorted.get(1).getTitle());
		assertEquals(AUTHORS[1], sorted.get(2).getAuthor());
		assertEquals(AUTHORS[0], sorted.get(3).getAuthor());
	}

	@Test
	public void testStringLibSortAuthor() {
		ArrayList<LibraryBookGeneric<String>> sorted = stringLib.getOrderedByAuthor();
		assertEquals(AUTHORS[2], sorted.get(0).getAuthor());
		assertEquals("All I Know is Nothing", sorted.get(0).getTitle());
		assertEquals(AUTHORS[2], sorted.get(1).getAuthor());
		assertEquals("But Hey, So Am I", sorted.get(1).getTitle());
		assertEquals(AUTHORS[1], sorted.get(2).getAuthor());
		assertEquals(AUTHORS[0], sorted.get(3).getAuthor());
	}

	@Test
	public void testPhoneLibSortDate() {
		phoneLib.checkout(ISBNS[0], this.p1, 1, 1, 2013);
		phoneLib.checkout(ISBNS[1], p1, 1, 2, 2013);
		phoneLib.checkout(ISBNS[2], p1, 1, 3, 2013);
		ArrayList<LibraryBookGeneric<PhoneNumber>> sorted = phoneLib.getOverdueList(1, 1, 2012);
		for (LibraryBookGeneric<PhoneNumber> b : sorted) {
			assertEquals(ISBNS[sorted.indexOf(b)], b.getIsbn());
		}
		sorted.clear();
		sorted = phoneLib.getOverdueList(1, 2, 2013);
		assertEquals(1, sorted.size());
		assertEquals(new GregorianCalendar(2013, 1, 3).toInstant(), sorted.get(0).getDueDate().toInstant());
		assertEquals(ISBNS[2], sorted.get(0).getIsbn());
		assertEquals(AUTHORS[2], sorted.get(0).getAuthor());
	}

	@Test
	public void testStringLibSortDate() {
		stringLib.checkout(ISBNS[0], "Hubris", 1, 1, 2013);
		stringLib.checkout(ISBNS[1], "Hubris", 1, 2, 2013);
		stringLib.checkout(ISBNS[2], "Hubris", 1, 3, 2013);
		ArrayList<LibraryBookGeneric<String>> sorted = stringLib.getOverdueList(1, 1, 2012);
		for (LibraryBookGeneric<String> b : sorted) {
			assertEquals(ISBNS[sorted.indexOf(b)], b.getIsbn());
		}
		sorted.clear();
		sorted = stringLib.getOverdueList(1, 2, 2013);
		assertEquals(1, sorted.size());
		assertEquals(new GregorianCalendar(2013, 1, 3).toInstant(), sorted.get(0).getDueDate().toInstant());
		assertEquals(ISBNS[2], sorted.get(0).getIsbn());
		assertEquals(AUTHORS[2], sorted.get(0).getAuthor());

		sorted.clear();
		sorted = stringLib.getOverdueList(1, 1, 2013);
		assertEquals(2, sorted.size());
		assertEquals(new GregorianCalendar(2013, 1, 3).toInstant(), sorted.get(1).getDueDate().toInstant());
		assertEquals(new GregorianCalendar(2013, 1, 2).toInstant(), sorted.get(0).getDueDate().toInstant());
	}

	@Test
	public void testPhoneLibSortDateEmpty() {
		ArrayList<LibraryBookGeneric<PhoneNumber>> sorted = phoneLib.getOverdueList(1, 1, 2012);
		assertEquals(0, sorted.size());
	}

	@Test
	public void testStringLibSortDateEmpty() {
		ArrayList<LibraryBookGeneric<String>> sorted = stringLib.getOverdueList(1, 1, 2012);
		assertEquals(0, sorted.size());
	}

	@Test
	public void originalTest() {

		// test a library that uses names (String) to id patrons
		LibraryGeneric<String> lib1 = new LibraryGeneric<String>();
		lib1.add(9780374292799L, "Thomas L. Friedman", "The World is Flat");
		lib1.add(9780330351690L, "Jon Krakauer", "Into the Wild");
		lib1.add(9780446580342L, "David Baldacci", "Simple Genius");

		String patron1 = AUTHORS[1];

		if (!lib1.checkout(9780330351690L, patron1, 1, 1, 2008))
			fail("TEST FAILED: first checkout");
		if (!lib1.checkout(9780374292799L, patron1, 1, 1, 2008))
			fail("TEST FAILED: second checkout");
		ArrayList<LibraryBookGeneric<String>> booksCheckedOut1 = lib1.lookup(patron1);
		if (booksCheckedOut1 == null || booksCheckedOut1.size() != 2
				|| !booksCheckedOut1.contains(new Book(9780330351690L, "Jon Krakauer", "Into the Wild"))
				|| !booksCheckedOut1.contains(new Book(9780374292799L, "Thomas L. Friedman", "The World is Flat"))
				|| !booksCheckedOut1.get(0).getHolder().equals(patron1)
				|| !booksCheckedOut1.get(0).getDueDate().equals(new GregorianCalendar(2008, 1, 1))
				|| !booksCheckedOut1.get(1).getHolder().equals(patron1)
				|| !booksCheckedOut1.get(1).getDueDate().equals(new GregorianCalendar(2008, 1, 1)))
			fail("TEST FAILED: lookup(holder)");
		if (!lib1.checkin(patron1))
			fail("TEST FAILED: checkin(holder)");

		// test a library that uses phone numbers (PhoneNumber) to id patrons
		LibraryGeneric<PhoneNumber> lib2 = new LibraryGeneric<PhoneNumber>();
		lib2.add(9780374292799L, "Thomas L. Friedman", "The World is Flat");
		lib2.add(9780330351690L, "Jon Krakauer", "Into the Wild");
		lib2.add(9780446580342L, "David Baldacci", "Simple Genius");

		PhoneNumber patron2 = new PhoneNumber("801.555.1234");

		if (!lib2.checkout(9780330351690L, patron2, 1, 1, 2008))
			fail("TEST FAILED: first checkout");
		if (!lib2.checkout(9780374292799L, patron2, 1, 1, 2008))
			fail("TEST FAILED: second checkout");
		ArrayList<LibraryBookGeneric<PhoneNumber>> booksCheckedOut2 = lib2.lookup(patron2);
		if (booksCheckedOut2 == null || booksCheckedOut2.size() != 2
				|| !booksCheckedOut2.contains(new Book(9780330351690L, "Jon Krakauer", "Into the Wild"))
				|| !booksCheckedOut2.contains(new Book(9780374292799L, "Thomas L. Friedman", "The World is Flat"))
				|| !booksCheckedOut2.get(0).getHolder().equals(patron2)
				|| !booksCheckedOut2.get(0).getDueDate().equals(new GregorianCalendar(2008, 1, 1))
				|| !booksCheckedOut2.get(1).getHolder().equals(patron2)
				|| !booksCheckedOut2.get(1).getDueDate().equals(new GregorianCalendar(2008, 1, 1)))
			fail("TEST FAILED: lookup(holder)");
		if (!lib2.checkin(patron2))
			fail("TEST FAILED: checkin(holder)");
	}

	@Test
	public void testGetAuthorSameNames() {
		// Tests ordering a library list with authors of the same name, thus
		// sorting off of the books
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(123456789, "John Doe", "There's Idiots Everywhere"));
		list.add(new LibraryBookGeneric<String>(123789456, "John Doe", "You're Included"));
		list.add(new LibraryBookGeneric<String>(789456123, "John Doe", "But Hey, So Am I"));
		lib2.addAll(list);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();
		list2.add(new LibraryBookGeneric<String>(789456123, "John Doe", "But Hey, So Am I"));
		list2.add(new LibraryBookGeneric<String>(123456789, "John Doe", "There's Idiots Everywhere"));
		list2.add(new LibraryBookGeneric<String>(123789456, "John Doe", "You're Included"));

		// lib3.addAll(list2);

		assertEquals(list2, lib2.getOrderedByAuthor());
	}

	@Test
	public void testGetAuthorMixNames() {
		// Tests ordering a library list with authors of different names and
		// some
		// same names to make sure mixes work
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		list.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "Hopfully"));
		list.add(new LibraryBookGeneric<String>(123789456, "Anne Doe", "You're Included"));
		lib2.addAll(list);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();
		list2.add(new LibraryBookGeneric<String>(123789456, "Anne Doe", "You're Included"));
		list2.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		list2.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "Hopfully"));
		list2.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		// lib3.addAll(list2);

		assertEquals(list2, lib2.getOrderedByAuthor());
	}

	@Test
	public void testOverdueNoneOverdue() {
		// Tests ordering a library list of overdue books with none books being
		// overdue
		// Should return an empty list
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		list.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		lib2.addAll(list);
		lib2.checkout(123789456, "Jarvis", 01, 01, 2016);
		lib2.checkout(789456123, "Peter Parker", 02, 01, 2016);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();

		assertEquals(list2, lib2.getOverdueList(01, 01, 2017));
	}

	@Test
	public void testOverdueAllOverdue() {
		// Tests ordering a library list of overdue books with all books being
		// overdue
		// Should return an every entry in the list
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		lib2.addAll(list);
		lib2.checkout(123789456, "Jarvis", 01, 01, 2016);
		lib2.checkout(789456123, "Peter Parker", 02, 01, 2016);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();
		list2.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		list2.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		lib3.addAll(list);
		lib3.checkout(123789456, "Jarvis", 01, 01, 2016);
		lib3.checkout(789456123, "Peter Parker", 02, 01, 2016);

		assertEquals(list2, lib2.getOverdueList(01, 01, 2010));
	}

	@Test
	public void testOverdueMixOverdue() {
		// Tests ordering a library list of overdue books with mix of books
		// being overdue
		// NOTE: does not test the Ordering portion, simply retriving the
		// correct isbn's
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		list.add(new LibraryBookGeneric<String>(123789455, "Bob Doe", "Hopfully"));
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		list.add(new LibraryBookGeneric<String>(123789433, "Anne Doe", "You're Included"));

		lib2.addAll(list);
		lib2.checkout(789456123, "Jarvis", 01, 01, 2016);
		lib2.checkout(123789455, "Jarvis", 02, 01, 2016);
		lib2.checkout(123789433, "Jarvis", 03, 01, 2016);
		lib2.checkout(123789456, "Jarvis", 04, 01, 2016);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();
		list2.add(new LibraryBookGeneric<String>(123789433, "Anne Doe", "You're Included"));
		list2.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));

		assertEquals(list2, lib2.getOverdueList(02, 15, 2016));
	}

	@Test
	public void testOverdueNoneCheckedOut() {
		// Tests ordering a library list of overdue books with mix of books
		// being overdue
		// NOTE: does not test the Ordering portion, simply retriving the
		// correct isbn's
		ArrayList<LibraryBookGeneric<String>> list = new ArrayList<>();
		list.add(new LibraryBookGeneric<String>(789456123, "Donny Deaux", "But Hey, So Am I"));
		list.add(new LibraryBookGeneric<String>(123789455, "Bob Doe", "Hopfully"));
		list.add(new LibraryBookGeneric<String>(123789433, "Anne Doe", "You're Included"));
		list.add(new LibraryBookGeneric<String>(123789456, "Bob Doe", "An Alphabetical Biography"));
		lib2.addAll(list);

		ArrayList<LibraryBookGeneric<String>> list2 = new ArrayList<>();

		assertEquals(list2, lib2.getOverdueList(02, 15, 2016));
	}

}
