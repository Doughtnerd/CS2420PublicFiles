package assignment03;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BinarySearchSetTest {
	//Implemented basic tests.
	BinarySearchSet<String> cStringSet;
	BinarySearchSet<String> nStringSet;
	BinarySearchSet<Integer> intSet;
	static ArrayList<String> sList;
	static ArrayList<Integer> iList;
	static ArrayList<String> pSMatchList;

	@BeforeClass
	public static void setUpArrayLists() {
		pSMatchList = new ArrayList<>();
		sList = new ArrayList<>();
		iList = new ArrayList<>();

		sList.add("hhhh");
		sList.add("ggg");
		sList.add("fffff");
		sList.add("ee");
		sList.add("dddddd");
		sList.add("ccccccc");
		sList.add("bbbbbbbb");
		sList.add("aaaaaaaaa");
		
		for (int i = 1000; i > 0; i--) {
			iList.add(i);
		}
	}

	@Before
	public void setupNaturalStringSet() {
		nStringSet = new BinarySearchSet<>();
	}

	@Before
	public void setupComparatorStringSet() {
		cStringSet = new BinarySearchSet<>(new StringLengthCompare());
	}

	@Before
	public void setupNaturalIntegerSet() {
		intSet = new BinarySearchSet<>();
	}

	@Test
	public void testComparatorNull() {
		assertEquals(null, nStringSet.comparator());
		assertEquals(null, intSet.comparator());
	}


	@Test
	public void testNStringSetFirst() {
		nStringSet.add("Bubble");
		nStringSet.add("Aardvark");
		assertEquals("Aardvark", nStringSet.first());
	}

	@Test
	public void testCStringSetFirst() {
		cStringSet.add("Aardvark");
		cStringSet.add("Bubble");
		assertEquals("Bubble", cStringSet.first());
	}

	@Test
	public void testIntSetFirst() {
		intSet.add(3);
		intSet.add(0);
		assertEquals(new Integer(0), intSet.first());
	}

	@Test
	public void testNStringSetLast() {
		nStringSet.add("Bubble");
		nStringSet.add("Aardvark");
		assertEquals("Bubble", nStringSet.last());
	}

	@Test
	public void testCStringSetLast() {
		cStringSet.add("Aardvark");
		cStringSet.add("Bubble");
		assertEquals("Aardvark", cStringSet.last());
	}

	@Test
	public void testIntSetLast() {
		intSet.add(3);
		intSet.add(0);
		assertEquals(new Integer(3), intSet.last());
	}

	@Test
	public void testNStringSetAdd() {
		assertEquals(true, nStringSet.add("Bubble"));
		assertEquals(true, nStringSet.add("Aardvark"));
		assertEquals(false, nStringSet.add("Bubble"));
		assertEquals(false, nStringSet.contains(null));
	}

	@Test
	public void testCStringSetAdd() {
		assertEquals(true, cStringSet.add("Aardvark"));
		assertEquals(true, cStringSet.add("Bubble"));
		assertEquals(false, cStringSet.add("Bubble"));
	}

	@Test
	public void testIntSetAdd() {
		assertEquals(true, intSet.add(3));
		assertEquals(true, intSet.add(0));
		assertEquals(false, intSet.add(0));
	}

	@Test
	public void testNStringSetIndexOf() {
		nStringSet.add("Bubble");
		nStringSet.add("Aardvark");
		assertEquals(1, nStringSet.indexOf("Bubble"));
		assertEquals(0, nStringSet.indexOf("Aardvark"));
	}

	@Test
	public void testCStringSetIndexOf() {
		cStringSet.add("Bubble");
		cStringSet.add("Aardvark");
		assertEquals(1, cStringSet.indexOf("Aardvark"));
		assertEquals(0, cStringSet.indexOf("Bubble"));
	}

	@Test
	public void testIntSetIndexOf() {
		intSet.add(3);
		intSet.add(0);
		assertEquals(1, intSet.indexOf(3));
		assertEquals(0, intSet.indexOf(0));
	}

	@Test
	public void testNStringSetAddAll() {
		assertEquals(true, nStringSet.addAll(sList));
		assertEquals(0, nStringSet.indexOf("aaaaaaaaa"));
		assertEquals(1, nStringSet.indexOf("bbbbbbbb"));
	}

	@Test
	public void testCStringSetAddAll() {
		assertEquals(true, cStringSet.addAll(sList));
		assertEquals(7, cStringSet.indexOf("aaaaaaaaa"));
		assertEquals(6, cStringSet.indexOf("bbbbbbbb"));
	}

	@Test
	public void testIntSetAddAll() {
		assertEquals(true, intSet.addAll(iList));
		assertEquals(0, intSet.indexOf(1));
		assertEquals(1, intSet.indexOf(2));
	}

	@Test
	public void testNStringSetClear() {
		assertEquals(true, nStringSet.addAll(sList));
		assertEquals(8, nStringSet.size());
		nStringSet.clear();
		assertEquals(0, nStringSet.size());
	}

	@Test
	public void testCStringSetClear() {
		assertEquals(true, cStringSet.addAll(sList));
		assertEquals(8, cStringSet.size());
		cStringSet.clear();
		assertEquals(0, cStringSet.size());
	}

	@Test
	public void testIntSetClear() {
		assertEquals(true, intSet.addAll(iList));
		assertEquals(1000, intSet.size());
		intSet.clear();
		assertEquals(0, intSet.size());
	}

	@Test
	public void testNStringSetContains() {
		assertEquals(false, nStringSet.contains("ee"));
		assertEquals(true, nStringSet.addAll(sList));
		assertEquals(true, nStringSet.contains("ee"));
	}

	@Test
	public void testCStringSetContains() {
		assertEquals(false, cStringSet.contains("ee"));
		assertEquals(true, cStringSet.addAll(sList));
		assertEquals(true, cStringSet.contains("ee"));
	}

	@Test
	public void testIntSetContains() {
		assertEquals(false, intSet.contains(1));
		intSet.addAll(iList);
		assertEquals(true, intSet.contains(1));
		assertFalse(intSet.contains(0));
	}

	@Test
	public void testNStringSetContainsAll() {
		assertEquals(false, nStringSet.containsAll(sList));
		assertEquals(true, nStringSet.addAll(sList));
		assertEquals(true, nStringSet.containsAll(sList));
	}

	@Test
	public void testCStringSetContainsAll() {
		assertEquals(false, cStringSet.containsAll(sList));
		assertEquals(true, cStringSet.addAll(sList));
		assertEquals(true, cStringSet.containsAll(sList));
	}

	@Test
	public void testIntSetContainsAll() {
		assertEquals(false, intSet.containsAll(iList));
		intSet.addAll(iList);
		assertEquals(true, intSet.containsAll(iList));
	}

	@Test
	public void testNStringSetIsEmpty() {
		assertEquals(true, nStringSet.isEmpty());
		nStringSet.addAll(sList);
		assertEquals(false, nStringSet.isEmpty());
	}

	@Test
	public void testCStringSetIsEmpty() {
		assertEquals(true, cStringSet.isEmpty());
		cStringSet.addAll(sList);
		assertEquals(false, cStringSet.isEmpty());
	}

	@Test
	public void testIntSetIsEmpty() {
		assertEquals(true, intSet.isEmpty());
		intSet.addAll(iList);
		assertEquals(false, intSet.isEmpty());
	}

	@Test
	public void testNStringSetIterator() {
		Iterator<String> iter = nStringSet.iterator();
		assertEquals(false, iter.hasNext());
		nStringSet.addAll(sList);
		assertEquals(true, iter.hasNext());
		int i = 7;
		while (iter.hasNext()) {
			assertEquals(sList.get(i), iter.next());
			i--;
		}
		assertEquals(false, iter.hasNext());
	}

	@Test
	public void testCStringSetIterator() {
		Iterator<String> iter = cStringSet.iterator();
		assertEquals(false, iter.hasNext());
		cStringSet.addAll(sList);
		assertEquals(true, iter.hasNext());
		int i = 0;
		while (iter.hasNext()) {
			String s = iter.next();
			if (i == 0) {
				assertEquals(sList.get(3), s);
			} else if (i == 2) {
				assertEquals(sList.get(0), s);
			} else if (i == 3) {
				assertEquals(sList.get(2), s);
			} else {
				assertEquals(sList.get(i), s);
			}
			i++;
		}
		assertEquals(false, iter.hasNext());
		assertEquals(8, i);
	}

	@Test
	public void testIntSetIterator() {
		Iterator<Integer> iter = intSet.iterator();
		assertEquals(false, iter.hasNext());
		intSet.addAll(iList);
		assertEquals(true, iter.hasNext());
		int i = 999;
		while (iter.hasNext()) {
			Integer e = iter.next();
			assertEquals(iList.get(i), e);
			i--;
		}
		assertEquals(false, iter.hasNext());
	}

	@Test
	public void testNStringSetRemoveWithNext() {
		nStringSet.add("Bubble");
		assertEquals(1, nStringSet.size());
		//needs to call Next. I am not sure how to do that
		//Any test that has "WithNext" needs to call next()
		nStringSet.iterator();
		
		assertTrue(nStringSet.remove("Bubble"));
		assertEquals(0, nStringSet.size());
		assertFalse(nStringSet.contains("Bubble"));
	}

	@Test
	public void testCStringSetRemoveWithNext() {
		assertFalse(cStringSet.remove("Bubble"));
		cStringSet.add("Bubble");
		assertEquals(1, cStringSet.size());
		//needs to call Next. I am not sure how to do that
				cStringSet.iterator();
		assertTrue(cStringSet.remove("Bubble"));
		assertEquals(0, cStringSet.size());
		assertFalse(cStringSet.contains("Bubble"));
	}

	@Test
	public void testIntSetRemoveWithNext() {
		assertFalse(intSet.remove(1));
		intSet.add(1);
		assertEquals(1, intSet.size());
		//needs to call Next. I am not sure how to do that
				intSet.iterator();
		assertTrue(intSet.remove(1));
		assertEquals(0, intSet.size());
		assertFalse(intSet.contains(1));
	}
	
	@Test
	public void testIntSetIterRemoveTwiceInARow() {
		intSet.add(0);
		intSet.add(1);
		intSet.add(2);
		int i = 0;
		Iterator<Integer> iter = intSet.iterator();
		while(iter.hasNext()){
			assertEquals(new Integer(i++), iter.next());
		}
		iter = intSet.iterator();
		assertEquals(true, iter.hasNext());
		iter.next();
		iter.remove();
		assertEquals(true, iter.hasNext());
		try {
			iter.remove();
			fail("Exception not caught");
		} catch(IllegalStateException e){
			
		}
	}
	
	@Test
	public void testNStringSetRemoveAllWithNext() {
		assertFalse(nStringSet.removeAll(sList));
		nStringSet.add("Bubble");
		nStringSet.add("hhhh");
		assertEquals(2, nStringSet.size());
		assertTrue(nStringSet.removeAll(sList));
		assertEquals(1, nStringSet.size());
	}
	
	@Test
	public void testStringSetRemoveAllWithNext() {
		assertFalse(cStringSet.removeAll(sList));
		cStringSet.add("Bubble");
		cStringSet.add("hhhh");
		assertEquals(2, cStringSet.size());
		assertTrue(cStringSet.removeAll(sList));
		assertEquals(1, cStringSet.size());
	}
	
	@Test 
	public void testIntIter() {
		intSet.add(1);
		Iterator<Integer> iter = intSet.iterator();
		assertEquals(true, iter.hasNext());
		try{
			iter.remove();
			fail("Exception not caught");
		} catch (IllegalStateException e){
			
		}
	}
	
	@Test
	public void testConcurrency(){
		intSet.add(0);
		intSet.add(1);
		intSet.add(2);
		Iterator<Integer> iter = intSet.iterator();
		iter.next();
		iter.remove();
		try{
			iter.remove();
			fail("Exception not caught");
		} catch(IllegalStateException e){
			
		}
		
		intSet.remove(1);
		try{
			iter.remove();
			fail("Exception not caught");
		} catch(ConcurrentModificationException e){
			
		}
	}
	
	@Test
	public void testIntSetRemoveAllWithNext() {
		
		assertFalse(intSet.removeAll(iList));
		intSet.add(1);
		assertEquals(1, intSet.size());
		assertTrue(intSet.removeAll(iList));
		assertEquals(0, intSet.size());
	}
	
	
	@Test
	public void testSize() {
		//fail("Not Implemented");
	}

	@Test
	public void testNStringSetToArray() {
		nStringSet.addAll(sList);
		Object[] array = nStringSet.toArray();
		for(int i = 7, j = 0; i >= 0; i--, j++){
			assertEquals(sList.get(i), array[j]);
		}	
	}
	
	@Test
	public void testIntSetGrowThenClear(){
		for(int i = 0; i<25; i++){
			intSet.add(i);
		}
		assertEquals(25,intSet.size());
		//Now the size is 55. What ahappens when it's cleared?
		intSet.clear();
		assertEquals(0, intSet.size());

	}
	@Test
	public void testIntSetAddDup(){
		intSet.add(1);
		intSet.add(2);
		assertEquals(2, intSet.size());
		assertFalse(intSet.add(2));

	}
	@Test
	public void testCStringSetAddDup(){
		cStringSet.add("Test");
		assertFalse(cStringSet.add("Test"));
	}
	@Test
	public void testNStringSetAddDup(){
		nStringSet.add("Test");
		assertFalse(nStringSet.add("Test"));
	}

	/**
	 * Compares two strings based on length. If the strings are of equal length,
	 * compares them based off of lexicographical order
	 */
	class StringLengthCompare implements Comparator<String> {

		/**
		 * Compares two strings based on length. If the strings are of equal
		 * length, compares them based off of lexicographical order
		 * 
		 * @param lhs
		 *            - Left hand side of the comparison.
		 * @param rhs
		 *            - Right hand side of the comparison.
		 * @return -1 if lhs < rhs, 0 if lhs == rhs, 1 if lhs > rhs.
		 */
		@Override
		public int compare(String lhs, String rhs) {
			if (lhs.length() > rhs.length()) {
				return 1;
			} else if (lhs.length() < rhs.length()) {
				return -1;
			} else {
				if (lhs.compareTo(rhs) < 0) {
					return -1;
				} else if (lhs.compareTo(rhs) > 0) {
					return 1;
				} else {
					return 0;
				}
			}
		}

	}
}
