package assignment03;

import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import assignment03.SortedSet;

/**
 * A data structure that utilizes binary search to add, remove, and find
 * elements it contains.
 * 
 * @author Christopher Carlson, Brianne Young
 *
 * @param <E>
 *            Any Object that implements comparable or will be used with a
 *            comparator.
 */
public class BinarySearchSet<E> implements SortedSet<E>, Iterable<E> {
	/**
	 * Initial size of the data array.
	 */
	private static final int INIT_SIZE = 10;

	/**
	 * The generic type data array backing the data structure.
	 */
	private E[] data;

	/**
	 * The size of the data array as far as how many elements are contained
	 * within it.
	 */
	private int size;

	/**
	 * The comparator this data structure uses for ordering. Follows natural
	 * order if null.
	 */
	private Comparator<? super E> comparator;

	private int modCount;
	/**
	 * If this constructor is used to create the sorted set, it is assumed that
	 * the elements are ordered using their natural ordering (i.e., E implements
	 * Comparable<? super E>).
	 */
	public BinarySearchSet() {
		this(null);
	}

	/**
	 * If this constructor is used to create the sorted set, it is assumed that
	 * the elements are ordered using the provided comparator.
	 * 
	 * @param comparator
	 *            Comparator to use for sorting.
	 */
	@SuppressWarnings("unchecked")
	public BinarySearchSet(Comparator<? super E> comparator) {
		modCount = 0;
		data = (E[]) new Object[INIT_SIZE];
		this.comparator = comparator;
	}

	/**
	 * @return The comparator used to order the elements in this set, or null if
	 *         this set uses the natural ordering of its elements (i.e., uses
	 *         Comparable).
	 */
	@Override
	public Comparator<? super E> comparator() {
		return comparator;
	}

	/**
	 * @return the first (lowest, smallest) element currently in this set
	 * @throws NoSuchElementException
	 *             if the set is empty
	 */
	@Override
	public E first() throws NoSuchElementException {
		if (size == 0) {
			throw new NoSuchElementException();
		} else {
			return data[0];
		}
	}

	/**
	 * @return the last (highest, largest) element currently in this set
	 * @throws NoSuchElementException
	 *             if the set is empty
	 */
	@Override
	public E last() throws NoSuchElementException {
		if (size == 0) {
			throw new NoSuchElementException();
		} else {
			return data[size - 1];
		}
	}

	/**
	 * Adds a single element to the array and recursively shifts elements around
	 * the insertion point. If the array is at maximum capacity, this method
	 * makes a call to the growArray method and recursively calls add.
	 * 
	 * @param element
	 *            The element to add to the array.
	 * @return True if the element was successfully added, false if the element
	 *         already exists in the array.
	 */
	@Override
	public boolean add(E element) {
		if(element == null){
			return false;
		}
		if (size != data.length) {
			if (indexOf(element) == -1) {
				int iPt = this.findInsertionPoint(element, 0, size - 1);
				if (data[iPt] != null) {
					E temp = data[iPt];
					data[iPt] = element;
					return add(temp);
				} else {
					data[iPt] = element;
					size++;
					return true;
				}
			} else {
				return false;
			}
		} else {
			growArray();
			return add(element);
		}
	}

	/**
	 * This method finds the correct insertion point for a new element in the
	 * array in order to maintain a sorted order.
	 * 
	 * @param element
	 *            Element to find a location for.
	 * @param left
	 *            The lower bound of the array.
	 * @param right
	 *            The upper bound of the array.
	 * @return The index that should be used for inserting the new element.
	 */
	@SuppressWarnings("unchecked")
	private int findInsertionPoint(E element, int left, int right) {
		if (size == 0) {
			return size;
		}
		int insertionPoint = 0;
		int mid = (left + right) / 2;
		while (left <= right) {
			mid = (left + right) / 2;
			if (comparator != null) {
				if (comparator.compare(data[mid], element) < 0) {
					left = mid + 1;
				} else if (comparator.compare(data[mid], element) > 0) {
					right = mid - 1;
				}
			} else {
				if (((Comparable<E>) data[mid]).compareTo(element) < 0) {
					left = mid + 1;
				} else if (((Comparable<E>) data[mid]).compareTo(element) > 0) {
					right = mid - 1;
				}
			}
		}
		if (comparator != null) {
			if (comparator.compare(data[mid], element) <= 0) {
				insertionPoint = mid + 1;
			} else {
				insertionPoint = mid;
			}
		} else {
			if (((Comparable<E>) data[mid]).compareTo(element) <= 0) {
				insertionPoint = mid + 1;
			} else {
				insertionPoint = mid;
			}
		}
		return insertionPoint;
	}

	/**
	 * Called when the array runs out of room, allocates twice the previous
	 * space for the data array.
	 */
	private void growArray() {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[size * 2];
		for (int i = 0; i < size; i++) {
			temp[i] = data[i];
		}
		data = temp;
	}

	/**
	 * Returns the index of the given element, if it exists in the array.
	 * 
	 * @param element
	 *            Element to look for
	 * @return The index of the element in the data array, -1 if the element
	 *         does not exist.
	 */
	@SuppressWarnings("unchecked")
	public int indexOf(E element) {
		if (size == 0 || element == null) {
			return -1;
		}
		int left, right;
		left = 0;
		right = size - 1;
		while (left <= right) {
			int mid = (left + right) / 2;
			if (comparator != null) {
				if (comparator.compare(data[mid], element) < 0) {
					left = mid + 1;
				} else if (comparator.compare(data[mid], element) == 0) {
					return mid;
				} else {
					right = mid - 1;
				}
			} else {
				if (((Comparable<E>) data[mid]).compareTo((E) element) < 0) {
					left = mid + 1;
				} else if (((Comparable<E>) data[mid]).compareTo((E) element) == 0) {
					return mid;
				} else {
					right = mid - 1;
				}
			}
		}
		return -1;
	}

	/**
	 * Adds each element in the Collection to the array.
	 * 
	 * @param elements
	 *            Collection of elements.
	 * @return true if all elements were successfully added, false if the array
	 *         already contains one or more of the elements.
	 */
	@Override
	public boolean addAll(Collection<? extends E> elements) {
		boolean flag = false;
		for (E e : elements) {
			if (add(e)){
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * Clears all elements from the array.Since there was no specification about returning data to the initial size, or keep it the same
	 * we chose to set it to the initial size to grow again if needed 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		data = (E[]) new Object[INIT_SIZE];
		size = 0;

	}

	/**
	 * Searches through the array to see if it contains element.
	 * 
	 * @param element
	 *            Object to search for.
	 * @return true if the array contains the object, false otherwise.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean contains(Object element) {
		if(element == null){
			return false;
		}
		if (this.indexOf((E) element) != -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Searches through the array to see if it contains all elements contained
	 * within the Collection.
	 * 
	 * @param elements
	 *            Collection to be checked for containment in this set
	 * @return true if this set contains all of the elements of the specified
	 *         collection
	 */
	@Override
	public boolean containsAll(Collection<?> elements) {
		for (Object e : elements) {
			if (contains(e) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks the array to see if it contains any data.
	 * 
	 * @return true if the array is empty, false otherwise.
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/**
	 * Returns an Iterator for this data set. Protects against Concurrent modification
	 * and doesn't allow for calling remove without first calling next.
	 */
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private boolean nextCalled = false;
			private int iterModCount = modCount;
			private int index = -1;
			
			public boolean hasNext() {
				return index + 1 < size && data[index + 1] != null;
			}

			public void remove() {
				checkConcurrency();
				if(hasNext()){
					if(nextCalled==true){
						BinarySearchSet.this.remove((Object) data[index]);
						iterModCount = modCount;
						index--;
						nextCalled = false;
					} else {
						throw new IllegalStateException();
					}
				}
			}
			
			private void checkConcurrency(){
				if(modCount!=iterModCount){
					throw new ConcurrentModificationException();
				}
			}

			public E next() {
				checkConcurrency();
				if (hasNext()) {
					nextCalled = true;
					return data[++index];
				} else {
					return null;
				}
			}
			
		};
	}

	/**
	 * Removes the specified element from this set if it is present.
	 * 
	 * @param element
	 *            Object to be removed from this set, if present
	 * @return true if this set contained the specified element
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean remove(Object element) {
		if(element == null){
			return false;
		}
		if (indexOf((E) element) == -1) {
			return false;
		} else {
			data[indexOf((E) element)] = null;
			removeGap();
			modCount++;
			size--;
			return true;
		}
	}

	/**
	 * This scans the array for gaps in data in case of a remove, and closes the
	 * gap so proper sorting/use of the array can continue.
	 */
	private void removeGap() {
		@SuppressWarnings("unchecked")
		E[] temp = (E[]) new Object[size];
		int offset = 0;
		for (int i = 0; i < size; i++) {
			if (data[i] != null) {
				temp[i - offset] = data[i];
			} else {
				offset++;
			}
		}
		data = temp;
	}

	/**
	 * Removes from this set all of its elements that are contained in the
	 * specified collection.
	 * 
	 * @param elements
	 *            Collection containing elements to be removed from this set
	 * @return true if this set changed as a result of the call
	 */
	@Override
	public boolean removeAll(Collection<?> elements) {
		
		boolean flag = false;
		for (Object e : elements) {
			if (remove(e)) {
				flag = true;
				
			}
		}
		return flag;
	}

	/**
	 * Returns the current size of the array.
	 * 
	 * @return Size of the array.
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Loops through the data contained in the array and creates an Object[].
	 * 
	 * @return The data contained in the array as an Object[].
	 */
	@Override
	public Object[] toArray() {
		Object[] result = new Object[size];
		for (int i = 0; i < size; i++) {
			result[i] = data[i];
		}
		return result;
	}



}
