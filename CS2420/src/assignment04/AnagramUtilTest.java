package assignment04;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnagramUtilTest {
	String reallyShortList[]={"elephant"};
	String shortList[]={ "staider", "gallery", "tardies", "astride", "allergy"};
	String randomizedSorted[] = {"allergy","astride","gallery","staider","tardies"};
	String backward[]={"castor", "costar", "actors"};
	String backwardSorted[] = {"actors", "costar", "castor"};
	String sorted[]={"actors", "costar", "castor"};
	
	String longList[]={"staider", "gallery", "tardies", "astride", "allergy", "abets", "beast", "beats", "betas", "baste"};

	
	@Test
	public void testSort() {
		String s = "back";
		assertEquals("abck", AnagramUtil.sort(s));
		assertEquals("Begin", AnagramUtil.sort("Begin"));
		assertEquals("Begin", AnagramUtil.sort("Being"));
		assertEquals("aaabbb", AnagramUtil.sort("bababa"));
	}
	
	@Test
	public void testSortNullAndEmpty(){
		assertEquals("", AnagramUtil.sort(null));
		assertEquals("", AnagramUtil.sort(""));
	}

	@Test
	public void testInsertionSortLength1() {
		AnagramUtil.insertionSort(reallyShortList, null);
		String token = reallyShortList[0];
		assertEquals("elephant", token);
	}
	
	@Test
	public void testInsertionSortAlreadySorted() {
		AnagramUtil.insertionSort(sorted, null);
		for(int i =0; i>sorted.length; i++){
			String token =sorted[i];
			assertEquals(token, sorted[i]);
		}
	}
	
	@Test
	public void testInsertionSortBackwards() {
		AnagramUtil.insertionSort(backward, null);
		for(int i =0; i>backward.length; i++){
			String token =backwardSorted[i];
			assertEquals(token, backward[i]);
		}
	}
	
	@Test
	public void testInsertionSortRandomized() {
		AnagramUtil.insertionSort(shortList, null);
		for(int i =0; i>shortList.length; i++){
			String token =randomizedSorted[i];
			assertEquals(token, shortList[i]);
		}
	}
	
	@Test
	public void testAreAnagrams() {
		assertTrue(AnagramUtil.areAnagrams("Begin", "Being"));
	}
	
	@Test
	public void testAreAnagramsLhsCapitalized() {
		assertTrue(AnagramUtil.areAnagrams("Kentucky", "kenckuty"));
	}
	
	@Test
	public void testAreAnagramsRHSCapitals() {
		assertTrue(AnagramUtil.areAnagrams("potato", "taPOtO"));
	}
	
	@Test
	public void testAreAnagramsEmpty() {
		assertFalse(AnagramUtil.areAnagrams("", ""));
		assertFalse( AnagramUtil.areAnagrams(null, ""));
		assertFalse(AnagramUtil.areAnagrams(null, null));
		assertFalse( AnagramUtil.areAnagrams("", null));
	}
	
	@Test
	public void testGetLargestAnagramGroupStringArray() {
		String[] array = AnagramUtil.getLargestAnagramGroup(longList);
		String[] largestAnagram = {"abets","beast","beats","betas","baste"};
		assertArrayEquals(largestAnagram, array);
	}

	@Test
	public void testGetLargestAnagramGroupStringFile() {
		String[] array = AnagramUtil.getLargestAnagramGroup("sample_word_list.txt");
		String[] fileArray ={"carets","Caters","caster","crates","Reacts","recast","traces"};
		assertArrayEquals(fileArray, array);
	}
	
	@Test
	public void testGetLargestAnagramGroupEmpty() {
		//Try a file that does not exist. Should return empty array
		String[] emptyArray = {};
		String[] array = AnagramUtil.getLargestAnagramGroup("non_existent_file.txt");
		assertArrayEquals(emptyArray, array);
		
		//Try an empty file
		String[] array2 = AnagramUtil.getLargestAnagramGroup("empty.txt");
		assertArrayEquals(emptyArray, array2);
	}
	
	@Test
	public void testGetLargestAnagramGroupStringFileNoAnagrams() {
		//Reading a file with no anagrams
		String[] array = AnagramUtil.getLargestAnagramGroup("file_with_no_anagrams.txt");
		String[] fileArray ={ };
		assertArrayEquals(fileArray, array);
	}
	
	@Test
	public void testGetLargestAnagramGroupStringArrayNoAnagrams() {
		String[] largestAnagram = {"list","of","words","with","nonesense"};
		String[] fileArray ={};
		//Finding the largest group in an array with no anagrams
		String[] array2 = AnagramUtil.getLargestAnagramGroup(largestAnagram);
		
		assertArrayEquals(fileArray, array2);
		
	}
}
