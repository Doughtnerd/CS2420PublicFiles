package assignment04;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class built to find collections of anagrams in a given list or file.
 * 
 * @author Christopher Carlson, Brianne Young
 *
 */
public class AnagramUtil {

	/**
	 * This method returns the sorted version of the input string. The sorting
	 * must be accomplished using an insertion sort.
	 * 
	 * @param input
	 *            String to sort
	 * @return The input String with its characters sorted lexicographically if
	 *         input is not null and not empty.
	 */
	public static String sort(String input) {
		if (input == null || input.length() == 0) {
			return "";
		}
		String[] array = input.split("(?!^)");
		insertionSort(array, new IgnoreStringCaseComparator());
		StringBuilder builder = new StringBuilder();
		for (String s : array) {
			builder.append(s);
		}
		String retval = builder.toString();
		return retval;
	}

	/**
	 * This generic method sorts the input array using an insertion sort and the
	 * input Comparator object. If comparator is null, sorts the objects based on 
	 * their natural order.
	 * 
	 * @param list
	 *            The array to sort.
	 * @param comparator
	 *            The comparator to use for sorting.
	 */
	@SuppressWarnings("unchecked")
	public static <T> void insertionSort(T[] list, Comparator<? super T> comparator) {
		if (list == null || list.length == 0 || list.length == 1) {
			return;
		}
		for (int i = 1; i < list.length; i++) {
			T key = list[i];
			int j = i;
			if (comparator != null) {
				while (j > 0 && comparator.compare(key, list[j - 1]) < 0) {
					T temp = list[j - 1];
					list[j - 1] = key;
					list[j] = temp;
					j--;
				}
			} else {
				while (j > 0 && ((Comparable<T>) key).compareTo(list[j - 1]) < 0) {
					T temp = list[j - 1];
					list[j - 1] = key;
					list[j] = temp;
					j--;
				}
			}
		}
	}

	/**
	 * This method returns true if the two input strings are anagrams of each
	 * other, otherwise returns false.
	 * 
	 * @param lhs
	 *            Left hand side of the argument.
	 * @param rhs
	 *            Right hand side of the argument
	 * @return True if lhs is equal to rhs, ignoring case. False otherwise.
	 */
	public static boolean areAnagrams(String lhs, String rhs) {
		if (!(lhs == null || lhs.equals("") || rhs == null || rhs.equals(""))) {
			if (sort(lhs).compareToIgnoreCase(sort(rhs)) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method returns the largest group of anagrams in the input array of
	 * words, in no particular order. It returns an empty array if there are no
	 * anagrams in the input array.
	 * 
	 * @param inputArray
	 *            The array to search for the largest group of anagrams in.
	 * 
	 * @return The largest group of anagrams found in the array.
	 */
	public static String[] getLargestAnagramGroup(String[] input) {
		if (input == null || input.length == 0) {
			return new String[0];
		}
		insertionSort(input, new StringAnagramComparator());
		ArrayList<ArrayList<String>> container = new ArrayList<>();
		ArrayList<String> temp = null;
		for (int i = 0; i < input.length; i++) {
			temp = new ArrayList<>();
			temp.add(input[i]);
			while (i < input.length - 1 && areAnagrams(input[i], input[i + 1])) {
				temp.add(input[i + 1]);
				i++;
			}
			if (temp.size() != 1) {
				container.add(temp);
			}
		}
		int maxIndex = -1;
		int maxSize = 0;
		for (int i = 0; i < container.size(); i++) {
			if (container.get(i).size() > maxSize) {
				maxIndex = i;
				maxSize = container.get(i).size();
			}
		}
		String[] a = new String[0];
		if (container.size() != 0) {
			return container.get(maxIndex).toArray(a);
		} else {
			return a;
		}
		// Too bad this couldn't be used....
		/*
		 * TreeMap<Integer, String[]> tree = new TreeMap<>(); ArrayList<String>
		 * list = new ArrayList<>(); for (int i = 0; i < input.length; i++) {
		 * String key = input[i]; for (int j = i + 1; j < input.length; j++) {
		 * String toCompare = input[j]; if (areAnagrams(key, toCompare)) {
		 * 
		 * if (list.contains(key) == false) { list.add(key);
		 * 
		 * } list.add(input[j]); } } String[] retval = new String[1];
		 * tree.put(list.size(), list.toArray(retval)); list = new
		 * ArrayList<>(); } if (tree.lastEntry() == null) { return new
		 * String[0]; } else { return tree.lastEntry().getValue(); }
		 */
	}

	/**
	 * Reads the list of words from the input filename. It is assumed that the
	 * file contains one word per line. If the file does not exist or is empty,
	 * the method returns an empty array because there are no anagrams.
	 * 
	 * @param filename
	 *            The file to read into an array.
	 * @return The largest group of anagrams contained in the file.
	 */
	public static String[] getLargestAnagramGroup(String filename) {
		ArrayList<String> results = new ArrayList<String>();
		String[] retval = new String[0];
		try {

			BufferedReader input = new BufferedReader(new FileReader(filename));
			while (input.ready()) {
				String line = input.readLine();
				results.add(line);
			}
			input.close();
		} catch (Exception e) {
			return retval;
		}
		return getLargestAnagramGroup(results.toArray(retval));

	}

	/**
	 * A comparator class that compares two strings, ignoring case.
	 * 
	 * @author Christopher Carlson, Brianne Young
	 *
	 */
	private static class IgnoreStringCaseComparator implements Comparator<String> {

		/**
		 * Compares two strings, ignoring their case.
		 * 
		 * @param lhs
		 *            Left hand side of the argument.
		 * @param rhs
		 *            Right hand side of the argument.
		 * @return An int less than, equal to, or greater than 0 if lhs is less
		 *         than, equal to, or greater than rhs.
		 */
		@Override
		public int compare(String lhs, String rhs) {
			return (lhs.compareToIgnoreCase(rhs));
		}
	}

	/**
	 * A comparator class that compares the sorted representations of two words,
	 * ignoring case. This is used for determining if two words are anagrams of
	 * eachother.
	 * 
	 * @author Christopher Carlson, Brianne Young
	 *
	 */
	private static class StringAnagramComparator implements Comparator<String> {

		/**
		 * Compares the sorted representation of two strings.
		 * 
		 * @param lhs
		 *            Left hand side of the argument.
		 * @param rhs
		 *            Right hand side of the argument.
		 * @return An int less than, equal to, or greater than 0 if lhs is less
		 *         than, equal to, or greater than rhs.
		 */
		@Override
		public int compare(String lhs, String rhs) {
			return AnagramUtil.sort(lhs).compareToIgnoreCase(AnagramUtil.sort(rhs));
		}

	}

}
