package assignment04;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class AnagramTester {

	private static final int ITER_COUNT = 3;
	private static Random rand;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		
		long startTime = System.nanoTime();
		while (System.nanoTime() - startTime < 1000000000);
		
		try(FileWriter fw = new FileWriter(new File("find_largest_experiment.tsv"))) { //open up a file writer so we can write to file.
			for(int exp = 1; exp <= 20; exp++) { // This is used as the exponent to calculate the size of the set.
				int size = (int) Math.pow(2, exp); // or ..  
				//size = 1 << exp; // the two statements are equivalent, look into bit-shifting if you're interested what is going on here.
				// Do the experiment multiple times, and average out the results
				long totalTime = 0;
				for (int iter = 0; iter < ITER_COUNT; iter++) {
					// SET UP!
					ArrayList<String> set = new ArrayList<>();
					for(int i = 0; i < size; i++) {
						set.add(randomString(5));
					}
					//int findElement = new Random().nextInt(size); // This gets me a random in between 0 and size;
					
					// TIME IT!
					long start = System.nanoTime();
					AnagramUtil.getLargestAnagramGroup(set.toArray(new String[0]));
					long stop = System.nanoTime();
					totalTime += stop - start;
				}
				double averageTime = totalTime / (double)ITER_COUNT;
				System.out.println(size + "\t" + averageTime); // print to console
				fw.write(size + "\t" + averageTime + "\n"); // write to file.
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Create a random string [a-z] of specified length
	public static String randomString(int length)
	{
		String retval = "";
		for(int i = 0; i < length; i++)
		{
			// ASCII values a-z,A-Z are contiguous (52 characters)
			retval += (char)('a' + (rand.nextInt(26)));
		}
		return retval;
	}
	
	// Reads words from a file (assumed to contain one word per line)
	// Returns the words as an array of strings.
	public static String[] readFile(String filename)
	{
		ArrayList<String> results = new ArrayList<String>();
		try
		{
			BufferedReader input = new BufferedReader(new FileReader(filename));
			while(input.ready())
			{
				results.add(input.readLine());
			}
			input.close();
		}
		catch(Exception e)
		{e.printStackTrace();}
		String[] retval = new String[1];
		return results.toArray(retval);
	}
	
}
