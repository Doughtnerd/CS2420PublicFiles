package lab04;

public class BetterRandomNumberGenerator implements RandomNumberGenerator {
	private long seed;
	private long multiplier;
	private long increment;
	public static void main (String[] args){
		BetterRandomNumberGenerator rand = new BetterRandomNumberGenerator();
		rand.setSeed(System.currentTimeMillis());
		rand.setConstants(1, 1);
		System.out.println(rand.nextInt(3000));
	}
	
	
	
	@Override
	public int nextInt(int max) {
		if (seed == 0)
			seed = 1;
		if (multiplier == 0)
			multiplier =1;
		if (increment == 0)
			increment = 1;
		seed = (int) (((multiplier*seed) + (increment))%max)&((1L <<48)-1);
		return (int) seed;
	}

	@Override
	public void setSeed(long seed) {
		this.seed = seed;
	}

	@Override
	public void setConstants(long multiplier, long increment) {
		this.multiplier = multiplier;
		this.increment = increment;
	}

}
