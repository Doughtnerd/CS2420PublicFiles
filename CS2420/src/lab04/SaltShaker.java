package lab04;

import lab04.RandomNumberGenerator;

public class SaltShaker {
    
	public String getSalt(RandomNumberGenerator rng) {
		StringBuilder sb = new StringBuilder();
		rng.setConstants(25214903917L, 11);
		int randomNumber = 0;
		while(sb.length() < 10) {
			for(int i = 0; i<1024; i++) {
				randomNumber = rng.nextInt(95);
			}
			sb.append((char)(32 + randomNumber));
		}
		return sb.toString();
	}
}
