package miscClasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import assignment05.SortUtil;

public class SortVocab {
	static ArrayList<String> list = new ArrayList<String>();

	public static void main(String[] args){
		readFromFile("C:/Users/Chris/Desktop/Vocab.txt");
		SortUtil.mergesort(list, null);
		for(String s : list){
			System.out.println(s);
		}
	}
	
	private static void readFromFile(String filename) {
		try {
			File file = new File(filename);
			Scanner fileInput = new Scanner(file);
			while (fileInput.hasNextLine()) {
				String s = fileInput.nextLine();
				if (!s.equals("")) {
					list.add(s.toLowerCase());
				}
			}

		} catch (FileNotFoundException e) {
		}
	}
}
