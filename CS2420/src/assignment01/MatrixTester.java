package assignment01;

import static org.junit.Assert.*;

import org.junit.Test;

public class MatrixTester {

	private Matrix m1 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
	private Matrix m2 = new Matrix(new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
	private Matrix m3 = new Matrix(new int[][] { { 1, 2, 3 }, { 2, 5, 6 } });
	private Matrix m4 = new Matrix(new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
	private Matrix m5 = new Matrix(new int[][] { { 6, 5, 4 }, { 3, 2, 1 } });
	private Matrix m6 = new Matrix(new int[][] { { 9, 8, 7 }, { 6, 5, 4 }, { 3, 2, 1 } });
	private Matrix addResult = new Matrix(new int[][] { { 2, 4, 6 }, { 4, 10, 12} });
	private Matrix falseAddResult = new Matrix(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } });
	private Matrix mult1 = new Matrix(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } });
	private Matrix mult2 = new Matrix(new int[][] { { 7, 8 }, { 9, 10 }, { 11, 12 } });
	private Matrix multResult1 = new Matrix(new int[][] { { 58, 64 }, { 139, 154 } });
	private Matrix multResult2 = new Matrix(new int[][] {{39,54,69},{49,68,87},{59,82,105}});
	private Matrix m7 = new Matrix(new int[][] {{1}});
	private Matrix m8 = new Matrix(new int[][] {{2}});
	
	@Test
	public void testToString() {
		assertEquals("1 2 3 \n\n2 5 6 \n\n", m1.toString());
		assertEquals("1 2 3 \n\n4 5 6 \n\n7 8 9 \n\n", m2.toString());
		assertEquals("1 \n\n", m7.toString());
		assertEquals("2 \n\n", m8.toString());
	}
	
	@Test
	public void testEquals(){
		assertFalse(m1.equals(m4));
		assertFalse(m2.equals(m3));
		assertFalse(m1.equals(m2));
		assertTrue(m1.equals(m3));
		assertTrue(m2.equals(m4));
		assertFalse(m5.equals(m3));
		assertFalse(m6.equals(m2));
		assertFalse(m1.equals(null));
		assertFalse(m7.equals(m8));
		assertTrue(m1.equals(m1));
	}
	
	@Test
	public void testPlus(){
		assertEquals(addResult, m1.plus(m3));
		assertNotEquals(this.falseAddResult, m1.plus(m3));
		assertEquals(null, m1.plus(m2));
		assertEquals(null, m1.plus(null));
		assertEquals("3 \n\n", m7.plus(m8).toString());
	}
	
	@Test
	public void testMultiply(){
		Matrix result = mult1.times(mult2);
		Matrix result2 = mult2.times(mult1);
		assertArrayEquals(multResult1.data, result.data);
		assertArrayEquals(this.multResult2.data, result2.data);
		assertEquals(null, m1.times(m1));
		assertEquals(null, m1.times(null));
		assertEquals("2 \n\n", m7.times(m8).toString());
	}

}
