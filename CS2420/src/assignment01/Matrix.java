package assignment01;

/**
 * This class is an multidimensional array object build to store ints.
 * 
 * @author Chris Carlson
 *
 */
public class Matrix {
	int numRows;
	int numColumns;
	int data[][];

	/**
	 * Constructs a new matrix with the given data and automatically determines
	 * dimensions.
	 * 
	 * @param data
	 *            The data that the matrix will contain.
	 */
	public Matrix(int data[][]) {
		numRows = data.length; // d.length is the number of 1D arrays in the 2D
								// array
		if (numRows == 0) {
			numColumns = 0;
		} else {
			numColumns = data[0].length; // d[0] is the first 1D array
		}
		this.data = new int[numRows][numColumns]; // create a new matrix to hold
													// the data
		// copy the data over
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				this.data[i][j] = data[i][j];
			}
		}
	}

	/**
	 * Returns true if the lhs and rhs matrices are equal in data length, width,
	 * and value. Returns false otherwise.
	 * 
	 * @param other
	 *            The RHS Matrix of the argument.
	 * @return Whether or not the LHS of the argument equals the RHS.
	 */
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!(other instanceof Matrix)) { // make sure the Object we're
											// comparing to is a Matrix
			return false;
		}
		Matrix matrix = (Matrix) other; // if the above was not true, we know
										// it's safe to treat 'o' as a Matrix
		if (!(data.length == matrix.data.length)) {
			return false;
		}
		if (!(this.data[0].length == matrix.data[0].length)) {
			return false;
		}
		for (int i = 0; i < this.data.length; i++) {
			for (int j = 0; j < this.data[0].length; j++) {
				if (!(this.data[i][j] == matrix.data[i][j])) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Returns a string consisting of the elements in the matrix data in an
	 * orderly fashion with spaces in between elements and new lines after each
	 * row.
	 * 
	 * @return The matrix as a formatted String.
	 */
	@Override // instruct the compiler that we do indeed intend for this method
				// to override the superclass' (Object) version
	public String toString() {
		String s = "";
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				s = s + data[i][j] + " ";
			}
			s = s + "\n\n";
		}
		return s;
	}

	/**
	 * Returns the matrix that is a product of the lhs matrix and the rhs
	 * matrix. Returns null if the matrices are incompatible.
	 * 
	 * @param matrix
	 *            RHS of the argument.
	 * @return The resulting matrix of operation.
	 */
	public Matrix times(Matrix matrix) {
		if (matrix == null) {
			return null;
		}
		if (!(this.data[0].length == matrix.data.length)) {
			return null;
		}
		int newRows = data.length;
		int newCols = matrix.data[0].length;
		Matrix result = new Matrix(new int[newRows][newCols]);
		for (int i = 0; i < result.data.length; i++) {
			for (int j = 0; j < result.data[0].length; j++) {
				int sumOfElements = 0;
				for (int k = 0; k < this.data[0].length; k++) {
					sumOfElements += this.data[i][k] * matrix.data[k][j];
				}
				result.data[i][j] = sumOfElements;
			}
		}
		return result;
	}

	/**
	 * Returns a matrix that is a result of adding the elements of the lhs
	 * matrix to the elements in the rhs matrix. Returns null if the matrices
	 * are of different sizes.
	 * 
	 * @param matrix
	 *            The RHS of the argument.
	 * @return The resulting matrix of the operation.
	 */
	public Matrix plus(Matrix matrix) {
		if (matrix == null) {
			return null;
		}
		if (!(data.length == matrix.data.length)) {
			return null;
		}
		if (!(data[0].length == matrix.data[0].length)) {
			return null;
		}
		Matrix result = new Matrix(new int[data.length][data[0].length]);
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				result.data[i][j] = (data[i][j] + matrix.data[i][j]);
			}
		}
		return result;
	}
}
