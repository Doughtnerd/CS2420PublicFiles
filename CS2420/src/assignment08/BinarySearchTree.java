package assignment08;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import assignment08.SortedSet;

/**
 * Unbalanced Binary Search Tree data structure.
 * 
 * @author Christopher Carlson
 * @author Shaheen Page
 *
 * @param <T>
 *            - Type of data this tree contains.
 */
public class BinarySearchTree<T extends Comparable<? super T>> implements SortedSet<T> {

	/**
	 * The size of the tree.
	 */
	private int size;

	/**
	 * The root node of the tree.
	 */
	private Node root;

	/**
	 * Constructs a BinarySearch Tree of type T.
	 */
	public BinarySearchTree() {

	}

	/**
	 * Ensures that this set contains the specified item.
	 * 
	 * @param item
	 *            - the item whose presence is ensured in this set
	 * @return true if this set changed as a result of this method call (that
	 *         is, if the input item was actually inserted); otherwise, returns
	 *         false
	 * @throws NullPointerException
	 *             if the item is null
	 */
	@Override
	public boolean add(T item) throws NullPointerException {
		if (item == null) {
			throw new NullPointerException();
		} else if (size == 0) {
			this.root = new Node(item);
			return true;
		} else {
			if (size <= 1 << 14){
				return addRecursiveMethod(item);
			} else {
				return addLoopingMethod(item);
			}
		}
	}

	/**
	 * This is the entry point method that handles adding a new node to the tree
	 * using findposition() and addChild()
	 * 
	 * @param item
	 *            - The data for the node being added.
	 * @return True if the item was successfully added to the tree, false
	 *         otherwise.
	 */
	private boolean addLoopingMethod(T item) {
		Node insertPoint = findPosition(item);
		if (insertPoint.data.compareTo(item) == 0) {
			return false;
		}
		return addChild(insertPoint, item);
	}

	/**
	 * This is the driver method for addRecursive and handles the assigning of
	 * new nodes into the tree.
	 * 
	 * @param item
	 *            - The data t
	 * @return True if the item was successfully added to the tree, false otherwise.
	 */
	private boolean addRecursiveMethod(T item) {
		Node insertionPoint = addRecursive(item, root, null);
		int comparison = item.compareTo(insertionPoint.data);
		Node newNode = comparison == 0 ? null : new Node(item);
		if (comparison > 0) {
			insertionPoint.right = newNode;
		} else if (comparison < 0) {
			insertionPoint.left = newNode;
		} else {
			return false;
		}
		newNode.parent = insertionPoint;
		return true;
	}

	/**
	 * This is an alternative method for finding the parent node for a new node
	 * that is being added to the tree.
	 * 
	 * @param item
	 *            - The data the new node will contain.
	 * @param node
	 *            - The node being operated on.
	 * @param previous
	 *            - The parent of the current node.
	 * @return Either the node that contains the item being added or the node
	 *         that should be the parent of the new node.
	 */
	private Node addRecursive(T item, Node node, Node previous) {
		if (node == null) {
			return previous;
		} else {
			int comparison = item.compareTo(node.data);
			if (comparison < 0) {
				return addRecursive(item, node.left, node);
			} else if (comparison > 0) {
				return addRecursive(item, node.right, node);
			} else {
				return node;
			}
		}

	}

	/**
	 * This method traverses the tree to find the appropriate spot to insert the
	 * desired node.
	 * 
	 * @param item
	 *            Item wishing to be inserted into the tree
	 * @return The Node that should become the parent node of the item.
	 */
	private Node findPosition(T item) {
		Node tNode = root, previous = null;
		while (tNode != null) {
			previous = tNode;
			int comparison = item.compareTo(tNode.data);
			if (comparison < 0) {
				tNode = tNode.left;
			} else if (comparison > 0) {
				tNode = tNode.right;
			} else {
				return tNode;
			}
		}
		return previous;
	}

	/**
	 * Adds a child to the given parent node with item as the data.
	 * 
	 * @param parent
	 *            The parent node to add a child to.
	 * @param item
	 *            The data the new child node should contain.
	 * @return True if the data was successfully added.
	 */
	private boolean addChild(Node parent, T item) {
		int comparison = item.compareTo(parent.data);
		if (comparison < 0) {
			Node newNode = new Node(item);
			parent.left = newNode;
			newNode.parent = parent;
		} else if (comparison > 0) {
			Node newNode = new Node(item);
			parent.right = newNode;
			newNode.parent = parent;
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Ensures that this set contains all items in the specified collection.
	 * 
	 * @param items
	 *            - the collection of items whose presence is ensured in this
	 *            set
	 * @return true if this set changed as a result of this method call (that
	 *         is, if any item in the input collection was actually inserted);
	 *         otherwise, returns false
	 * @throws NullPointerException
	 *             if any of the items were null
	 */
	@Override
	public boolean addAll(Collection<? extends T> items) {
		boolean flag = false;
		for (T item : items) {
			if (add(item)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * Removes all items from this set. The set will be empty after this method
	 * call.
	 */
	@Override
	public void clear() {
		root = null;
		size = 0;
	}

	/**
	 * Determines if there is an item in this set that is equal to the specified
	 * item.
	 * 
	 * @param item
	 *            - the item sought in this set
	 * @return true if there is an item in this set that is equal to the input
	 *         item; otherwise, returns false
	 * @throws NullPointerException
	 *             if the item is null
	 */
	@Override
	public boolean contains(T item) {
		if (item == null) {
			throw new NullPointerException();
		}
		if (size == 0) {
			return false;
		}
		if (findPosition(item).data.compareTo(item) == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Determines if for each item in the specified collection, there is an item
	 * in this set that is equal to it.
	 * 
	 * @param items
	 *            - the collection of items sought in this set
	 * @return true if for each item in the specified collection, there is an
	 *         item in this set that is equal to it; otherwise, returns false
	 * @throws NullPointerException
	 *             if any of the items is null
	 */
	@Override
	public boolean containsAll(Collection<? extends T> items) {
		boolean flag = true;
		for (T item : items) {
			if (!contains(item)) {
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * Returns the first (i.e., smallest) item in this set.
	 * 
	 * @throws NoSuchElementException
	 *             if the set is empty
	 */
	@Override
	public T first() throws NoSuchElementException {
		if (this.size == 0) {
			throw new NoSuchElementException();
		}
		return getLeftMostNode(root).data;
	}

	/**
	 * Returns true if this set contains no items.
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/**
	 * Returns the last (i.e., largest) item in this set.
	 * 
	 * @throws NoSuchElementException
	 *             if the set is empty
	 */
	@Override
	public T last() throws NoSuchElementException {
		if (this.size == 0) {
			throw new NoSuchElementException();
		}
		return getRightMostNode(root).data;
	}

	/**
	 * Ensures that this set does not contain the specified item.
	 * 
	 * @param item
	 *            - the item whose absence is ensured in this set
	 * @return true if this set changed as a result of this method call (that
	 *         is, if the input item was actually removed); otherwise, returns
	 *         false
	 * @throws NullPointerException
	 *             if the item is null
	 */
	@Override
	public boolean remove(T item) {
		if (item == null) {
			throw new NullPointerException();
		}
		if (size == 0) {
			return false;
		}
		Node temp = findPosition(item);
		if (temp.data.compareTo(item) == 0) {
			if (temp.left == null || temp.right == null) {
				splice(temp);
			} else {
				Node w = temp.right;
				w = getLeftMostNode(w);
				temp.data = w.data;
				splice(w);
			}
			size--;
			return true;
		}
		return false;
	}

	/**
	 * Recurses through the right side of the list to get the smallest element.
	 * 
	 * @param node
	 *            - Node whos subtrees are being recursed on.
	 * @return Last node in the sequence.
	 */
	private Node getLeftMostNode(Node node) {
		if (node.left == null) {
			return node;
		} else {
			return getLeftMostNode(node.left);
		}
	}

	/**
	 * Recurses through the left side of the list to get the smallest element.
	 * 
	 * @param node
	 *            - Node whos subtrees are being recursed on.
	 * @return Last node in the sequence.
	 */
	private Node getRightMostNode(Node node) {
		if (node.right == null) {
			return node;
		} else {
			return getRightMostNode(node.right);
		}
	}

	/**
	 * Recombines the tree when removing a node with 1 or no children.
	 * 
	 * @param node
	 *            - The node to remove
	 */
	private void splice(Node node) {
		Node temp, parent;
		// Set temp to node's child.
		if (node.left != null) {
			temp = node.left;
		} else {
			temp = node.right;
		}

		/*
		 * If node is equal to root, set temp as the new root and set the parent
		 * placeholder node variable to null.
		 */
		if (node == root) {
			root = temp;
			parent = null;
		} else {
			/*
			 * Otherwise, set the parent placeholder to the node's parent
			 */
			parent = node.parent;
			/*
			 * Then check which side node was on and set that side to temp,
			 * thereby dereferencing node and deleting it.
			 */
			if (parent.left == node) {
				parent.left = temp;
			} else {
				parent.right = temp;
			}
		}
		/*
		 * Set up the reference from the temp back to the parent if temp is not
		 * null.
		 */
		if (temp != null) {
			temp.parent = parent;
		}
	}

	/**
	 * Ensures that this set does not contain any of the items in the specified
	 * collection.
	 * 
	 * @param items
	 *            - the collection of items whose absence is ensured in this set
	 * @return true if this set changed as a result of this method call (that
	 *         is, if any item in the input collection was actually removed);
	 *         otherwise, returns false
	 * @throws NullPointerException
	 *             if any of the items is null
	 */
	@Override
	public boolean removeAll(Collection<? extends T> items) {
		boolean flag = false;
		for (T item : items) {
			if (remove(item)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * Returns the number of items in this set.
	 */
	@Override
	public int size() {
		return this.size;
	}

	/**
	 * Returns an ArrayList containing all of the items in this set, in sorted
	 * order.
	 */
	@Override
	public ArrayList<T> toArrayList() {
		ArrayList<T> list = new ArrayList<>();
		toArrayRecursive(list, root);
		return list;
	}

	/**
	 * Recursively traverses the tree in-order and adds the appropriate elements
	 * to the list.
	 * 
	 * @param list
	 *            - List to add the elemements to.
	 * @param node
	 *            - Current node in the sequence.
	 */
	private void toArrayRecursive(ArrayList<T> list, Node node) {
		if (node == null) {
			return;
		} else {
			toArrayRecursive(list, node.left);
			list.add(node.data);
			toArrayRecursive(list, node.right);
		}
	}

	/**
	 * Writes a dot file from the Binary Search Tree Data to be used to make a
	 * tree graph.
	 * 
	 * @param filename
	 *            - The filepath, name, and extension to write the file as.
	 */
	public void writeDot(String filename) {
		try {
			// PrintWriter(FileWriter) will write output to a file
			PrintWriter output = new PrintWriter(new FileWriter(filename));

			// Set up the dot graph and properties
			output.println("digraph BST {");
			output.println("node [shape=record]");

			if (root != null)
				writeDotRecursive(root, output);

			output.println("}");
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeDotRecursive(Node n, PrintWriter output) throws Exception {
		// output.println(n.data + "[label=\"<L> |<D> " + n + "|<R> \"]");
		if (n.left != null) {
			// write the left subtree
			writeDotRecursive(n.left, output);

			// then make a link between n and the left subtree
			output.println(n.data + ":L -> " + n.left.data + ":D");
		}
		if (n.right != null) {
			// write the left subtree
			writeDotRecursive(n.right, output);

			// then make a link between n and the right subtree
			output.println(n.data + ":R -> " + n.right.data + ":D");
		}

	}

	/**
	 * Inner class representing the data nodes in the tree.
	 * 
	 * @author Christopher Carlson
	 * @author Shaheen Page
	 *
	 */
	class Node {

		/**
		 * Parent node of this node.
		 */
		private Node parent;

		/**
		 * Left child of this node.
		 */
		private Node left;

		/**
		 * Right child of this node.
		 */
		private Node right;

		/**
		 * Data this node contains.
		 */
		private T data;

		/**
		 * Constructs a node with null data.
		 */
		private Node() {
			this(null);
		}

		/**
		 * Constructs a node with passed data.
		 * 
		 * @param data
		 *            - Starting data for the node.
		 */
		private Node(T data) {
			this.data = data;
			size++;
		}
	}
}
