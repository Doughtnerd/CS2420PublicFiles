package assignment08;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SpellCheckerTest {

	private static final Object[] TEST_ONE_RESULT = {"cehck","mispelllled","willl","onty","teh","dictionry"};
	private static final String SMALL_DICTIONARY = "src/assignment08/dictionary.txt";
	private static final String GOOD_LUCK = "src/assignment08/good_luck.txt";
	private static final String HELLO_WORLD = "src/assignment08/hello_world.txt";
	private static final String TEST_1 = "src/assignment08/spellcheck_test1.txt";
	private static final String TEST_2 = "src/assignment08/spellcheck_test2.txt";
	private SpellChecker smallSC;
	
		
	
	@Before
	public void setupChecker(){
		smallSC = new SpellChecker(new File(SMALL_DICTIONARY));
	}

	@Test
	public void testAddToDictionary() {
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).isEmpty());
		smallSC.removeFromDictionary("hello");
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).get(0).equals("hello"));
		smallSC.addToDictionary("hello");
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).isEmpty());
	}

	@Test
	public void testRemoveFromDictionary() {
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).isEmpty());
		smallSC.removeFromDictionary("hello");
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).get(0).equals("hello"));
	}

	@Test
	public void testSpellCheck() {
		assertTrue(smallSC.spellCheck(new File(HELLO_WORLD)).isEmpty());
		assertTrue(smallSC.spellCheck(new File(GOOD_LUCK)).get(0).equals("bst"));
		assertArrayEquals(smallSC.spellCheck(new File(TEST_1)).toArray(), TEST_ONE_RESULT);
		assertTrue(smallSC.spellCheck((new File(TEST_2))).isEmpty());
	}

}
