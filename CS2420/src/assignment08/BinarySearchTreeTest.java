package assignment08;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BinarySearchTreeTest {

	BinarySearchTree<Integer> tree;
	static ArrayList<Integer> intList;
	static ArrayList<Integer> compareList;
	static ArrayList<Integer> mixedList;
	static ArrayList<Integer> noMatchList;
	
	@BeforeClass
	public static void setupLists(){
		intList = new ArrayList<>();
		compareList = new ArrayList<>();
		mixedList = new ArrayList<>();
		noMatchList = new ArrayList<>();
		
		for(int i = 0, j = 0; i < 20; i++, j+=2){
			intList.add(i);
			compareList.add(j);
			noMatchList.add(-10-i);
		}
		
		mixedList.add(2);
		mixedList.add(1);
		mixedList.add(4);
		mixedList.add(-1);
		mixedList.add(0);
		mixedList.add(5);
		
		Collections.shuffle(intList);
	}
	
	@Before
	public void setupTree() {
		tree = new BinarySearchTree<>();
	}

	@Test
	public void testAdd() {
		assertTrue(tree.add(3));
		assertEquals(1, tree.size());
		assertFalse(tree.add(3));
		assertEquals(1, tree.size());
	}
	
	@Test (expected = NullPointerException.class)
	public void testAddNullItem(){
		tree.add(null);
	}

	@Test
	public void testAddAll() {
		assertTrue(tree.addAll(intList));
		assertEquals(20, tree.size());
		assertTrue(tree.last()==19);
		assertTrue(tree.first()==0);
	}
	
	@Test (expected = NullPointerException.class)
	public void testAddAllNullItem(){
		ArrayList<Integer> list = new ArrayList<>();
		for (Integer i : intList){
			list.add(i);
		}
		list.add(null);
		tree.addAll(list);
	}

	@Test
	public void testClear() {
		tree.addAll(intList);
		tree.clear();
		assertEquals(0, tree.size());
		assertFalse(tree.contains(0));
	}

	@Test
	public void testContains() {
		tree.addAll(intList);
		assertTrue(tree.contains(0));
		assertFalse(tree.contains(-1));
		tree.remove(0);
		assertFalse(tree.contains(0));
	}
	
	@Test (expected = NullPointerException.class)
	public void testContainsNullItem(){
		tree.addAll(intList);
		tree.contains(null);
	}
	
	@Test (expected = NullPointerException.class)
	public void testContainsAllNullItem(){
		ArrayList<Integer> list = new ArrayList<>();
		for (Integer i : intList){
			list.add(i);
		}
		tree.addAll(list);
		list.add(null);
		tree.containsAll(list);
	}

	@Test
	public void testContainsAll() {
		assertFalse(tree.containsAll(intList));
		tree.addAll(intList);
		assertTrue(tree.containsAll(intList));
		assertFalse(tree.containsAll(compareList));
	}

	@Test
	public void testFirst() {
		tree.addAll(mixedList);
		assertEquals(new Integer(-1), tree.first());
		tree.remove(-1);
		assertTrue(tree.first()==0);
	}
	
	@Test (expected = NoSuchElementException.class)
	public void testFirstEmpty(){
		tree.first();
	}

	@Test
	public void testIsEmpty() {
		assertTrue(tree.isEmpty());
		assertEquals(0, tree.size());
		tree.addAll(intList);
		assertFalse(tree.isEmpty());
		assertTrue(tree.size()!=0);
		tree.clear();
		assertTrue(tree.isEmpty());
		assertEquals(0, tree.size());
	}

	@Test
	public void testLast() {
		tree.addAll(mixedList);
		assertEquals(new Integer(5), tree.last());
		assertFalse(tree.last()==4);
		tree.remove(5);
		assertEquals(new Integer(4), tree.last());
	}
	
	@Test (expected = NoSuchElementException.class)
	public void testLastEmpty(){
		tree.last();
	}

	@Test
	public void testRemove() {
		tree.addAll(mixedList);
		assertTrue(tree.remove(1));
		assertEquals(5, tree.size());
		assertFalse(tree.remove(9));
		assertEquals(5, tree.size());
	}
	
	@Test 
	public void testRemoveEmpty(){
		assertFalse(tree.remove(0));
	}
	
	@Test (expected = NullPointerException.class)
	public void testRemoveNullItem(){
		tree.addAll(intList);
		tree.remove(null);
	}

	@Test
	public void testRemoveAll() {
		tree.addAll(mixedList);
		assertTrue(tree.removeAll(mixedList));
		assertTrue(tree.size()==0);
		tree.addAll(intList);
		assertTrue(tree.removeAll(compareList));
		assertEquals(10, tree.size());
	}
	
	@Test (expected = NullPointerException.class)
	public void testRemoveAllNullItem(){
		ArrayList<Integer> list = new ArrayList<>();
		for (Integer i : intList){
			list.add(i);
		}
		tree.addAll(list);
		list.add(null);
		tree.removeAll(list);
	}
	
	@Test
	public void removeAllFalse(){
		tree.addAll(intList);
		assertFalse(tree.removeAll(noMatchList));
	}
	
	@Test
	public void removeAllEmpty(){
		assertFalse(tree.removeAll(intList));
	}

	@Test
	public void testToArrayList() {
		tree.addAll(mixedList);
		mixedList.sort(null);
		assertArrayEquals(tree.toArrayList().toArray(new Object[0]), mixedList.toArray(new Object[0]));
	}

}
