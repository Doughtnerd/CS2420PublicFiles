package assignment10;


import java.util.Random;
import java.util.TreeSet;

import org.junit.Test;

public class HashFunctorTest {
	private static final int ITER_COUNT = 10;
	private static final int LOOP_COUNT = 1000000;
	private static final int STRING_SIZE = 26;
	private static final int ARRAY_SIZE = 7;
	static Random rand = new Random(System.currentTimeMillis());
	GoodHashFunctor hash = new GoodHashFunctor();
	//MediocreHashFunctor hash = new MediocreHashFunctor();
	//BadHashFunctor hash = new BadHashFunctor();
	
	@Test
	public void testHash() {
		int total=0, count;
		for(int i = 0; i < ITER_COUNT; i++){
			count = 0;
			TreeSet<Integer> list = new TreeSet<Integer>();
			for(int j = 0; j < LOOP_COUNT; j++){
				if(!list.add(hash.hash(randomString(STRING_SIZE))%ARRAY_SIZE)){
					count++;
				}
			}
			total+=count;
		}
		double avg = (double) total/ITER_COUNT;
		System.out.println("Encountered average of " + avg + " collisions");
		System.out.println("Collision percentage is: " + (avg/(double) LOOP_COUNT)*100 + "%");
	}

	public static String randomString(int length)
	{
		String retval = "";
		for(int i = 0; i < length; i++)
		{
			retval += (char)('a' + (rand.nextInt(26)));
		}
		return retval;
	}
}
