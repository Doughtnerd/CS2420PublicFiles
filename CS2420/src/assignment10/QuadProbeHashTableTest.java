package assignment10;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class QuadProbeHashTableTest {

	static Random rand = new Random(System.currentTimeMillis());
	ArrayList<String> list;
	QuadProbeHashTable table;
	GoodHashFunctor hash = new GoodHashFunctor();
	
	@Before
	public void setup(){
		table = new QuadProbeHashTable(5, hash);
		list = new ArrayList<String>();
	}

	@Test
	public void testAdd() {
		assertTrue(table.add("Hello"));
		assertTrue(table.add("Hello"));
		assertTrue(table.add("Hello"));
		assertTrue(table.add("Hello"));
		assertTrue(table.add("Hello"));
		assertEquals(5, table.size());
	}
	
	@Test
	public void testAddAll() {
		for(int i = 0; i < 1000; i++){
			String s = randomString(8);
			list.add(s);
		}
		assertTrue(table.addAll(list));
		assertTrue(table.size()==1000);
	}

	@Test
	public void testClear() {
		for(int i = 0; i < 1000; i++){
			String s = randomString(8);
			list.add(s);
		}
		assertTrue(table.addAll(list));
		table.clear();
		assertFalse(table.contains(list.get(2)));
		assertTrue(table.size()==0);
	}

	@Test
	public void testContains() {
		for(int i = 0; i < 1000; i++){
			String s = randomString(8);
			list.add(s);
		}
		assertTrue(table.addAll(list));
		for(String s : list){
			assertTrue(list.contains(s));
		}
		assertFalse(table.contains("Hello"));
	}

	@Test
	public void testContainsAll() {
		for(int i = 0; i < 1000; i++){
			String s = randomString(8);
			list.add(s);
		}
		assertTrue(table.addAll(list));
		assertTrue(table.containsAll(list));
		assertTrue(table.size()==1000);
	}

	@Test
	public void testIsEmpty() {
		assertTrue(table.isEmpty());
		table.add("Hello");
		assertFalse(table.isEmpty());
		table.clear();
		assertTrue(table.isEmpty());
	}

	@Test
	public void testSize() {
		for(int i = 0; i < 1000; i++){
			String s = randomString(8);
			table.add(s);
			assertTrue(table.size()==i+1);
		}
	}

	public static String randomString(int length)
	{
		String retval = "";
		for(int i = 0; i < length; i++)
		{
			retval += (char)('a' + (rand.nextInt(26)));
		}
		return retval;
	}
	
}
