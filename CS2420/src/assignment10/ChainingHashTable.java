package assignment10;

import java.util.Collection;
import java.util.LinkedList;

public class ChainingHashTable implements Set<String> {

	/**
	 * Size of the Table
	 */
	private int size;

	/**
	 * The capacity the table was originally initialized to.
	 */
	private int initCapacity;

	/**
	 * The average bucket length (how full or long the array is);
	 */
	private double loadFactor;

	/**
	 * Maximum Load factor that is allowed for this table.
	 */
	private static double MAX_LOAD = 7;
	
	/**
	 * The functor to use for hashing.
	 */
	private HashFunctor functor;

	/**
	 * The LinkedList backing the array.
	 */
	private LinkedList<String>[] data;

	/**
	 * Creates a new Chaining Hash Table with initial capacity and HashFunctor.
	 * 
	 * @param capacity
	 *            - Initial Hash Table capacity.
	 * @param functor
	 *            - Functor to use for hashing.
	 */
	public ChainingHashTable(int capacity, HashFunctor functor) {
		if (functor == null) {
			functor = new GoodHashFunctor();
		}
		if (!isPrime(capacity)) {
			capacity = getNextPrime(capacity);
		}
		initCapacity = capacity;
		this.functor = functor;
		data = initialize(capacity);
	}

	/**
	 * Creates a new Chained Hash Table with an initial table length of
	 * capacity.
	 * 
	 * @param capacity
	 *            - Starting size of table.
	 * @return A new LinkedList<String>[].
	 */
	@SuppressWarnings("unchecked")
	private LinkedList<String>[] initialize(int capacity) {
		LinkedList<String>[] ret = (LinkedList<String>[]) new LinkedList[capacity];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = new LinkedList<String>();
		}
		return ret;
	}

	/**
	 * Returns the next prime number that is greater than n or equal to n.
	 * 
	 * @param n
	 *            - The number used to find the next prime in the sequence.
	 * @return n if n is prime, otherwise returns the next prime number greater
	 *         than n.
	 */
	private static int getNextPrime(int n) {
		boolean flag = false;
		if (n == 1) {
			getNextPrime(n + 1);
		}
		if (n == 2) {
			return n;
		}
		while (flag == false) {
			if (isPrime(n)) {
				flag = true;
			} else {
				n++;
			}
		}
		return n;
	}

	/**
	 * Checks if n is a prime number.
	 * 
	 * @param n
	 *            - The number to check.
	 * @return True if n is prime, false otherwise.
	 */
	private static boolean isPrime(int n) {
		if (n % 2 == 0) {
			return false;
		}
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	@Override
	public boolean add(String item) {
		if (item == null || item == "") {
			return false;
		}
		if (loadFactor >= MAX_LOAD) {
			resize();
		}
		int hash = functor.hash(item);
		int index = getIndex(hash);
		data[index].add(item);
		size++;
		loadFactor = size/data.length;
		return true;
	}
	
	/**
	 * Grows the data array to the next prime number that is greater than twice
	 * the current length.
	 */
	private void resize() {
		LinkedList<String>[] temp = data;
		int newSize = getNextPrime(temp.length * 2);
		data = initialize(newSize);
		for (int i = 0; i < temp.length; i++) {
				for(String s : temp[i]){
					int hash = functor.hash(s);
					int index = getIndex(hash);
					data[index].add(s);
				}
		}
	}

	/**
	 * Gets the appropriate index based off of the item hash.
	 * 
	 * @param n
	 *            - The item hash.
	 * @return n modulo data.length.
	 */
	private int getIndex(int n) {
		return Math.abs(n) % data.length;
	}

	@Override
	public boolean addAll(Collection<? extends String> items) {
		boolean flag = false;
		for (String s : items) {
			if (add(s)) {
				flag = true;
			}
		}
		return flag;
	}

	@Override
	public void clear() {
		data = initialize(initCapacity);
		size = 0;
		loadFactor = 0;
	}

	@Override
	public boolean contains(String item) {
		if (item == null || item == "") {
			return false;
		}
		int hash = functor.hash(item);
		int index = getIndex(hash);
		for (String s : data[index]) {
			if (s == item) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<? extends String> items) {
		boolean flag = true;
		for (String s : items) {
			if (!contains(s)) {
				flag = false;
			}
		}
		return flag;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}
	
	public int addUp(){
		int size = 0;
		for(LinkedList<String> l : data){
			size+=l.size();
		}
		return size;
	}
}
