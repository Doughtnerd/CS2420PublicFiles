package assignment10;

public class GoodHashFunctor implements HashFunctor {

	@Override
	public int hash(String item) {
		int hash = item.length() << item.charAt(item.length()/2);
		char[] chars = item.toCharArray();
		for (int i = 0; i < chars.length; i++){
			if("aeiou".indexOf(chars[i]) < 0){
				hash += (chars[i] * hash + 53);
			} else {
				hash = 31 * hash + chars[i];
			}
		}
		return Math.abs(hash);
	}
}
