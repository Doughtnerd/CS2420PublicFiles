package assignment10;

import java.util.Collection;

import assignment10.Set;

public class QuadProbeHashTable implements Set<String> {

	/**
	 * Size of the Table
	 */
	private int size;

	/**
	 * The capacity the table was originally initialized to.
	 */
	private int initCapacity;

	/**
	 * How full the array is.
	 */
	private double loadFactor;

	/**
	 * Maximum load factor the table is allowed to have
	 */
	private static double MAX_LOAD = 0.5;
	/**
	 * The functor to use for hashing.
	 */
	private HashFunctor functor;

	private int collisionCount;
	
	/**
	 * Array backing the table.
	 */
	private String[] data;

	/**
	 * Constructs a hash table of the given capacity that uses the hashing
	 * function specified by the given functor.
	 * 
	 * @param capacity
	 *            - Initial Hash Table capacity.
	 * @param functor
	 *            - Functor to use for hashing.
	 */
	public QuadProbeHashTable(int capacity, HashFunctor functor) {
		if (functor == null) {
			functor = new GoodHashFunctor();
		}
		if (!isPrime(capacity)) {
			capacity = getNextPrime(capacity);
		}
		collisionCount=0;
		initCapacity = capacity;
		this.functor = functor;
		data = new String[capacity];
	}

	/**
	 * Returns the next prime number that is greater than n or equal to n.
	 * 
	 * @param n
	 *            - The number used to find the next prime in the sequence.
	 * @return n if n is prime, otherwise returns the next prime number greater
	 *         than n.
	 */
	public static int getNextPrime(int n) {
		boolean flag = false;
		if (n == 1) {
			getNextPrime(n + 1);
		}
		if (n == 2) {
			return n;
		}
		while (flag == false) {
			if (isPrime(n)) {
				flag = true;
			} else {
				n++;
			}
		}
		return n;
	}

	/**
	 * Checks if n is a prime number.
	 * 
	 * @param n
	 *            - The number to check.
	 * @return True if n is prime, false otherwise.
	 */
	private static boolean isPrime(int n) {
		if (n % 2 == 0) {
			return false;
		}
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	@Override
	public boolean add(String item) {
		if (item == null || item == "") {
			return false;
		}
		if (loadFactor >= MAX_LOAD) {
			resize();
		}
		int hash = functor.hash(item);
		int index = getIndex(hash);
		if(data[index]!=null){
			this.collisionCount++;
		}
		data[probe(index, null)] = item;
		size++;
		loadFactor = (double) size / data.length;
		return true;
	}

	/**
	 * Grows the data array to the next prime number that is greater than twice
	 * the current length.
	 */
	private void resize() {
		String[] temp = data;
		int newSize = getNextPrime(temp.length * 2);
		data = new String[newSize];
		for (int i = 0; i < temp.length; i++) {
			if (temp[i] != null) {
				int hash = functor.hash(temp[i]);
				int index = getIndex(hash);
				index = probe(index, null);
				if (index == -1) {
					System.err.println("Insertion not found");
				}
				data[index] = temp[i];
			}
		}
	}

	/**
	 * Probes the index first, then uses quadratic probing to find the next
	 * available open spot if item==null. If item!=null, uses quadratic probing
	 * to find the index of the item within the table.
	 * 
	 * @param index
	 *            - The index that was originally hashed.
	 * @param item
	 *            - the item to find.
	 * @return The next available index if item was null, otherwise returns the
	 *         index of item.
	 */
	private int probe(int index, String item) {
		item = item == null || item == "" ? null : item;
		if (data[index] == item) {
			return index;
		}
		for (int i = 0; i < data.length; i++) {
			int next = getIndex(index+(i*i));// & 0x7fffffff
			if (data[next] == item) {
				return next;
			}
		}
		return -1;
	}

	/**
	 * Gets the appropriate index based off of the item hash modded by the array
	 * length.
	 * 
	 * @param n
	 *            - The item hash.
	 * @return The index n should be inserted into.
	 */
	private int getIndex(int n) {
		return Math.abs(n) % data.length;
	}

	@Override
	public boolean addAll(Collection<? extends String> items) {
		boolean flag = false;
		for (String s : items) {
			if (add(s)) {
				flag = true;
			}
		}
		return flag;
	}

	@Override
	public void clear() {
		data = new String[initCapacity];
		size = 0;
		loadFactor = 0;
	}

	@Override
	public boolean contains(String item) {
		if (item == null || item == "") {
			return false;
		}
		int hash = functor.hash(item);
		int index = getIndex(hash);
		index = probe(index, item);
		return index != -1;
	}

	@Override
	public boolean containsAll(Collection<? extends String> items) {
		boolean flag = true;
		for (String s : items) {
			if (!contains(s)) {
				flag = false;
			}
		}
		return flag;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

	public String toString() {
		String s = "";
		int count = 0;
		for (int i = 0; i < data.length-1; i++) {
			if (data[i] != null) {
				s += data[i] + ", ";
				count++;
			}
		}
		s+=data[data.length-1];
		System.out.println(count);
		return s;
	}
	
	public int length(){
		return data.length;
	}
	
	public int getCollisions(){
		return this.collisionCount;
	}
}
