package file;

import java.io.File;
import java.io.IOException;

public class MakeFile {
	
	public static void main(String[] args) {
		File f = new File(args[0]);
		File parent = f.getParentFile();
		if(parent != null) {
			parent.mkdirs();
		}
		try {
			if(f.createNewFile()) {
				System.out.println("Created!");
			}
		} catch (IOException e) {
			System.out.println("Most worthless exception ever. " + e.getMessage());
		}
	}
}
