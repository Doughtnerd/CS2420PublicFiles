package file;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;

public class FindFile {
	public static void main(String[] args) {
		//File f = new File(args[0]);
		File f = findFile(args[0]);
		StringBuilder sb = new StringBuilder();
		try {
			Files.readAllLines(f.toPath()).forEach(sb::append);
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
		}
		System.out.println(sb.toString());
	}

	public static File findFile(String path) {
		URL url = FindFile.class.getClassLoader().getResource(path);
		if(url != null) {
			try{
				path = URLDecoder.decode(url.getPath(), "UTF-8");
			} catch(UnsupportedEncodingException e) {
				System.out.println("oh well.");
			}
		}
		return new File(path);
	}
}
