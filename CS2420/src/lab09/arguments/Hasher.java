import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {
	public static void main(String[] args) {
		String algorithm = System.getProperty("hash");
		String input = args[0];
		String hash = null;
		try {
			MessageDigest digester = MessageDigest.getInstance(algorithm);
			digester.update(input.getBytes());
			byte byteData[] = digester.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			hash = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("I'm sorry, I don't know what algorithm " + algorithm + " is.");
		}
		System.out.println("The " + algorithm + " hash of " + input + " is:\n" + hash);
	}
}
