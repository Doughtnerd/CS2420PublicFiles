public class IsPrime {
	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Don't forget to give me number!");
			return;
		}
		int prime = Integer.parseInt(args[0]);
		int sqrt = (int)Math.sqrt(prime);
		for(int i = 2; i <= sqrt; i++) {
			if(prime % i == 0) {
				System.out.println("Not prime.");
				return;
			}
		}
		System.out.println("Prime!");
	}
}
